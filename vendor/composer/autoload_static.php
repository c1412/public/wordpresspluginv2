<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitdb7b1df0ca20df35d6b5d3fbbc8d6788
{
    public static $prefixLengthsPsr4 = array (
        'c' => 
        array (
            'cds\\wordpress\\' => 14,
            'cds\\tribe\\' => 10,
            'cds\\templates\\' => 14,
            'cds\\storage\\' => 12,
            'cds\\options\\' => 12,
            'cds\\helpers\\' => 12,
            'cds\\gravity\\' => 12,
            'cds\\debug\\' => 10,
            'cds\\api\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'cds\\wordpress\\' => 
        array (
            0 => __DIR__ . '/../..' . '/wordpress',
        ),
        'cds\\tribe\\' => 
        array (
            0 => __DIR__ . '/../..' . '/tribe',
        ),
        'cds\\templates\\' => 
        array (
            0 => __DIR__ . '/../..' . '/templates',
        ),
        'cds\\storage\\' => 
        array (
            0 => __DIR__ . '/../..' . '/storage',
        ),
        'cds\\options\\' => 
        array (
            0 => __DIR__ . '/../..' . '/options',
        ),
        'cds\\helpers\\' => 
        array (
            0 => __DIR__ . '/../..' . '/helpers',
        ),
        'cds\\gravity\\' => 
        array (
            0 => __DIR__ . '/../..' . '/gravity',
        ),
        'cds\\debug\\' => 
        array (
            0 => __DIR__ . '/../..' . '/debug',
        ),
        'cds\\api\\' => 
        array (
            0 => __DIR__ . '/../..' . '/api',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
        'cds\\api\\Address' => __DIR__ . '/../..' . '/api/Address.php',
        'cds\\api\\Authors' => __DIR__ . '/../..' . '/api/Authors.php',
        'cds\\api\\BundleItems' => __DIR__ . '/../..' . '/api/BundleItems.php',
        'cds\\api\\Connection' => __DIR__ . '/../..' . '/api/Connection.php',
        'cds\\api\\Contact' => __DIR__ . '/../..' . '/api/Contact.php',
        'cds\\api\\ContactLog' => __DIR__ . '/../..' . '/api/ContactLog.php',
        'cds\\api\\Credits' => __DIR__ . '/../..' . '/api/Credits.php',
        'cds\\api\\Event' => __DIR__ . '/../..' . '/api/Event.php',
        'cds\\api\\EventsPayment' => __DIR__ . '/../..' . '/api/EventsPayment.php',
        'cds\\api\\Facility' => __DIR__ . '/../..' . '/api/Facility.php',
        'cds\\api\\Fees' => __DIR__ . '/../..' . '/api/Fees.php',
        'cds\\api\\Field' => __DIR__ . '/../..' . '/api/Field.php',
        'cds\\api\\Firm' => __DIR__ . '/../..' . '/api/Firm.php',
        'cds\\api\\FirmChange' => __DIR__ . '/../..' . '/api/FirmChange.php',
        'cds\\api\\FirmKey' => __DIR__ . '/../..' . '/api/FirmKey.php',
        'cds\\api\\Leaders' => __DIR__ . '/../..' . '/api/Leaders.php',
        'cds\\api\\Legislator' => __DIR__ . '/../..' . '/api/Legislator.php',
        'cds\\api\\ListItem' => __DIR__ . '/../..' . '/api/ListItem.php',
        'cds\\api\\ListKey' => __DIR__ . '/../..' . '/api/ListKey.php',
        'cds\\api\\Lists' => __DIR__ . '/../..' . '/api/Lists.php',
        'cds\\api\\Logging' => __DIR__ . '/../..' . '/api/Logging.php',
        'cds\\api\\MetaData' => __DIR__ . '/../..' . '/api/MetaData.php',
        'cds\\api\\Person' => __DIR__ . '/../..' . '/api/Person.php',
        'cds\\api\\PersonChange' => __DIR__ . '/../..' . '/api/PersonChange.php',
        'cds\\api\\PersonKey' => __DIR__ . '/../..' . '/api/PersonKey.php',
        'cds\\api\\PersonSearch' => __DIR__ . '/../..' . '/api/PersonSearch.php',
        'cds\\api\\PersonSearchHit' => __DIR__ . '/../..' . '/api/PersonSearchHit.php',
        'cds\\api\\RelatedEvents' => __DIR__ . '/../..' . '/api/RelatedEvents.php',
        'cds\\api\\RelatedProducts' => __DIR__ . '/../..' . '/api/RelatedProducts.php',
        'cds\\api\\Sessions' => __DIR__ . '/../..' . '/api/Sessions.php',
        'cds\\api\\TaxRules' => __DIR__ . '/../..' . '/api/TaxRules.php',
        'cds\\api\\UserDefinedFields' => __DIR__ . '/../..' . '/api/UserDefinedFields.php',
        'cds\\api\\UserDefinedLists' => __DIR__ . '/../..' . '/api/UserDefinedLists.php',
        'cds\\api\\Values' => __DIR__ . '/../..' . '/api/Values.php',
        'cds\\api\\Volunteer' => __DIR__ . '/../..' . '/api/Volunteer.php',
        'cds\\api\\storage\\LocalEvent' => __DIR__ . '/../..' . '/api/storage/LocalEvent.php',
        'cds\\api\\storage\\LocalFirm' => __DIR__ . '/../..' . '/api/storage/LocalFirm.php',
        'cds\\api\\storage\\LocalList' => __DIR__ . '/../..' . '/api/storage/LocalList.php',
        'cds\\api\\storage\\LocalPerson' => __DIR__ . '/../..' . '/api/storage/LocalPerson.php',
        'cds\\api\\storage\\LocalTables' => __DIR__ . '/../..' . '/api/storage/LocalTables.php',
        'cds\\debug\\Debug' => __DIR__ . '/../..' . '/debug/Debug.php',
        'cds\\helpers\\ApiHelper' => __DIR__ . '/../..' . '/helpers/ApiHelper.php',
        'cds\\helpers\\Encryption' => __DIR__ . '/../..' . '/helpers/Encryption.php',
        'cds\\options\\Activate' => __DIR__ . '/../..' . '/options/Activate.php',
        'cds\\options\\Cron' => __DIR__ . '/../..' . '/options/Cron.php',
        'cds\\options\\Database' => __DIR__ . '/../..' . '/options/Database.php',
        'cds\\options\\Settings' => __DIR__ . '/../..' . '/options/Settings.php',
        'cds\\options\\Shortcodes' => __DIR__ . '/../..' . '/options/Shortcodes.php',
        'cds\\options\\models\\Field' => __DIR__ . '/../..' . '/options/models/Field.php',
        'cds\\options\\models\\Page' => __DIR__ . '/../..' . '/options/models/Page.php',
        'cds\\options\\models\\Section' => __DIR__ . '/../..' . '/options/models/Section.php',
        'cds\\templates\\Template' => __DIR__ . '/../..' . '/templates/Template.php',
        'cds\\templates\\assets\\parts\\Alerts' => __DIR__ . '/../..' . '/templates/assets/parts/Alerts.php',
        'cds\\tribe\\SyncEvents' => __DIR__ . '/../..' . '/tribe/SyncEvents.php',
        'cds\\wordpress\\Login' => __DIR__ . '/../..' . '/wordpress/Login.php',
        'cds\\wordpress\\Password' => __DIR__ . '/../..' . '/wordpress/Password.php',
        'cds\\wordpress\\Paywall' => __DIR__ . '/../..' . '/wordpress/Paywall.php',
        'cds\\wordpress\\Profile' => __DIR__ . '/../..' . '/wordpress/Profile.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitdb7b1df0ca20df35d6b5d3fbbc8d6788::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitdb7b1df0ca20df35d6b5d3fbbc8d6788::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitdb7b1df0ca20df35d6b5d3fbbc8d6788::$classMap;

        }, null, ClassLoader::class);
    }
}
