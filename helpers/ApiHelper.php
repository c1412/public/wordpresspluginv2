<?php

namespace cds\helpers;

use cds\api\UserDefinedFields;
use cds\api\UserDefinedLists;


class ApiHelper
    {
    /**
     * Builds an array of types
     * @param string|null $ObjectType
     * @param mixed $Data
     * @return array
     */
    public static function array_of_type(?string $ObjectType = null, $Data)
        {
        if (!empty($ObjectType))
            {
            $ObjectType = 'cds\\api\\' . $ObjectType;
            }

        $array = array();

        foreach ($Data as $value)
            {
            if (empty($ObjectType))
                {
                array_push($array, $value);
                }
            else
                {
                array_push($array, new $ObjectType($value));
                }
            }

        return $array;

        }
    }