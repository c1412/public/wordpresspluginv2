<?php

namespace cds\helpers;

class Encryption
    {

    /**
     * Encrypt text
     * @param mixed $code
     * @return string
     */
    public static function encrypt( $code ) {
        if ( empty( $code ) ) {
          return '';
        }
          $key = 'SECURE_AUTH_KEY';
          $encryption_key = base64_decode( $key );
          $iv = substr(openssl_random_pseudo_bytes( openssl_cipher_iv_length('aes-256-cbc')), 0, 16 );
          $encrypted = openssl_encrypt( $code, 'aes-256-cbc', $encryption_key, OPENSSL_RAW_DATA, $iv );
          return base64_encode( $encrypted . '::' . $iv );
    }

    /**
     * Decrypt text
     * @param mixed $code
     * @return bool|string
     */
    public static function decrypt($code)
    {
        $key = 'SECURE_AUTH_KEY';
        $encryption_key = base64_decode($key);
        list($encrypted_data, $iv) = explode('::', base64_decode($code), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, OPENSSL_RAW_DATA, $iv);
    }

    }