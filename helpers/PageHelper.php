<?php

namespace cds\helpers;

use WP_Error;

defined('ABSPATH') || exit;

class PageHelper
{

    /**
     * Generate a new wordpress page
     * @param mixed $Title
     * @param mixed $Content
     * @param mixed|null $ParentId
     * @return int|WP_Error
     */
    public function create_page($Title, $Content, $ParentId = NULL)
    {
        $Page = get_page_by_title($Title, 'OBJECT', 'page');

        if (!empty($Page)) {
            return $Page->ID;
        }

        $PageId = wp_insert_post(
            array(
                'comment_status' => 'close',
                'ping_status'    => 'close',
                'post_author'    => 1,
                'post_title'     => ucwords($Title),
                'post_name'      => strtolower(str_replace(' ', '-', trim($Title))),
                'post_status'    => 'publish',
                'post_content'   =>  $Content,
                'post_type'      => 'page',
                'post_parent'    =>  $ParentId
            )
        );

        return $PageId;

    }

    public function is_backend_page(){

        // Checking to see if page is running in the backend
        // Note this will fire on all AJAX, this creates Issues
        if ( defined( 'REST_REQUEST' ) && REST_REQUEST || is_admin() || wp_doing_cron())  {
            $AdminPage = true;
        } else {
            $AdminPage = false;
        };

        return $AdminPage;
    }


}
