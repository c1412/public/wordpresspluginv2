<?php

namespace cds\affinipay\models;

defined('ABSPATH') or exit;

class ChargeModel
{

     public function __construct($input = null)
     {
          if (!empty($input)) {
               foreach ($input as $key => $val) {
                    if (property_exists(__CLASS__, $key)) {
                         $setter_function = 'set' . $key;
                         $this->$setter_function($val);
                    }
               }
          }
     }

     public function getId()
     {
          return $this->id;
     }
     public function setId($id)
     {
          $this->id = $id;
     }
     public $id; //String

     public function getType()
     {
          return $this->type;
     }
     public function setType($type)
     {
          $this->type = $type;
     }
     public $type; //String

     public function getAccount_id()
     {
          return $this->account_id;
     }
     public function setAccount_id($account_id)
     {
          $this->account_id = $account_id;
     }
     public $account_id; //String

     public function getStatus()
     {
          return $this->status;
     }
     public function setStatus($status)
     {
          $this->status = $status;
     }
     public $status; //String

     public function getAmount()
     {
          return $this->amount;
     }
     public function setAmount($amount)
     {
          $this->amount = $amount;
     }
     public $amount; //int

     public function getCurrency()
     {
          return $this->currency;
     }
     public function setCurrency($currency)
     {
          $this->currency = $currency;
     }
     public $currency; //String

     public function getAuto_capture()
     {
          return $this->auto_capture;
     }
     public function setAuto_capture($auto_capture)
     {
          $this->auto_capture = $auto_capture;
     }
     public $auto_capture; //boolean

     public function getCapture_Time()
     {
          return $this->capture_time;
     }
     public function setCapture_Time($capture_time)
     {
          $this->capture_time = $capture_time;
     }
     public $capture_time; //boolean

     public function getAmount_refunded()
     {
          return $this->amount_refunded;
     }
     public function setAmount_refunded($amount_refunded)
     {
          $this->amount_refunded = $amount_refunded;
     }
     public $amount_refunded; //int

     public function getAuthorization_code()
     {
          return $this->authorization_code;
     }
     public function setAuthorization_code($authorization_code)
     {
          $this->authorization_code = $authorization_code;
     }
     public $authorization_code; //String

     public function getMethod()
     {
          return (array) $this->method;
     }
     public function setMethod($method)
     {
          $this->method = (array) new CreditCardModel($method);
     }
     public $method; //Method

     public function getCvv_result()
     {
          return $this->cvv_result;
     }
     public function setCvv_result($cvv_result)
     {
          $this->cvv_result = $cvv_result;
     }
     public $cvv_result; //String

     public function getAvs_result()
     {
          return $this->avs_result;
     }
     public function setAvs_result($avs_result)
     {
          $this->avs_result = $avs_result;
     }
     public $avs_result; //String
}
