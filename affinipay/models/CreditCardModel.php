<?php

namespace cds\affinipay\models;

defined('ABSPATH') or exit;

class CreditCardModel
{

    public function __construct($input = null)
    {
        if (!empty($input)) {
            foreach ($input as $key => $val) {
                if (property_exists(__CLASS__, $key)) {
                    $setter_function = 'set' . $key;
                    $this->$setter_function($val);
                }
            }
        }
    }

    public function getNumber()
    {
        return $this->number;
    }
    public function setNumber($number)
    {
        $this->number = $number;
    }
    public $number; //String

    public function getCvv()
    {
        return $this->cvv;
    }
    public function setCvv($cvv)
    {
        $this->cvv = $cvv;
    }
    public $cvv; //String

    public function getExp_month()
    {
        return $this->exp_month;
    }
    public function setExp_month($exp_month)
    {
        $this->exp_month = $exp_month;
    }
    public $exp_month; //int

    public function getExp_year()
    {
        return $this->exp_year;
    }
    public function setExp_year($exp_year)
    {
        $this->exp_year = $exp_year;
    }
    public $exp_year; //int

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public $name; //String

    public function getAddress1()
    {
        return $this->address1;
    }
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }
    public $address1; //String

    public function getCity()
    {
        return $this->city;
    }
    public function setCity($city)
    {
        $this->city = $city;
    }
    public $city; //String

    public function getState()
    {
        return $this->state;
    }
    public function setState($state)
    {
        $this->state = $state;
    }
    public $state; //String

    public function getPostal_code()
    {
        return $this->postal_code;
    }
    public function setPostal_code($postal_code)
    {
        $this->postal_code = $postal_code;
    }
    public $postal_code; //String

    public function getType()
    {
        return $this->type;
    }
    public function setType($type)
    {
        $this->type = $type;
    }
    public $type; //String

    public function getCard_Type()
    {
        return $this->card_type;
    }
    public function setCard_Type($card_type)
    {
        $this->card_type = $card_type;
    }
    public $card_type; //String



}
