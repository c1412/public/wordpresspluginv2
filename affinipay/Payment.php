<?php

namespace cds\affinipay;

use cds\affinipay\models\CreditCardModel;
use cds\api\EventsPayment;
use cds\helpers\Encryption;
use ChargeIO_ApiError;
use ChargeIO_Charge;
use ChargeIO_PaymentMethodReference;
use WP_Error;

class Payment
    {
    public $PublicKey;
    private $SecretKey;
    private $Amount;

    public function __construct(string $Amount = null)
        {
        $this->set_public();
        $this->set_secret();
        $this->set_amount($Amount);
        }

    /**
     * Setting the public key for the connection.
     * @return void
     */
    private function set_public(): void
        {
        $Encryption = new Encryption();

        $this->PublicKey = get_option('AffinipayPublicKey');
        }

    /**
     * Setting the secret key for the connection.
     * @return void
     */
    private function set_secret(): void
        {
        $Encryption = new Encryption();

        if (get_option('cdsSiteUrl') === get_site_url())
            {
            $this->SecretKey = $Encryption->decrypt(get_option('cdsAffinipayLiveSecretKey'));
            }
        else
            {
            $this->SecretKey = $Encryption->decrypt(get_option('cdsAffinipayTestSecretKey'));
            }
        }

    /**
     * Setting the amount for the transaction.
     * @param string|null $Amount
     * @return void
     */
    private function set_amount(string $Amount = null): void
        {
        if (empty($Amount))
            {
            $Amount = '0';
            }

        $this->$Amount = number_format((float) preg_replace("/[^0-9.]/", "", $Amount) * 100., 0, '.', '');
        }

    /**
     * Managing errors returned from the payment api.
     * @param mixed $Error
     * @return WP_Error
     */
    private function set_error($Error)
        {
        $args = array();
        if (!empty($Error))
            {
            $args['Code'] = 500;
            $args['Title'] = "Payment Processing Error";
            $args['Message'] = "Our Payment processor CPACharge returned the following error: " . $Error->messages[0]["message"];
            $args['Method'] = __METHOD__;
            }
        return new WP_Error(500, $args['Message'], $args);
        }

    /**
     * Authorize the Credit Card
     * @param string $CreditCardToken
     * @param string $ChargeAmount
     * @return ChargeIO_Charge|WP_Error
     */
    public function authorize(string $CreditCardToken, string $Amount): ChargeIO_Charge|WP_Error
        {

        try
            {
            new $this($Amount);
            $Authorization = new ChargeIO_Charge();
            $Authorization = $Authorization->authorize(new ChargeIO_PaymentMethodReference(array('id' => $CreditCardToken)), $this->Amount);
            }
        catch (ChargeIO_ApiError $Error)
            {
            $Authorization = $this->set_error($Error);
            }

        return $Authorization;

        }

    /**
     * Summary of capture
     * @param ChargeIO_Charge $Authorization
     * @param mixed $ChargeAmount
     * @return bool|WP_Error
     */
    public function capture(ChargeIO_charge $Authorization, string $ChargeAmount): bool|WP_Error
        {

        try
            {
            new $this($ChargeAmount);
            $Authorization->capture($ChargeAmount, array('capture_time' => 'NEXT_AUTO_CAPTURE'));
            }
        catch (ChargeIO_ApiError $Error)
            {
            return $Authorization = $this->set_error($Error);
            }

        if ($Authorization->status !== 'AUTHORIZED' || $Authorization->auto_capture !== true)
            {
            $Authorization->void();
            return new WP_Error('500', 'Cart Processing Failed', 'The transaction authorization has been voided. Registrations rolled back.');
            }

        return true;

        }

        /**
     * Convert the CC Number string to work with AM.Net
     * @param mixed $CardNumber
     * @param mixed $CardType
     * @return string|string[]
     */
    public function format_credit_card($CardNumber, $CardType)
        {

        // If the card number in AM.Net is fully masked the error lives here
        $First = '*';

        switch ($CardType)
            {
            case 'VISA':
                $First = '4';
                break;
            case 'AMERICAN_EXPRESS':
                $First = '3';
                break;
            case 'MASTERCARD':
                $First = '5';
                break;
            case 'DISCOVER':
                $First = '6';
                break;
            }

        return substr_replace($CardNumber, $First, 0, 1);
        }

    /**
     * Create an events payment object for AM.net
     * @param ChargeIO_Charge $ChargeObject
     * @param mixed $PersonID
     * @return EventsPayment|bool
     */
    public function set_event_payment(ChargeIO_Charge $ChargeObject, $PersonID)
        {

        $CardObject = new CreditCardModel($ChargeObject->method);

        $Payment = new EventsPayment();

        $Payment->id = $PersonID;
        $Payment->payor= $CardObject->name;
        $Payment->cardno = $this->format_credit_card($CardObject->number, $CardObject->card_type);
        $Payment->exp = $CardObject->exp_month . '/' . $CardObject->exp_year;
        $Payment->authCode = $ChargeObject->authorization_code;
        $Payment->refNbr = $ChargeObject->id;
        $Payment->payBy = "P";
        $Payment->ccv = $CardObject->cvv;
        $Payment->postCCtoPending = false;
        $Payment->note = $CardObject->number . " " . $CardObject->exp_month . '/' . $CardObject->exp_year;
        $Payment->ccAmount = floatval(substr_replace(strval($ChargeObject->amount), '.', -2, 0));

        if (!empty($Payment))
            {
            return $Payment;
            }
        else
            {
            return false;
            }
        }
    //TODO:: Set Dues Payment Here
    public function set_dues_payment()
        {
        }

    }