<?php

namespace cds\affinipay;


defined('ABSPATH') or exit;

add_action('wp_ajax_person_lookup', array(__NAMESPACE__ . '\\SinglePurchase', 'ajax_person_lookup'));
add_action('wp_ajax_nopriv_person_lookup', array(__NAMESPACE__ . '\\SinglePurchase', 'ajax_person_lookup'));

add_action('wp_ajax_create_names_record', array(__NAMESPACE__ . '\\SinglePurchase', 'ajax_create_names_record'));
add_action('wp_ajax_nopriv_create_names_record', array(__NAMESPACE__ . '\\SinglePurchase', 'ajax_create_names_record'));

add_action('wp_ajax_process_transaction', array(__NAMESPACE__ . '\\SinglePurchase', 'ajax_process_transaction'));
add_action('wp_ajax_nopriv_process_transaction', array(__NAMESPACE__ . '\\SinglePurchase', 'ajax_process_transaction'));


class SinglePurchase
{
    public static function show($Attributes)
    {
        // Don't fire is the page is loaded in admin
        if (!PageHelper::is_backend_page()) {

            $Attributes = shortcode_atts(
                array(
                    'type' => '',
                    'purchase_id' => '',
                    'suggested_amount' => '',
                    'redirect_url' => '',
                    'show_marketing' => false,
                    'force_login' => false,
                ),
                $Attributes
            );

            // Check the attributes, on failure this will load Ed Fund Contributions
            $Type = !empty($Attributes['type']) ? $Attributes['type'] : 'contribution';
            $PurchaseID  = !empty($Attributes['purchase_id']) ? $Attributes['purchase_id'] : 'EF';
            $SuggestedAmount  = !empty($Attributes['suggested_amount']) ? $Attributes['suggested_amount'] : 25;
            $RedirectUrl  = !empty($Attributes['redirect_url']) ? $Attributes['redirect_url'] : site_url();
            $ShowMarketing  = $Attributes['show_marketing'];
            $Force_Login  = $Attributes['force_login'];

            // Load the scripts for the control
            SinglePurchase::enqueue_scripts();

            // Empty var for later
            $PersonDto = "";
            $CurrentlyRegistered = false;

            // Start Looking for Variables
            if ($Force_Login == true) {
                $PersonID = SinglePurchase::check_for_login(true, null);
            } else {
                $PersonID = SinglePurchase::check_for_login();
            }

            // Do we have a user
            if (!empty($PersonID)) {
                // Get the wordpress user ID
                $WordpressUserID = UserHelper::get_wp_user_by_amnet_ID($PersonID)->ID;
                // Get the person object from the user record
                $PersonDto = new PersonDto(json_decode(get_field('PersonObject', 'user_' . $WordpressUserID)));

                // Fallback to database
                if (empty($PersonDto->ID)) {
                    $PersonDto = Person::by_ID($PersonID);
                }

                if ($Type == 'event'){
                    $CurrentlyRegistered = SinglePurchase::check_for_duplicate($PersonDto->ID,$PurchaseID);
                }
            }

            $Amount = SinglePurchase::select_purchase_price($Type, $PurchaseID, $PersonID, $SuggestedAmount);

            $PublicKey = Actions::get_public_key();

            // Set the variables for the Javascript
            $jQueryScript  = 'Person = ' . json_encode($PersonDto) . '; ';
            $jQueryScript .= 'Amount = ' . json_encode($Amount) . '; ';
            $jQueryScript .= 'Type = ' . json_encode($Type) . '; ';
            $jQueryScript .= 'PurchaseID = ' . json_encode($PurchaseID) . '; ';
            $jQueryScript .= 'ShowMarketing = ' . json_encode($ShowMarketing) . '; ';
            $jQueryScript .= 'PublicKey = ' . json_encode($PublicKey) . '; ';
            $jQueryScript .= 'RedirectUrl = ' . json_encode($RedirectUrl) . '; ';
            $jQueryScript .= 'AjaxUrl = ' . json_encode(admin_url('admin-ajax.php')) . '; ';
            $jQueryScript .= 'Security = ' . json_encode(wp_create_nonce('security')) . '; ';
            $jQueryScript .= 'UserLookupAction = ' . json_encode('person_lookup') . '; ';
            $jQueryScript .= 'UserCreateAction = ' . json_encode('create_names_record') . '; ';
            $jQueryScript .= 'ProcessTransaction = ' . json_encode('process_transaction') . '; ';
            $jQueryScript .= 'Registered = ' . json_encode($CurrentlyRegistered) . '; ';

            // Load the variables
            wp_add_inline_script('SinglePurchase', $jQueryScript, 'before');

            include(TemplateOverride::cds_locate_template('cart/SinglePurchase.php'));
        }
    }

    /** Load the dependencies  **
     * Adding the initial scripts for the cart to function
     * @return void  */
    public static function enqueue_scripts()
    {
        // Add the jquery
        wp_enqueue_script("jquery");
        wp_enqueue_script("jquery-ui-button");
        wp_enqueue_script("jquery-ui-dialog");

        // Add the Javascript
        wp_enqueue_script('SinglePurchase', WP_PLUGIN_URL . '/cds/templates/assets/javascript/SinglePurchase.js', array('jquery'));
        wp_enqueue_script('ChargeIDFields', 'https://cdn.affinipay.com/hostedfields/1.1.1/fieldGen_1.1.1.js');

        // Add the styles
        wp_enqueue_style('Loading', WP_PLUGIN_URL . '/cds/templates/assets/css/loading.css');
        wp_enqueue_style('grid-ui-cart', WP_PLUGIN_URL . '/cds/templates/assets/css/grid-ui-cart.css');
    }

    /**
     * This is a one stop shop for managing the user information.
     *  1. Checks to see login should be forced.
     *  2. Returns Person ID for logged in user.
     *  3. Used to search for Person ID from email address
     * @param bool $RequireLogin
     * @param mixed $EmailAddress
     * @return $AMNetPersonID || null
     */
    public static function check_for_login($RequireLogin = false, $EmailAddress = null)
    {

        $AMNetPersonID = null;

        // Check if login is required by the control use, if so force login.
        // This can also be used to pass params via query string, for firm administrators.
        if ($RequireLogin) {
            $AMNetPersonID = UserHelper::get_amnet_ID_for_wp_user(UserHelper::check_login());
        }

        // Get the ID by email address
        if (empty($AMNetPersonID) && !empty($EmailAddress)) {
            // Search for user by email
            $PersonSearchResultResponseDto = new PersonSearchResultResponseDto(PersonSearch::search_by_email($EmailAddress));
            // If we have a result
            if (!empty($PersonSearchResultResponseDto) && $PersonSearchResultResponseDto->ResultCount != 0) {
                $AMNetPersonID = $PersonSearchResultResponseDto->PersonID;
            }
        }

        // Assign an AM.Net ID to use if the user is logged in and the $AMNetPersonID is not populated
        if (!empty(wp_get_current_user()) && empty($AMNetPersonID)) {
            $AMNetPersonID = UserHelper::get_amnet_ID_for_wp_user(get_current_user_ID());
        }

        return $AMNetPersonID;
    }

    public static function ajax_process_transaction()
    {

        if (isset($_POST) && !empty($_POST['security']) && wp_verify_nonce($_POST['security'], 'security')) {

            // Get the form data
            if (!empty($_POST['form_data'])) {
                parse_str($_POST['form_data'], $FormData);
            }

            // Get the variables
            $PersonDto = new PersonDto($_POST['person']);
            $Type = $FormData['Type'];
            $PurchaseID = $FormData['PurchaseID'];
            $Amount = $FormData['ChargeAmount'];
            $CreditCardToken = $_POST['card_token'];

            switch ($Type) {
                case "product":
                    $Response = SinglePurchase::process_single_product($PersonDto, $PurchaseID, $CreditCardToken);
                    break;
                case "event":
                    $Response = SinglePurchase::process_single_event($PersonDto, $PurchaseID, $CreditCardToken);
                    break;
                case "contribution":
                    $Response = SinglePurchase::process_single_contribution($PersonDto, $PurchaseID, $CreditCardToken, $Amount);
                    break;
            }

            if (is_wp_error($Response)) {
                wp_send_json_error($Response->get_error_data(), $Response->get_error_code());
            } else {
                wp_send_json_success($Response, 200);
            }
        }
    }

    /**
     * Called from AJAX this function is used to locate a names record for
     * indivIDuals that are attempting to complete a transaction without
     * being logged in.
     *
     * Note:: This function runs before the form is completed do not try to
     * create a user here.
     * @return json_object  */
    public static function ajax_person_lookup()
    {
        // Creating Variables for later use
        $Result = array();

        // Do we have a post and does it pass the security checks
        if (isset($_POST) && !empty($_POST['security']) && wp_verify_nonce($_POST['security'], 'security')) {

            // Look for a user by email address
            $PersonID = SinglePurchase::check_for_login(false, $_POST['email']);

            // Do we have a PersonID
            if (!empty($PersonID)) {

                // Get the wordpress user ID
                $WordpressUser = UserHelper::get_wp_user_by_amnet_ID($PersonID);
                // Get the person object from the Wordpress user record
                $PersonDto = new PersonDto(json_decode(get_field('PersonObject', 'user_' . $WordpressUser->ID)));

                // Fallback to database if needed
                if (empty($PersonDto->ID) || is_wp_error($PersonDto)) {
                    $PersonDto = Person::by_id($PersonID);
                }

                $CurrentlyRegistered = false;

                // Check for Duplicate registrations
                if ($_POST['type'] = 'event') {
                    $CurrentlyRegistered = SinglePurchase::check_for_duplicate($PersonID, $_POST['purchase_id']);
                }

                // Reset the amount
                $Amount = SinglePurchase::select_purchase_price($_POST['type'], $_POST['purchase_id'], $PersonDto->ID, $_POST['charge_amount']);

                $Result = array(
                    'Person' => $PersonDto,
                    'Amount' => $Amount,
                    'Registered' => $CurrentlyRegistered,
                );
            }
        }

        // Return time do we have some values or an error
        if (!empty($Result)) {
            wp_send_json_success($Result, 200);
        } else {
            wp_send_json_error($Result, 500);
        }
    }

    public static function check_for_duplicate($PersonID,$PurchaseID)
    {
        // Get future registrations
        $FutureRegistrations = EventRegistration::get_for_person_starting_after($PersonID, new DateTime());

        // Create container for active registrations
        $ActiveRegistrations = array();

        // Loop through each registration
        foreach ($FutureRegistrations as $EventRegistrationDto) {

            // Get registrations that have not been canceled
            if ($EventRegistrationDto->RegistrationStatusCode != 'C') {

                // Add registration to the active registrations container
                array_push($ActiveRegistrations, trim($EventRegistrationDto->EventCode) . trim($EventRegistrationDto->EventYear));
            }
        }

        return in_array($PurchaseID, $ActiveRegistrations);
    }

    /**
     * Every transaction needs to be tied to a specific person. This function
     * is used to create a user when a checkout process was completed but a names
     * record dID not exist.
     * @param mixed $FormData
     * @return mixed
     */
    public static function ajax_create_names_record($FormData = null)
    {
        if (isset($_POST) && !empty($_POST['security']) && wp_verify_nonce($_POST['security'], 'security')) {

            if (!empty($_POST['form_data'])) {
                parse_str($_POST['form_data'], $FormData);
            }

            // FIXME:: The API is not processing address information correctly, talk to Phil.
            // Current solution is to set Firm to #####
            // Define a new AM.Net Names record
            $NewNonMember = new PersonDto();

            $Names = explode(" ", $FormData['CardHolderName']);
            $NewNonMember->FirstName = $Names['0'];
            $NewNonMember->LastName = $Names[array_key_last($Names)];
            $NewNonMember->MemberStatusCode = 'N';
            $NewNonMember->Email = $FormData['EmailAddress'];
            $NewNonMember->HomePhone = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", preg_replace('[\D]', '', $FormData['PhoneNumber']));
            $NewNonMember->HomeAddressLine1 = $FormData['AddressLine1'];
            $NewNonMember->HomeAddressCity = $FormData['City'];
            $NewNonMember->HomeAddressStateCode = $FormData['State'];
            $NewNonMember->HomeAddressStreetZip = $FormData['ZipCode'];
            $NewNonMember->LinkedFirmCode = '#####';

            // Create a new AM.Net names record
            $PersonDto = Person::create($NewNonMember);

            // DID we create the record as expected
            if (!empty($PersonDto)) {
                // Yes we have a person define the ID
                wp_send_json_success($PersonDto, 200);
            } else {
                // No, the person was not created so we define an error
                $args = array(
                    "Code" => 500,
                    "Title" => "User could no be created.",
                    "Message" => "Could not create AM.Net names record for this order.",
                    "Method" => __METHOD__,
                    "Time" => ContentHelper::get_now_string(),
                    "Form Data" => json_encode(json_decode(json_encode($FormData)), JSON_PRETTY_PRINT),
                    "API Data" => json_encode(json_decode(json_encode($NewNonMember)), JSON_PRETTY_PRINT)
                );

                // Log Error
                Logging::report_error('Error In: ' . __METHOD__ . ' Local Time: ' . $args['Time'] . PHP_EOL . $args['Message'], json_encode($NewNonMember));

                // Return the error
                $Error =  new WP_Error($args['Code'], $args['Title'], $args);
                wp_send_json_error($Error, 500);
            }
        }
    }

    // Used to choose what type of purchase the user is attempting
    public static function select_purchase_price($Type, $PurchaseID, $PersonID = null, $SuggestedAmount = null)
    {
        // If we don't have a user use a non-member record
        if (empty($PersonID)) {
            $PersonID = get_field('NonMemberID', 'cds_core_payment');
        }

        $PersonDto = new PersonDto(Person::by_id($PersonID));

        switch ($Type) {
            case "product":
                $ProductDto = new ProductDto(Product::get($PurchaseID));
                if (!empty($PersonDto) && $PersonDto->MemberStatusCode = 'M') {
                    return $ProductDto->MemberPrice;
                } else {
                    return $ProductDto->NonmemberPrice;
                }
                break;
            case "event":
                $EventPriceDto = Event::get_price($PersonID, $PurchaseID, ContentHelper::get_now_string());
                return $EventPriceDto->MoneyToCollect;
                break;
            case "contribution":
                return $SuggestedAmount;
                break;
        }
    }

    /**
     * Used when we want to process a single event registration.
     * @param AMNetPaymentModel $Payment
     * @param PersonDto $PersonDto
     * @param string $EventCodeYear
     * @return EventsServerDto
     */
    public static function process_single_event(PersonDto $PersonDto, string $EventCodeYear, $CreditCardToken = null)
    {
        // New instance of an event server
        $EventsServer = new EventsServerDto();

        // Get the Event Price Object needed for fees
        $EventPriceDto = Event::get_price($PersonDto->ID, $EventCodeYear, ContentHelper::get_now_string());

        //Get the money to collect
        $ChargeAmount = $EventPriceDto->MoneyToCollect;

        // Check to see if we need to process a payment
        if (!empty($ChargeAmount)) {

            // Authorize the credit card
            $Authorization = Actions::authorize($CreditCardToken, $ChargeAmount);

            if (is_wp_error($Authorization)) {
                SinglePurchase::ajax_credit_card_error($Authorization);
            }

            // Get the payment
            $Payment = Actions::get_amnet_payment($Authorization, $PersonDto->ID);
            // Set the payment information
            foreach ($Payment as $Key => $Val) {
                if (property_exists($EventsServer, $Key)) {
                    $EventsServer->$Key = $Val;
                }
            }
        }

        // Set the required values for an event registration
        $EventsServer->ID = $PersonDto->ID;
        $EventsServer->Code1 = str_pad(substr($EventCodeYear, 0, -2), 8);
        $EventsServer->Yr = substr($EventCodeYear, -2);
        $EventsServer->TranDate = date("c"); //DATE_ISO8601
        $EventsServer->fees = array($EventPriceDto->CalculatedFees);
        $EventsServer->UseCPEBANK = false;
        $EventsServer->SendConfirmation = true;
        $EventsServer->RegStatus =  "R";
        $EventsServer->Note = 'Unlimited CPE Program';

        return $EventsServer;

        // DID the server return an error
        if (EventsServer::post($EventsServer) == true) {

            // Capture the Credit Card Authorization
            $Response = Actions::capture($Authorization, $Payment->CCAmount);
        } else {

            // void the Credit Card Authorization
            $Authorization->void();

            // Couldn't complete the transaction
            $args = array(
                "Code" => 500,
                "Title" => "Unable to add the payment to AM.Net.",
                "Message" => "Failed to create the product sales record for this transaction.",
                "Method" => __METHOD__,
                "Time" => ContentHelper::get_now_string(),
                "API Data" => json_encode($EventsServer, JSON_PRETTY_PRINT)
            );

            // Log Error
            Logging::report_error('Error In: ' . __METHOD__ . ' Local Time: ' . $args['Time'] . PHP_EOL . $args['Message'], json_encode($EventsServer));

            // Return the error
            $Response = new WP_Error($args['Code'], $args['Title'], $args);
        }

        return $Response;
    }


    /**
     * NOTE:: Waiting on Phil
     * Used when we want to process a single product purchase
     * @param AMNetPaymentModel $Payment
     * @param PersonDto $PersonDto
     * @param string $ProductCode
     * @return bool
     */
    public static function process_single_product(PersonDto $PersonDto, string $ProductCode, $CreditCardToken = null)
    {
        $ProductDto = new ProductDto(Product::get($ProductCode));
        $ProductSale = new ProductSaleDto();
        $ProductSaleLineItem = new ProductSaleLineItemDto();

        if ($PersonDto->MemberStatusCode = 'M') {
            $ProductSaleLineItem->Price = $ProductDto->MemberPrice;
        } else {
            $ProductSaleLineItem->Price = $ProductDto->NonmemberPrice;
        }

        // Do we need to Charge for this item
        if (!empty($ProductSaleLineItem->Price)) {

            // Authorize the Credit Card
            $Authorization = Actions::authorize($CreditCardToken, $ProductSaleLineItem->Price);


            if (is_wp_error($Authorization)) {
                SinglePurchase::ajax_credit_card_error($Authorization);
            }

            // Get the payment object
            $Payment = Actions::get_amnet_payment($Authorization, $PersonDto->ID);

            // Set the payment information
            foreach ($Payment as $Key => $Val) {
                if (property_exists($ProductSale, $Key)) {
                    $ProductSale->$Key = $Val;
                }
            }
        }

        // Set Product Info
        foreach ($ProductDto as $Key => $Val) {
            if (property_exists($ProductSale, $Key)) {
                $ProductSale->$Key = $Val;
            }
        }

        // Set Product Info
        foreach ($ProductDto as $Key => $Val) {
            if (property_exists($ProductSaleLineItem, $Key)) {
                $ProductSaleLineItem->$Key = $Val;
            }
        }


        $ProductSaleLineItem->ShippedQuantity = 1;
        $ProductSaleLineItem->ProductCode = $ProductDto->ItemCode;

        $ProductSale->TranDate = date("c"); //DATE_ISO8601
        $ProductSale->ID = $PersonDto->ID;
        $ProductSale->items = array($ProductSaleLineItem);
        $ProductSale->SendPurchaseConfirmationEmail = true;
        $ProductSale->ShipPref = "H";
        //$ProductSale->RefNbr = $Payment->RefNbr;

        // Did the server return an error
        if (ProductSale::post($ProductSale) == true) {

            // Capture the Credit Card Authorization
            $Response = Actions::capture($Authorization, $Payment->CCAmount);
        } else {

            // void the Credit Card Authorization
            $Authorization->void();

            // Couldn't complete the transaction
            $args = array(
                "Code" => 500,
                "Title" => "Unable to add the payment to AM.Net.",
                "Message" => "Failed to create the product sales record for this transaction.",
                "Method" => __METHOD__,
                "Time" => ContentHelper::get_now_string(),
                "API Data" => json_encode($ProductSale, JSON_PRETTY_PRINT)
            );

            // Log Error
            Logging::report_error('Error In: ' . __METHOD__ . ' Local Time: ' . $args['Time'] . PHP_EOL . $args['Message'], json_encode($ProductSale));

            // Return the error
            $Response = new WP_Error($args['Code'], $args['Title'], $args);
        }

        return $Response;
    }


    /**
     * Used to process a single contribution
     * @param AMNetPaymentModel $Payment
     * @param PersonDto $PersonDto
     * @param string $ContributionCode
     * @return bool
     */
    public static function process_single_contribution(PersonDto $PersonDto, string $ContributionCode, $CreditCardToken = null, string $Amount = null)
    {
        // Authorize the Credit Card
        $Authorization = Actions::authorize($CreditCardToken, $Amount);

        if (is_wp_error($Authorization)) {

            SinglePurchase::ajax_credit_card_error($Authorization);
        } else {
            $Payment = Actions::get_amnet_payment($Authorization, $PersonDto->ID);
        }

        $DuesMaintenance = new DuesMaintenanceDto();
        $DuesMaintenanceContribution = new DuesMaintenanceContributionDto();

        $DuesMaintenanceContribution->code = $ContributionCode;
        $DuesMaintenanceContribution->fund = null;
        $DuesMaintenanceContribution->anonymous = "N";
        $DuesMaintenanceContribution->amt = $Payment->CCAmount;

        $DuesMaintenance->ID = $PersonDto->ID;
        $DuesMaintenance->Firm = !empty($PersonDto->Firm) ? $PersonDto->Firm : null;
        $DuesMaintenance->TranDate = date("c"); //DATE_ISO8601
        $DuesMaintenance->Cardno = $Payment->Cardno;
        $DuesMaintenance->cvv = $Payment->ccv;
        $DuesMaintenance->Exp = $Payment->Exp;
        $DuesMaintenance->Note = $Payment->Note;
        $DuesMaintenance->EMail = !empty($PersonDto->Email) ? $PersonDto->Email : null;
        $DuesMaintenance->Payor = $Payment->Payor;
        $DuesMaintenance->CCAmount = $Payment->CCAmount;
        $DuesMaintenance->CCAccount = $Payment->CCAccount;
        $DuesMaintenance->contributions = array($DuesMaintenanceContribution);
        $DuesMaintenance->PostZeroDues = 'Y';
        $DuesMaintenance->DuesPayment = 0;
        $DuesMaintenance->DuesAdjustment = 0;
        $DuesMaintenance->DuesBilling = 0;
        $DuesMaintenance->RefNbr = $Payment->RefNbr;
        $DuesMaintenance->SendEFContributionReceipt = true;

        // DID the server return an error
        if (DuesMaintenance::post($DuesMaintenance) == true) {

            // Capture the Credit Card Authorization
            $Response = Actions::capture($Authorization, $Payment->CCAmount);
        } else {

            // void the Credit Card Authorization
            $Authorization->void();

            // Couldn't complete the transaction
            $args = array(
                "Code" => 500,
                "Title" => "Unable to add the payment to AM.Net.",
                "Message" => "Failed to create the contribution record for this transaction.",
                "Method" => __METHOD__,
                "Time" => ContentHelper::get_now_string(),
                "API Data" => json_encode($DuesMaintenance, JSON_PRETTY_PRINT)
            );

            // Log Error
            Logging::report_error('Error In: ' . __METHOD__ . ' Local Time: ' . $args['Time'] . PHP_EOL . $args['Message'], json_encode($DuesMaintenance));

            // Return the error
            $Response = new WP_Error($args['Code'], $args['Title'], $args);
        }

        return $Response;
    }

    /**
     * Error when authorizing a charge
     * @param mixed $AuthorizeCharge
     * @return mixed
     */
    public static function ajax_credit_card_error($Authorization)
    {

        // Get the information for the error
        $args = array(
            "Code" => $Authorization->get_error_code(),
            "Title" => $Authorization->get_error_message(),
            "Message" => $Authorization->get_error_data(),
            "Method" => __METHOD__
        );

        // Build the error
        $Error = Logging::build_error($args);

        // Send the error back to the ajax call
        return  wp_send_json_error($Authorization->get_error_data(), $Authorization->get_error_code());
    }
}
