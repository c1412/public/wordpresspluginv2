<?php

namespace cds;

use cds\options\Activate;

define('DIR_PATH',plugin_dir_path(__FILE__));
define('WP_PLUGIN_URL', plugin_dir_path(__FILE__));
define('CDS_LOGIN_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('CDS_LOGIN_TEMPLATE_DIR', CDS_LOGIN_PLUGIN_DIR . 'templates/');
define('CDS_LOGIN_TEMPLATE_OVERRIDE_DIR', get_template_directory() . '/cds/login/');

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/options/Settings.php';
require_once __DIR__ . '/options/Cron.php';
require_once __DIR__ . '/options/CustomFields.php';
require_once __DIR__ . '/wordpress/Paywall.php';
require_once __DIR__ . '/options/Shortcodes.php';

use cds\api\Test;

/**
 * Plugin Name: CDS API Connectivity
 * Version: 0.0
 * Description: Integrates the CDS AM.Net API into
 * Author: cjenkins@scacpa.org
 * Author URI:
 * Plugin URI:
 */

if (!class_exists('Start')) {
    class Start {

        /**
         * Constructor
         */
        public function __construct() {
            $this->setup_actions();
        }

        /**
         * Setting up Hooks
         */
        public function setup_actions() {
            //Main plugin hooks
            register_activation_hook(__FILE__,array( __NAMESPACE__.'\\Start','activate'));
            register_deactivation_hook(__FILE__,array( __NAMESPACE__.'\\Start','deactivate'));
            register_uninstall_hook(__FILE__,array(__NAMESPACE__.'\\Start','delete'));
        }

        /**
         * Activate Callback
         * @return void
         */
        public static function activate() {
            if (!current_user_can('activate_plugins'))
            {
                return;
            }

            $Activate = new Activate();
            $Activate->data_actions();
            $Activate->custom_fields();
            $Activate->cron_events();
        }

        /**
         * Deactivate callback
         * @return void
         */
        public static function deactivate() {
            if (!current_user_can('activate_plugins'))
            {
                return;
            }
        }

        /**
         * Delete CallBack
         * @return void
         */
        public static function delete()
        {
            if (!current_user_can('activate_plugins'))
            {
                return;
            }
        }

    }
    $wp_plugin_template = new Start();
}