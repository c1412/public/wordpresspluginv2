<?php

namespace cds\wordpress;

use cds\api\Person;
use cds\storage\PersonStorage;
use cds\templates\assets\parts\Alerts;
use cds\templates\Template;

//If called directly exit
defined('ABSPATH') or exit;

$Login = new Login();

// Enable ajax authentication
add_action('wp_ajax_user_check', array($Login, 'user_check'));
add_action('wp_ajax_nopriv_user_check', array($Login, 'user_check'));

// Changes the login path if custom value is set
add_filter('login_url', array($Login, 'override_login_url'), 10, 3);
// Manages failed logins
add_filter('wp_login_failed', array($Login, 'override_login_failed'), 10, 2);
// Changes the reset password functions
add_filter('lostpassword_url', array($Login, 'override_password_reset_url'), 10, 2);
// Creates a new path for registration
add_filter('register_url', array($Login, 'override_registration_url'), 10);

// NOTE: Test these in production slow locally
add_action('wp_footer', array($Login, 'user_last_seen'), 20);
// NOTE: Test these in production slow locally
add_action('wp_login', array($Login, 'user_last_login'), 20, 2);
// Resets the Cookie expiration to one year
add_filter('auth_cookie_expiration', array($Login, 'cookie_expiration'), 20, 5);
// Add the navigation items
add_filter('wp_nav_menu_top-navigation_items', array($Login, 'add_login_to_menu'), 10, 2);

class Login
    {

    /**
     * Loading required scripts and variables
     * @return void
     */
    public function enqueue_scripts()
        {
        wp_enqueue_script('Login', WP_PLUGIN_URL . '/cds/templates/assets/javascript/Login.js', array('jquery'));
        wp_enqueue_script('Validation', 'https://cdn.jsdelivr.net/npm/jquery-validation@1/dist/jquery.validate.min.js', array('jquery'));

        $AjaxVars = array(
            'wp_ajax_url'             => admin_url('admin-ajax.php'),// Location of AJAX
            'wp_ajax_nonce'           => wp_create_nonce('session_nonce'), // Nonce to use for security
            'wp_ajax_return_function' => 'user_check',
        );

        // Localize the script so we can send variables
        wp_localize_script('Login', 'ajax_vars', $AjaxVars);
        }
    public function show_login()
        {
        // If for some reason we were logged in and passed here continue on.
        if (is_user_logged_in())
            {
            // If redirect known then do it
            if (isset($_GET['redirect_to']) && !empty(url_to_postid($_GET['redirect_to'])))
                {
                wp_safe_redirect($_GET['redirect_to']);
                exit();
                }

            // If a post was passed then go there
            if (isset($_GET['post']) && !empty($Url = get_permalink($_GET['post'])))
                {
                wp_safe_redirect($Url);
                exit();
                }

            // Nothing found? Go home!
            wp_redirect(home_url());
            exit();
            }
        else
            {
            $this->enqueue_scripts();
            // and array to hold any arguments we need to pass to the form
            $args = array();

            if (isset($_GET['redirect_to']) && !empty(url_to_postid($_GET['redirect_to'])))
                {
                // Check for valid post
                $args['redirect_to'] = $_GET['redirect_to'];
                }
            else if (isset($_GET['post']) && !empty($Url = get_permalink($_GET['post'])))
                {
                // If a post id was passed on then resolve it.
                $args['redirect_to'] = $Url;
                }
            else
                {
                $args['redirect_to'] = home_url();
                }

            // This is used when paywall is directed to the login screen.
            if (isset($_GET['notice']))
                {
                switch ($_GET['notice'])
                    {
                    case 'Member Only Access':
                        $Title = get_the_title($_GET['post']);
                        $Link = '<a href="' . $args['redirect_to'] . '">' . $Title . '</a>';
                        $Body = str_replace('{$Link}', $Link, get_option('cdsPaywallMembershipRequiredText'));
                        $Alert = new Alerts('General', 'alert', 'Member Only Content', $Body, true);
                        $args['notice'] = $Alert->Content;
                        break;
                    case 'Login Required':
                        $Title = get_the_title($_GET['post']);
                        $Link = '<a href="' . $args['redirect_to'] . '">' . $Title . '</a>';
                        $Body = str_replace('{$Link}', $Link, get_option('cdsPaywallLoginText'));
                        $Alert = new Alerts('General', 'alert', 'Login Required', $Body, true);
                        $args['notice'] = $Alert->Content;
                        break;
                    default:
                        $args['notice'] = $_GET['notice'];
                    }
                }

            $Template = new Template('Login.php', $args);
            }
        }

    /**
     * Ajax form validation
     * @return void
     */
    public function user_check()
        {

        if (isset($_POST) && wp_verify_nonce($_POST['security'], 'session_nonce'))
            {
            // Get the form data from javascript
            parse_str($_POST['form_data'], $FormData);

            // Find the specific user
            if (empty($FormData['log']) || empty($FormData['pwd']))
                {
                $Subject = "Incomplete Login";
                $Body = "You must provide an email address and password when trying to log in.<br><br>Please review your information and try again.";
                $Alert = new Alerts('Error', 'alert-error-box', $Subject, $Body, false);
                wp_send_json_error($Alert->Content);
                exit;
                }

            // Get the Wordpress User
            $WordPressUser = get_user_by('login', trim($FormData['log']));

            // Did we find a user
            if (empty($WordPressUser))
                {

                // Look in storage and the API.
                $Person = new Person();
                $Person = $Person->get_by_email($FormData['log']);

                // If we have a person get the WordPress User.
                if (isset($Person->ID))
                    {
                    $WordPressUser = $Person->get_wordpress_user_by_person_id(trim($Person->ID));
                    $WordPressUserId = $WordPressUser->ID;
                    }
                }
            else
                {
                // Set the WordPressID
                $WordPressUserId = $WordPressUser->ID;
                }

            // Check for errors and return error is it exists
            if (empty($WordPressUserId))
                {
                $Subject = "Account Not Found ";
                $Body = "An account for " . $FormData['log'] . " could not be located.<br><br>Please review your email address and try again.";
                $Alert = new Alerts('Error', 'alert-error-box', $Subject, $Body, false);
                wp_send_json_error($Alert->Content);
                exit;
                }

            // We have an account but we're not sure what email address we mapped to.
            $WordPressUser = get_user_by('ID', trim($WordPressUserId));
            $WordPressUser = wp_authenticate_username_password(null, $WordPressUser->user_login, $FormData['pwd']);

            // Bad password allow link to email for login
            if (is_wp_error($WordPressUser) || empty($WordPressUser))
                {
                $Subject = "Incorrect Password";
                $Body = "The password used for " . $FormData['log'] . " is incorrect.<br><br><a href= " . wp_lostpassword_url() . ">Click here</a> to reset your password.";
                $Alert = new Alerts('Error', 'alert-error-box', $Subject, $Body, false);
                wp_send_json_error($Alert->Content);
                exit;
                }

            wp_send_json_success();

            exit;
            }
        }

    // We shouldn't need this but just in case.
    public function override_login_failed($UserName, $Error)
        {
        $LoginUrl = wp_login_url();
        $LoginUrl = add_query_arg('notice', urlencode($Error->get_error_message()), $LoginUrl);

        wp_safe_redirect($LoginUrl);
        exit();
        }

    /**
     * Tracks the users last visit
     * @return void
     */
    public function user_last_seen()
        {
        if (is_user_logged_in())
            {

            global $wpdb;

            $wpdb->update('cds_persons',
                    array('LastVisit' => current_time('mysql')),
                    array('WordPressUserID' => get_current_user_id()),
                    '%s');
            return;
            }
        }

    /**
     * Tracks Users Last Login
     * @param mixed $user_login
     * @param mixed $user
     * @return void
     */
    public function user_last_login($user_login, $user)
        {
        if (!is_wp_error($user))
            {

            global $wpdb;

            $wpdb->update('cds_persons',
                    array('LastLogin' => current_time('mysql')),
                    array('WordPressUserID' => $user->ID),
                    '%s');
            }
        }

    /**
     * Extends the expiration of the cookie for one year
     * @param mixed $length
     * @param mixed $user_id
     * @param mixed $remember
     * @return mixed
     */
    public function cookie_expiration($length, $user_id, $remember)
        {
        return YEAR_IN_SECONDS;
        }

    /**
     * Adjusts the WordPress login url to comply with system settings. We're leaving this generic in case it is decided to
     * use an alternative login later. Null setting is allowed.
     * @param mixed $LoginUrl
     * @param mixed $Redirect
     * @param mixed $ForceRedirect
     * @return string|void
     */
    public function override_login_url($LoginUrl, $Redirect, $ForceRedirect)
        {
        // Check for populated login page URL.
        if (!empty($PageID = get_option('cdsLoginRedirect')))
            {
            $LoginUrl = get_page_link($PageID);
            $LoginUrl = add_query_arg('redirect_to', urlencode($Redirect), $LoginUrl);
            $LoginUrl = add_query_arg('reauth', $ForceRedirect, $LoginUrl);
            }

        return $LoginUrl;
        }

    /**
     * Setting the lost password url
     * @param mixed $PasswordResetUrl
     * @param mixed $Redirect
     * @return mixed
     */
    public function override_password_reset_url($PasswordResetUrl, $Redirect)
        {
        if (!empty($PageId = get_option('cdsPasswordReset')))
            {
            $PasswordResetUrl = get_page_link($PageId);
            $PasswordResetUrl = add_query_arg('redirect', urlencode($Redirect), $PasswordResetUrl);
            }

        return $PasswordResetUrl;
        }

    /**
     * Set the URL of the registration page.
     * @param mixed $RegisterUrl
     * @return mixed
     */
    public function override_registration_url($RegisterUrl)
        {
        if (!empty($PageID = get_option('cdsLoginRegistration')))
            {
            $RegisterUrl = get_page_link($PageID);
            }

        return $RegisterUrl;
        }

    /**
     * Dynamically add login to menu
     * @param mixed $Items
     * @param mixed $args
     * @return string
     */
    public function add_login_to_menu($Items, $args)
        {

        if (is_user_logged_in())
            {
            $Link = '<li class="home"><a href="' . wp_logout_url(get_permalink()) . '">' . __('Logout') . '</a></li>';
            }
        else
            {
            $Link = '<li class="home"><a href="' . wp_login_url(get_permalink()) . '">' . __('Login') . '</a></li>';
            }

        $Items .= $Link;

        return $Items;
        }

    /**
     * Allows for login from a transient.
     * @param mixed $login_guid
     * @return bool
     */
    public function programmatic_login($WPUserID)
        {
        // Check to see if the user is already logged in, if so log them out.
        if (is_user_logged_in())
            {
            wp_logout();
            }

        // Hook into the authenticate filter to allows user to login.
        add_filter('authenticate', [$this, 'allow_programmatic_login'], 10, 3); // hook in earlier than other callbacks to short-circuit them

        // Get the user from the transient value
        $WPUser = get_user_by('id', $WPUserID);

        // Set the user credentials for programmatic login.
        $Credentials = array(
            'user_login'    => $WPUser->user_login,
            'user_password' => $WPUser->user_pass,
            'remember'      => true
        );

        // Sign the user on
        $WPUser = wp_signon($Credentials, true);

        // End the authentication
        remove_filter('authenticate', [$this, 'allow_programmatic_login'], 10);

        //Check to ensure we have a user
        if (is_a($WPUser, 'WP_User'))
            {
            // Set the user to current user
            wp_set_current_user($WPUser->ID, $WPUser->user_login);
            // Ensure the user is logged in
            if (is_user_logged_in())
                {
                return true;
                }
            }
        return false;
        }

    /**
     * Allows the programmatic login.
     * @param mixed $WPUser
     * @param mixed $Username
     * @param mixed $Password
     * @return \WP_User|bool
     */
    public function allow_programmatic_login($WPUser, $Username, $Password)
        {
        return get_user_by('login', $Username);
        }

    }