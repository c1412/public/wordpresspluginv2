<?php

namespace cds\wordpress;

use cds\api\Logging;
use cds\api\Person;
use cds\storage\PersonStorage;
use cds\templates\assets\parts\Alerts;
use cds\templates\Template;
use WP_Error;

//If called directly exit
defined('ABSPATH') or exit;

$Password = new Password();

add_action('wp_ajax_password_reset', array($Password, 'password_reset'));
add_action('wp_ajax_nopriv_password_reset', array($Password, 'password_reset'));

class Password
    {
    /**
     * Process the reset request.
     * @return void
     */
    public function password_reset()
        {
        if (isset($_POST) && wp_verify_nonce($_POST['security'], 'session_nonce'))
            {
            // Get the form data from javascript
            parse_str($_POST['form_data'], $FormData);

            // Find the specific user
            $Person = new Person();
            $WPUser = $Person->get_wordpress_user_by_email($FormData['email_address']);

            // Couldn't find a user set-up and error.
            if (empty($WPUser))
                {
                $Subject = "Account Not Found ";
                $Body = "An account for " . $FormData['email_address'] . " could not be located.<br><br>Please review your entry and try again.";

                $AlertText = new Alerts('Error', 'alert-error-box', $Subject, $Body, false);
                $AlertText = $AlertText->Content;

                wp_send_json_error($AlertText);
                exit;
                }
            else
                {
                // User found get the data from name storage.
                $Person = new Person(get_user_meta($WPUser->ID, 'cdsId', true));
                }

            // Build an array to send back to the Email
            $User = array(
                'id'    => $WPUser->ID,
                'email' => $WPUser->user_login
            );

            if (isset($Person->EmailAddress2))
                {
                $User['email2'] = $Person->EmailAddress2;
                }

            if (!empty($AlertText = $this->send_password_reset_email($User)))
                {
                wp_send_json_success($AlertText);
                }
            }
        }

    /**
     * Include the required scripts.
     * @return void
     */
    public function enqueue_scripts()
        {
        wp_enqueue_script('Password', WP_PLUGIN_URL . '/cds/templates/assets/javascript/Password.js', array('jquery'));
        wp_enqueue_script('Validation', 'https://cdn.jsdelivr.net/npm/jquery-validation@1/dist/jquery.validate.min.js', array('jquery'));

        $js_ajax_variables = array(
            'wp_ajax_url'             => admin_url('admin-ajax.php'),// Location of AJAX
            'wp_ajax_nonce'           => wp_create_nonce('session_nonce'),// Nonce to use for security
            'wp_ajax_return_function' => 'password_reset' // What function to call on ajax return

        );

        // Localize the script so we can send variables
        wp_localize_script('Password', 'ajax_vars', $js_ajax_variables);
        }
    /**
     * Display the password reset form.
     * @return void
     */
    public function show_password_reset()
        {
        $this->enqueue_scripts();

        $Template = new Template('Password.php');
        }

    /**
     * Creates a transient for the specific user and returns a reset link
     * @param array $WPUser
     * @return WP_Error|string
     */
    public function create_reset_link(array $WPUser)
        {
        // Expire the transient in 15 Minutes
        $Expire = 900;

        // Hash a unique ID to use as guid for look-up
        $Name = uniqid(wp_hash_password(get_user_meta($WPUser['id'], 'cdsId', true)), true);

        // Did we manage to create a transient entry
        if (set_transient($Name, $WPUser['id'], $Expire))
            {
            // This is where to put the logic to email the link
            return ('<a href="' . wp_registration_url() . '?action=reset&login_guid=' . $Name . '">Login as ' . $WPUser['email'] . '</a>');
            }
        else
            {
            $Error = new WP_Error("500", "Create Transient Failed: " . __METHOD__, (array) $WPUser);
            new Logging($Error);
            return $Error;

            }
        }

    /**
     * Generates and sends the password reset email.
     * @param array $WPUser
     * @return null|bool
     */
    public function send_password_reset_email(array $WPUser)
        {
        $Alerts = new Alerts();
        // Set the variables.
        $EmailHeaders = array('Content-Type: text/html; charset=UTF-8');
        $To = trim($WPUser['email']);
        $NickName = get_user_meta($WPUser['id'], 'nickname', true);
        $EmailSubject = get_option('Account Reset Request', true);
        $EmailSubject = wp_strip_all_tags($EmailSubject, true);
        $EmailBody = get_option('cdsPasswordRecoveryText');
        $ResetLink = $this->create_reset_link($WPUser);

        // Insert the Link
        $EmailBody = str_replace('{$Link}', $ResetLink, $EmailBody);
        $EmailBody = str_replace('{$User}', $NickName, $EmailBody);

        // Capture second email if it exists
        if (!empty($email2 = trim($WPUser['email2'])))
            {
            $To = $To . ',' . $email2;
            }

        // Try to send mail and return a notice.
        if (!is_wp_error($ResetLink))
            {
            $Subject = "Password Reset Email Sent";
            $EmailArray = explode(',', $To, 2);
            $Emails = preg_replace('/(?<=.).(?=.*@)/u', '*', $EmailArray[0]) . ' and ' . preg_replace('/(?<=.).(?=.*@)/u', '*', $EmailArray[1]);
            $Body = "We've sent email to " . $Emails . ". <br><br>Please check your email, including your junk/spam folders for password reset instructions.";


            $AlertText = $Alerts->get_success_alert('alert-success-box', $Subject, $Body, false);

            $IsSent = wp_mail($To, $EmailSubject, $EmailBody, $EmailHeaders);

            if ($IsSent)
                {
                return $AlertText;
                }
            else
                {

                $AlertText = $Alerts->get_error_alert('Failed to Email', 'Mail server returned error sending Password Reset.', false);
                $Error = new WP_Error("Malformed Email", __NAMESPACE__ . " Password Recovery Email Failed for " . $To . ".");
                new Logging($Error);
                return $AlertText;
                }

            }
        else
            {
            $AlertText = $Alerts->get_error_alert('Failed to Email', 'Mail server returned error sending Password Reset.', false);
            $Error = new WP_Error("Failed to build Link", __NAMESPACE__ . " Password Recovery Email Failed for " . $To . ".");
            new Logging($Error);
            return $AlertText;
            }
        }

    }