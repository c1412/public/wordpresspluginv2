<?php

namespace cds\wordpress;

use cds\api\Logging;
use cds\api\Person;
use cds\storage\PersonStorage;
use cds\templates\assets\parts\Alerts;
use cds\templates\Template;
use DateTime;
use WP_Error;

// If called directly exit
defined('ABSPATH') or exit;

$Profile = new Profile();
// User to define the query vars used on the page
add_filter('query_vars', array($Profile, 'add_query_vars_filter'));

// Wire up the AJAX
add_action('wp_ajax_process_profile', array($Profile, 'process_profile'));
add_action('wp_ajax_nopriv_process_profile', array($Profile, 'process_profile'));

class Profile
{
    /**
     * Include the required scripts.
     * @return void
     */
    public  function enqueue_scripts()
    {
        wp_enqueue_script('profile', WP_PLUGIN_URL . '/cds/templates/assets/javascript/Profile.js', array('jquery'));
        wp_enqueue_script('Validation', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js', array('jquery'));
        wp_enqueue_script('Additional', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/additional-methods.min.js', array('Validation'));

        // Variable Name
        $Constant = 'ajax_vars';
        // Arguments to pass
        $Args = array(
            'wp_ajax_url' => admin_url('admin-ajax.php'),
            'wp_ajax_nonce' => wp_create_nonce('session_nonce'),
            'wp_ajax_return_function' => 'process_profile'
        );
        // Bundle up the script
        $Script = $Constant . "=" . json_encode($Args);

        // Add the values to the script
        wp_add_inline_script('profile', $Script, 'before');
    }

    /**
     * Called by ajax when the form is submitted
     * @return void
     */
    public  function process_profile()
    {
        // Check for form data
        if (isset($_POST) && wp_verify_nonce($_POST['security'], 'session_nonce'))
        {
            $IsNewRecord = false;

            // Get the values from the form
            parse_str($_POST['form_data'], $FormData);

            // Create a person object
            $Person = new Person();

            // Assign values to the person object
            foreach ($FormData as $Key => $Value)
            {
                if (property_exists($Person, $Key))
                {
                    if (!empty($Value))
                    {
                        $Person->$Key = $Value;
                    }
                    else
                    {
                        $Person->$Key = "#BLANK#";
                    }
                }
            }

            $Date = DateTime::createFromFormat('Y-m-d', $Person->BirthDate);

            if ($Date)
            {
                $Person->BirthDate = $Date->format('m-d-Y');
            }


            // Choose if we are going to update the record or create a new record
            if ($Person->ID !== "#BLANK#")
            {
                // Update the record in AM.Net
                $ReturnedPerson = $Person->Update($Person->ID,$Person);
            }
            else
            {
                // Remove the ID property so we don't have errors
                unset($Person->ID);
                // Create a record in AM.Nte
                $ReturnedPerson = $ReturnedPerson = $Person->Create($Person);
            }

            // Handle the API errors
            if (is_wp_error($ReturnedPerson))
            {
                $ErrorCode = $ReturnedPerson->get_error_code();
                $ErrorMessage = $ReturnedPerson->get_error_message();

                switch($ErrorCode){
                    case 89:
                        $Alert = new Alerts('Error',"alert-box", "We've detected an existing account.", "One or more of the email addresses used is already attached to an existing account. <br><br><a href= " . wp_lostpassword_url() . ">Click here</a> to reset your password.", false);
                        $Alert = $Alert->Content;
                        break;
                    default:
                    $Alert = new Alerts('Error',"alert-box", "The server returned error code " . $ErrorCode .".", "The error returned is " . $ErrorMessage .". This error has been logged and sent to support.", false);
                    $Alert = $Alert->Content;
                }

                wp_send_json_error($Alert);
                exit;
            }

            $Person = new Person();

            // Try to update storage
            if (is_wp_error($WPUserID = $Person->import($ReturnedPerson->ID)))
            {
                $Error = new WP_Error('500','Storage Update Failed for '.$ReturnedPerson->ID , (array)$FormData);
                new Logging($Error);
            }

            // Update password if needed
            if (!empty($FormData['Password']) && !empty($WPUserID))
            {
                wp_set_password($FormData['Password'], $WPUserID);
            }

            //Log the user out and back in to refresh the settings.
            $Login = new Login();
            $Login->programmatic_login($WPUserID);

            wp_send_json_success();
            exit;
        }
        else
        {
            $Alert = new Alerts('Error', "alert-box", "Oh no, something went wrong!", "The security token used for form access expired. Normally reloading the page will correct the issue.", false);
            wp_send_json_error($Alert->Content);
            exit;
        }
    }

    /**
     * Show the profile form
     * @return void
     */
    public  function show_profile()
    {
        // If the user is logged in we should load the value for the user
        if (is_user_logged_in())
        {
            $Action = get_query_var('action');
            // Yes get the person object
            $PersonID = get_user_meta(get_current_user_id(), "cdsId", true);
            $Person = new Person($PersonID);
        }

        // Check to see if this is a password reset.
        if (get_query_var('action') == 'reset' && !empty(get_query_var('login_guid')))
        {
            $Action = 'reset';
            // Get the id from the query
            $LoginGuid = get_query_var('login_guid');
            // Get the transient and return the user id
            $WPUserId = get_transient($LoginGuid);
            // Get a person storage object
            $Person = new Person();
            $Person = $Person->get_person_by_wordpress_id($WPUserId);

            // Kill the transient.
            delete_transient($LoginGuid);
        }

        // We can't find a user so assume this is a new user.
        if (empty($Person->ID))
        {
            $Action = 'reset';
            // No build a blank person option
            $Person = new Person();
        }

        if (get_query_var('action') == 'complete')
        {
            $Action = 'complete';
        }

        $this->enqueue_scripts();

        $Args = array('action' => $Action, 'person' => $Person,);

        $Template = new Template('Profile.php', $Args);
    }

    public static function build_success_alert(){
        $WPUser = wp_get_current_user();
        $Alert = new Alerts('Success',"alert-box","Your update completed successfully.", get_user_meta( $WPUser->ID, 'nickname', true ) .", you are logged in as ". $WPUser->user_login.".<br><br> You may continue with updates or browse the site.");
        return $Alert->Content;
    }


    // Add the query vars to global so they can be called in page.
    public  function add_query_vars_filter($vars)
    {
        $vars[] = "action";
        $vars[] = "login_guid";
        return $vars;
    }
}