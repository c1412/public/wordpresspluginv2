<?php
namespace cds\wordpress;

//If called directly exit
defined('ABSPATH') or exit;

$Paywall = new Paywall();
add_action('template_redirect', array($Paywall, 'check_user_access'));

class Paywall
{
    /**
     * Reads page restrictions and checks them against the users roles.
     * @return void
     */

    public  function check_user_access()
    {
        // Get the Post ID and any associated restrictions
        if (!empty($WPPostID = get_the_ID()) && !empty($PaywallValue = get_post_meta($WPPostID, '_cdsPaywallMetaKey', true)))
        {
            // The page has restrictions so we need to to ensure the use has logged in.
            if (is_user_logged_in())
            {
                // User is logged in, check to see if this page has a member restriction.
                if ($PaywallValue = 'member')
                {

                    // Member restriction is active, get the user roles for the current user.
                    $WPUser = wp_get_current_user();
                    $Roles = (array)$WPUser->roles;

                    // Does the user have a member or administrator role?
                    if (!in_array('member', $Roles) && !in_array('administrator', $Roles))
                    {
                        // Checking to see if we have set a custom redirect page. If not fallback to login page
                        if (!empty($cdsLoginMembershipRequired = get_option('cdsLoginMembershipRequired'))) {
                            $LoginUrl = get_page_link($cdsLoginMembershipRequired);
                        }
                        else
                        {
                            $LoginUrl = wp_login_url();
                        }

                        $LoginUrl = add_query_arg('post', urlencode(get_the_ID()), $LoginUrl);
                        $LoginUrl = add_query_arg('notice', urlencode("Member Only Access"), $LoginUrl);
                        $LoginUrl = remove_query_arg('redirect_to',$LoginUrl);

                        wp_redirect($LoginUrl);
                    }
                }
            }
            else
            {
                // Redirect to login
                $LoginUrl = wp_login_url();;
                $LoginUrl = add_query_arg('post', urlencode(get_the_ID()), $LoginUrl);
                $LoginUrl = add_query_arg('notice', urlencode("Login Required"), $LoginUrl);
                $LoginUrl = remove_query_arg('redirect_to',$LoginUrl);

                wp_redirect($LoginUrl);
            }
        }
    }
}