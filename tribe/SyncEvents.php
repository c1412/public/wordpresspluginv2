<?php

namespace cds\tribe;

use cds\api\Event;
use cds\api\Firm;
use cds\api\Lists;
use cds\api\Logging;
use cds\api\Person;
use cds\options\Cron;
use DateTime;
use WP_Error;
use WP_Query;


defined('ABSPATH') or exit;

class SyncEvents
    {

    /**
     * Get a list of dirty event IDs from local storage.
     * @return array
     */
    public function get_events(): array
        {

        global $wpdb;

        $Results = null;
        // The SQL query
        $Query = $wpdb->prepare("SELECT ID FROM cds_events WHERE IsDirty = %s", array('1'));

        // The results
        $Results = $wpdb->get_results($Query);

        // Create array of Code Year values
        return array_unique(array_column($Results, 'ID'));

        }

    /**
     * Schedule a cron job to update Tribe Event Data.
     * @return void
     */
    public function schedule_updates(): void
        {
        $Events = $this->get_events();

        // Fire-up cron
        $Cron = new Cron();

        // If there are events to process and we are outside business hours run process.
        if ($Events)
            {
            // Check that we are not running the primary event sync, if we are then stall.
            if (!empty(wp_next_scheduled('CDS_Populate_Events_Batch')) && time() > wp_next_scheduled('CDS_Populate_Batch'))
                {
                // Schedule the cron job
                // TODO: Uncomment Below
                $Cron->create_cron_batch($Events, 'CDS_Populate_Tribe_Events');
                // Update the last synced datetime.
                update_option('cdsSyncedTribe', current_time('Y-m-d\TH:i'));
                }
            }
        }

    /**
     * Insert or update a Tribe Event Listing
     * @param string $CodeYear
     * @return void
     */
    public function import(string $CodeYear): void
        {

        // Get the AM.Net event object
        $Event = new Event($CodeYear);

        $EventDate = new DateTime($Event->EndDate);
        $CurrentDate = new DateTime();

        // Tribe is used to display courses available for registration, historic events should not be populated.
        if ($EventDate >= $CurrentDate)
            {
            // Set the values to query Tribe Events
            $QueryArgs = array(
                'post_type'        => 'tribe_events',
                'suppress_filters' => true,
                'meta_key'         => '_cdsCodeYear',
                'meta_query'       => array(
                    'key'   => '_cdsCodeYear',
                    'value' => str_replace(' ', '', $Event->CodeYear),
                )
            );

            // Query Tribe Events
            $TribeEventQuery = new WP_Query($QueryArgs);

            //Did we find an existing event?
            if ($TribeEventQuery->have_posts())
                {
                $PostID = $TribeEventQuery->posts[0]->ID;
                $CustomFields = tribe_get_option('custom-fields');

                // Check for Locked
                // TODO: Remove loop use search
                foreach ($CustomFields as $Field)
                    {
                    if ($Field === 'Locked')
                        {
                        $EventIsLocked = get_post_meta($PostID, $Field['name'], True);
                        }
                    }
                }

            if ($EventIsLocked !== 'yes')
                {
                // Code to update or delete an event
                $this->populate_tribe_event($Event, $PostID);
                }
            else
                {
                // Event is locked, keep it locked and skip the update.
                $this->add_custom_field($PostID, 'yes', 'Locked', 'checkbox', array('yes'));
                }
            }
        else
            {
            // Old event don't sync it
            global $wpdb;
            $wpdb->update('cds_events', array('IsDirty' => 0), array('ID' => str_replace(' ', '', trim($Event->CodeYear))), '%s', '%s');
            }
        }

    /**
     * Copy an event from local storage into tribe events
     * @param Event $Event
     * @param mixed $PostID
     * @return void
     */
    protected function populate_tribe_event(Event $Event, $PostID = null)
        {

        global $wpdb; // Used to update local storage.

        $List = new Lists(); // Used later to get code descriptions

        // NOTE: AM.Net stores these fields as strings ang without validation.
        // Start Time - For the event
        $EventStartTime = empty($Event->BeginTimeDay1) ? '12:00 PM' : $Event->BeginTimeDay1;
        preg_match("/([0-9]{1,2}):([0-9]{1,2})([a-zA-Z]+)/", preg_replace('/\s+/', '', $EventStartTime), $StartTime);

        // End Time - End Time for the event
        $EventEndTime = ($Event->BeginDate === $Event->EndDate) ? $Event->EndTimeDay1 : $Event->EndTimeDay2;
        $EventEndTime = empty($EventEndTime) ? '1:00 PM' : $EventEndTime;
        preg_match("/([0-9]{1,2}):([0-9]{1,2})([a-zA-Z]+)/", preg_replace('/\s+/', '', $EventEndTime), $EndTime);


        // Set the initial values for the event.
        $TribeEvent = [
            'post_title'         => $Event->Title . ' (' . $Event->Code . '/' . $Event->Year . ')',
            'post_excerpt'       => wp_trim_words(wp_kses_post($Event->MarketingDescription) . 55, '...'),
                                                                                                         // Trim the marketing copy to the excerpt.
            'post_content'       => $this->get_content($Event),
            'post_status'        => 'publish',
            'post_author'        => 1,
            'EventStartDate'     => DateTime::createFromFormat('Y-m-d H:i:s', str_replace('T', ' ', $Event->BeginDate))->format('Y-m-d'),
            'EventEndDate'       => DateTime::createFromFormat('Y-m-d H:i:s', str_replace('T', ' ', $Event->EndDate))->format('Y-m-d'),
            'EventStartHour'     => $StartTime[1],
            'EventStartMinute'   => $StartTime[2],
            'EventStartMeridian' => $StartTime[3],
            'EventEndHour'       => $EndTime[1],
            'EventEndMinute'     => $EndTime[2],
            'EventEndMeridian'   => $EndTime[3],
            'EventCost'          => $this->get_event_cost($Event->Fees),
            'Venue'              => $this->get_venue($Event->FacilityLink)
        ];

        // Are we inserting or updating?
        if (empty($PostID))
            {
            $PostID = tribe_create_event($TribeEvent);
            }
        else
            {
            $PostID = tribe_update_event($PostID, $TribeEvent);
            }

        // Error check before we continue
        if (empty($PostID))
            {
            $Error = new WP_Error('Tribe Insert Failed For: ' . $Event->CodeYear, __METHOD__, (array) $TribeEvent);
            new Logging($Error);
            }
        else
            {
            // Link the AM.Net CodeYear value to Tribe Event Post Meta
            update_post_meta($PostID, '_cdsCodeYear', str_replace(' ', '', trim($Event->CodeYear)));
            // Update Local Storage
            $wpdb->update('cds_events', array('IsDirty' => 0), array('ID' => str_replace(' ', '', trim($Event->CodeYear))), '%s', '%s');
            }

        // Are we hiding this event from the website?
        if ($Event->ExcludeFromInternalCatalog || $Event->ExcludeFromWebsite || $Event->StatusCode === 'C')
            {
            // Hide the event
            delete_post_meta($PostID, '_EventOrganizerID');
            delete_post_meta($PostID, $this->get_custom_field_name('Chapter'));
            delete_post_meta($PostID, $this->get_custom_field_name('Course Level'));
            delete_post_meta($PostID, $this->get_custom_field_name('Credit Hours'));
            update_post_meta($PostID, '_EventHideFromUpcoming', 'yes');
            }
        else
            {
            //Show the event
            wp_set_object_terms($PostID, $this->get_terms($Event->CompanyCode, $Event->ExternalEventCode2), 'tribe_events_cat', false);
            wp_set_post_tags($PostID, $this->get_tags($Event->Credits), false);
            $this->add_custom_field($PostID, '', 'Redirect URL', 'text');
            $this->add_custom_field($PostID, $Event->CpeBankEligible, 'CPE Bank', 'checkbox', array('true', 'false'));
            $this->add_custom_field($PostID, $Event->NewEvent, 'New Event', 'checkbox', array('true', 'false'));
            $this->add_custom_field($PostID, $Event->CreditHours, 'Credit Hours', 'text');
            $this->add_custom_field($PostID, $Event->MaximumRegistrations, 'Max Registrations', 'text');
            $this->add_custom_field($PostID, $List->get_item_description('EVLV', $Event->LevelCode) ?: 'Basic', 'Course Level', 'text');
            //NOTE:: SCACPA Specific Could be handled through user training
            $this->add_custom_field($PostID, in_array($Event->CompanyCode, array('WEBCA', 'LIVES', 'WEBIA', 'WEBIN', '00005', 'ONDEM', true)) ? '' : $List->get_item_description('EVLO', $Event->FacilityLocationFirmCode), 'Chapter', 'text');

            // If a facility link exists add the map features.
            if ($Event->FacilityLink)
                {
                update_post_meta($PostID, '_EventShowMap', true);
                update_post_meta($PostID, '_EventShowMapLink', true);
                }
            }

        // Set the organizers
        $OrganizerIDs = $this->get_organizer($Event->Leaders);
        delete_post_meta($PostID, '_EventOrganizerID');

        foreach ($OrganizerIDs as $ID)
            {
            add_post_meta($PostID, '_EventOrganizerID', $ID);
            }
        }

    /**
     * Returns the format of an event.
     * @param string $event_company
     * @param string $external_eventCode2
     * @return mixed
     */
    //NOTE:: SCACPA Specific Code
    public function get_terms(string $event_company, string $external_eventCode2)
        {
        // First we need to map our AM.Net categories into more meaningful web filters.
        $event_type = '';
        if ($event_company == 'LIVES' || $event_company == 'WEBCA' || $event_company == 'WEBIA' || $event_company == 'WEBIN' || $event_company == '00005')
            {
            $event_type = 'Online-Video';
            }
        else if ($event_company == 'ONDEM')
            {
            //If the event is self study what type of self study is it?
            if ($external_eventCode2 == 'SSWC')
                {
                $event_type = 'Self-Study Video';
                }
            else
                {
                $event_type = 'Self-Study Text';
                }
            }
        else
            {
            $event_type = 'In-Person';
            }

        $existing_term = get_term_by('name', $event_type, 'tribe_events_cat', OBJECT);

        if (!$existing_term)
            {
            $new_term = (object) wp_insert_term($event_type, 'tribe_events_cat', $args = array('description' => $event_type, str_replace(' ', '_', $event_type, )));
            $term_id = $new_term->term_id;
            }
        else
            {
            $term_id = $existing_term->term_id;
            }

        return $term_id;
        }

    /**
     * Gets a list of CPE credit types
     * @param array $Credits
     * @return null|string
     */
    public function get_tags(array $Credits)
        {

        $List = new Lists();
        $CreditTypes = null;
        $i = 0;

        if (!empty($Credits))
            {
            foreach ($Credits as $Credit)
                {

                if ($i > 0)
                    {
                    $CreditTypes = $CreditTypes . ",";
                    }

                $CreditTypes = $CreditTypes . $List->get_item_description('EVCC', $Credit->CreditCategoryCode);
                $i++;
                }
            }

        return ($CreditTypes);
        }

    /**
     * Add a Custom Field to Tribe Event Post
     * @param int $PostID
     * @param mixed $FieldValue
     * @param string $FieldLabel
     * @param string $FieldType
     * @param array $FieldOptions
     * @return mixed
     */
    public function add_custom_field(int $PostID, $FieldValue, string $FieldLabel, string $FieldType, array $FieldOptions = array())
        {

        $FieldExists = false;
        $CustomFields = tribe_get_option('custom-fields') ?: array();

        // Does the field exist?
        foreach ($CustomFields as $Field)
            {
            if ($Field['label'] === $FieldLabel)
                {
                $FieldExists = true;
                $Name = $Field['name'];
                }
            }

        // Field does not exist
        if ($FieldExists === false)
            {
            $index = count($CustomFields) + 1;

            $CustomFields[] = array(
                'name'   => "_ecp_custom_$index",
                'label'  => $FieldLabel,
                'type'   => $FieldType,
                'values' => $FieldValue
            );

            $Name = "_ecp_custom_$index";

            tribe_update_option('custom-fields', $CustomFields);
            }

        update_post_meta($PostID, $Name, $FieldValue);

        }

    /**
     * Get the value of a Tribe Event Custom Field
     * @param string $FieldLabel
     * @return mixed
     */
    public function get_custom_field_name(string $FieldLabel)
        {

        $CustomFields = tribe_get_option('custom-fields');
        $Name = '';

        foreach ($CustomFields as $Field)
            {
            if ($Field['label'] === $FieldLabel)
                {
                $Name = $Field['name'];
                }
            }

        return $Name;
        }

    /**
     * Creates or returns the organizers for an event
     * @param array $Leaders
     * @return array
     */
    public function get_organizer(array $Leaders): array
        {
        $OrganizerIDs = array();
        foreach ($Leaders as $Leader)
            {

            $OrganizerData = new Person($Leader->NamesId);
            $NewOrganizer = array();

            if (!empty($OrganizerData))
                {

                $CPA = ', CPA';

                if ($OrganizerData->CertifiedCode == 'N' || $OrganizerData->CertifiedCode == '')
                    {
                    $CPA = '';
                    }

                $Credentials = '';

                if (!empty($OrganizerData->Credentials))
                    {
                    $Credentials = ', ' . $OrganizerData->Credentials;
                    }

                $FormattedLeader = $OrganizerData->FirstName . ' ' . $OrganizerData->MiddleInitial . ' ' . $OrganizerData->LastName . $CPA . $Credentials;

                $NewOrganizer['Organizer'] = $FormattedLeader;
                $NewOrganizer['Email'] = $OrganizerData->Email;
                $NewOrganizer['Website'] = '';
                $NewOrganizer['Phone'] = '';
                $NewOrganizer['post_content'] = $OrganizerData->LeaderBio; //wp_kses_post($OrganizerData->LeaderBio);
                }

            $ExistingOrganizer = get_page_by_title($NewOrganizer['Organizer'], OBJECT, 'tribe_Organizer');

            if (empty($ExistingOrganizer))
                {
                $OrganizerID = tribe_create_organizer((array) $NewOrganizer);
                }
            else
                {
                $OrganizerID = tribe_update_organizer($ExistingOrganizer->ID, (array) $NewOrganizer);
                }

            array_push($OrganizerIDs, $OrganizerID);
            }

        return $OrganizerIDs;
        }

    //Return a Tribe Event venue based in an AMnet Firm ID.
    public function get_venue(string $FirmID)
        {
        if (!empty($FirmID))
            {

            $Firm = new Firm(str_replace(' ', '', $FirmID));
            $Venue = array();
            $Venue['ID'] = '';

            if (!empty($Firm->IndexName))
                {

                $Venue['Venue'] = str_replace(' & ', ' and ', $Firm->IndexName);
                $Venue['Country'] = 'United States';
                $Venue['Address'] = $Firm->Address->Line1;
                $Venue['City'] = $Firm->Address->City;
                $Venue['State'] = $Firm->Address->StateCode;
                $Venue['Zip'] = $Firm->Address->Zip;
                $Venue['Phone'] = $Firm->Phone;
                }

            $ExistingVenue = get_page_by_title(str_replace(' & ', ' and ', $Firm->IndexName), OBJECT, 'tribe_venue');

            if (empty($ExistingVenue) && !empty($Venue))
                {
                $VenueID = tribe_create_venue((array) $Venue);
                }
            elseif (!empty($Venue))
                {
                $VenueID = tribe_update_venue($ExistingVenue->ID, (array) $Venue);
                }

            return array('VenueID' => $VenueID);
            }
        return null;
        }

    /**
     * Building the marketing copy for a tribe event
     * @param Event $Event
     * @return string
     */
    public function get_content(Event $Event)
        {
        $pay_load = '';
        if (trim($Event->MarketingDescription))
            {
            $pay_load = $pay_load . '<h1>Event Description</h1>';
            $pay_load = $pay_load . wp_kses_post($Event->MarketingDescription);
            }
        if (trim($Event->DesignedFor))
            {
            $pay_load = $pay_load . '<h1>Designed For</h1>';
            $pay_load = $pay_load . wp_kses_post($Event->DesignedFor);
            }
        if (trim($Event->Objectives))
            {
            $pay_load = $pay_load . '<h1>Objectives</h1>';
            $pay_load = $pay_load . wp_kses_post($Event->Objectives);
            }
        if (Trim($Event->MajorSubjects))
            {
            $pay_load = $pay_load . '<h1>Major Subjects</h1>';
            $pay_load = $pay_load . wp_kses_post($Event->MajorSubjects);
            }
        if (trim($Event->Prerequisites))
            {
            $pay_load = $pay_load . '<h1>Prerequisites</h1>';
            $pay_load = $pay_load . wp_kses_post($Event->Prerequisites);
            }
        if (trim($Event->AdvancedPreparation))
            {
            $pay_load = $pay_load . '<h1>Instructions</h1>';
            $pay_load = $pay_load . wp_kses_post($Event->AdvancedPreparation);
            }

        return $pay_load;
        }

    /**
     * Return the Tribe Event cost from an AMnet free array.
     * //TODO: SCACPA Specific can replace to endpoint call with non-member account for highest fee.
     * @param mixed $Fees
     * @return mixed
     */
    public function get_event_cost($Fees)
        {
        $Code = 'SF';
        foreach ($Fees as $Fee)
            {
            if ($Fee->Ty2 == $Code)
                {
                return $Fee->Amount;
                }
            }
        }
    }