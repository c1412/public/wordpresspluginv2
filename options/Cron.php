<?php

namespace cds\options;

use cds\api\Event;
use cds\api\Firm;
use cds\api\Lists;
use cds\api\Person;
use cds\tribe\SyncEvents;
use DateTime;
use DateTimeZone;

// Cron jobs specific to this plugin
$Cron = new Cron();
add_filter('cron_schedules', array($Cron, 'add_schedules'), 10, 1);

// Base cron functions
$Cron = new Cron();

// Setting up list synchronization
$Lists = new Lists();
add_action('CDS_Check_Lists', array($Lists, 'schedule_updates'));
add_action('CDS_Populate_List', array($Lists, 'import'), 20, 1);
add_action('CDS_Populate_List_Batch', array($Cron, 'process_cron_batch'), 20, 3);

$Firm = new Firm();
add_action('CDS_Check_Firms', array($Firm, 'schedule_updates'));
add_action('CDS_Populate_Firm', array($Firm, 'import'), 15, 1);
add_action('CDS_Populate_Firm_Batch', array($Cron, 'process_cron_batch'), 15, 3);

$Person = new Person();
add_action('CDS_Check_Persons', array($Person, 'schedule_updates'));
add_action('CDS_Populate_Person', array($Person, 'import'), 15, 1);
add_action('CDS_Populate_Person_Batch', array($Cron, 'process_cron_batch'), 15, 3);

$Event = new Event();
add_action('CDS_Check_Events', array($Event, 'schedule_updates'));
add_action('CDS_Populate_Events', array($Event, 'import'), 15, 1);
add_action('CDS_Populate_Events_Batch', array($Cron, 'process_cron_batch'), 15, 3);

$Tribe = new SyncEvents();
add_action('CDS_Check_Tribe', array($Tribe, 'schedule_updates'));
add_action('CDS_Populate_Tribe_Events', array($Tribe, 'import'), 15, 1);
add_action('CDS_Populate_Tribe_Events_Batch', array($Cron, 'process_cron_batch'), 15, 3);

class Cron
    {
    private string $CurrentTime;
    private string $OpenTime;
    private string $CloseTime;
    private string $TimeUntilClose;
    public bool $DoingBusiness;
    private int $ChunkSize = 100; // NOTE: Consider adding this to settings.

    public function __construct()
        {
        $this->set_times();
        }

    /**
     * Used to create batches for updates, ensure we can throttle as needed.
     * @param array $Keys
     * @param string $SingularEventName
     * @return void
     */
    public function create_cron_batch(array $Keys, string $SingularEventName): void
        {

        // Split the array into chunks
        $Chunks = array_chunk($Keys, $this->ChunkSize);

        // Set the time between chunks
        $ChunkDelay = $this->ChunkSize * 3;

        // Keep track of the number of chunks we are processing used for timing
        $Count = 0;

        $BatchEventName = $SingularEventName . '_Batch';
        // Loop through the chunks
        foreach ($Chunks as $Chunk)
            {
            wp_schedule_single_event(
                time() + $ChunkDelay * $Count,
                $BatchEventName,
                array(
                        array($Chunk),
                        $SingularEventName,
                        $BatchEventName
                )
            );
            //Increase the count.
            $Count++;
            }
        }

    /**
     * Processing a batch of items
     * @param array $Chunk
     * @param string $SingularEventName
     * @param string $BatchEventName
     * @return void
     */
    public function process_cron_batch(array $Chunk, string $SingularEventName, string $BatchEventName): void
        {
        // Do we already have another set of processes running? If so reschedule so we don't stack.
        if (!empty(wp_next_scheduled($SingularEventName)) && time() > wp_next_scheduled($SingularEventName))
            {

            wp_schedule_single_event(
                time() + $this->ChunkSize,
                $BatchEventName,
                array(
                        $Chunk,
                        $SingularEventName,
                        $BatchEventName
                )
            );
            }
        else
            {
            // Keep track of the number of chunks we are processing used for timing
            $Count = 0;
            // Loop through the chunks
            foreach ($Chunk[0] as $Key)
                {
                wp_schedule_single_event(
                    time() + $Count * 2,
                    $SingularEventName,
                    array(trim($Key))
                );

                //Increase the count.
                $Count++;
                }
            }
        }

    /**
     * Used to reschedule cron jobs that should not run during business hours.
     * @param string $hook_name
     * @return bool
     */
    public function reschedule_hook(string $hook_name): bool
        {
        if ($this->DoingBusiness)
            {
            wp_clear_scheduled_hook($hook_name);
            wp_schedule_event(time() + $this->TimeUntilClose, 'daily', $hook_name);
            return true;
            }
        else
            {
            return false;
            }
        }

    /**
     * Sets the business operating hours.
     * @return void
     */
    private function set_times()
        {
        $TimeZone = new DateTimeZone(wp_timezone_string());

        $CurrentTime = new DateTime('now', $TimeZone);
        $this->CurrentTime = strtotime($CurrentTime->format('g:i a'));

        $OpenTime = new DateTime(get_option('cdsOpenTime', wp_timezone_string()), $TimeZone);
        $this->OpenTime = strtotime($OpenTime->format('g:i a'));

        $CloseTime = new DateTime(get_option('cdsCloseTime', wp_timezone_string()), $TimeZone);
        $this->CloseTime = strtotime($CloseTime->format('g:i a'));

        $this->TimeUntilClose = $this->CloseTime - $this->CurrentTime;

        if ($this->CurrentTime > $this->OpenTime && $this->CurrentTime < $this->CloseTime)
            {
            $this->DoingBusiness = true;
            }
        else
            {
            $this->DoingBusiness = false;
            }
        }

    public function schedule()
        {
        // PHIL: This endpoint does not have a change so we will call it weekly
        if (!wp_next_scheduled('CDS_Check_Lists'))
            {
            wp_schedule_event(time(), 'weekly', 'CDS_Check_Lists');
            }
        if (!wp_next_scheduled('CDS_Check_Firms'))
            {
            wp_schedule_event(time(), 'cds_thirty_minutes', 'CDS_Check_Firms');
            }
        if (!wp_next_scheduled('CDS_Check_Persons'))
            {
            wp_schedule_event(time(), 'cds_one_minute', 'CDS_Check_Persons');
            }
        if (!wp_next_scheduled('CDS_Check_Events'))
            {
            wp_schedule_event(time(), 'cds_fifteen_minutes', 'CDS_Check_Events');
            }
        if (!wp_next_scheduled('CDS_Check_Tribe'))
            {
            wp_schedule_event(time(), 'cds_fifteen_minutes', 'CDS_Check_Tribe');
            }
        }

    public function add_schedules($Schedules)
        {
        $Schedules['cds_one_minute'] = array(
            'interval' => 60,
            'display'  => __('One Minute')
        );

        $Schedules['cds_five_minutes'] = array(
            'interval' => 300,
            'display'  => __('Five Minutes')
        );

        $Schedules['cds_ten_minutes'] = array(
            'interval' => 600,
            'display'  => __('Ten Minutes')
        );

        $Schedules['cds_fifteen_minutes'] = array(
            'interval' => 900,
            'display'  => __('Fifteen Minutes')
        );

        $Schedules['cds_thirty_minutes'] = array(
            'interval' => 1800,
            'display'  => __('Thirty Minutes')
        );

        $Schedules['cds_forty_five_minutes'] = array(
            'interval' => 2100,
            'display'  => __('Forty-Five Minutes')
        );

        return $Schedules;
        }
    }