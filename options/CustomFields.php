<?php

namespace cds\logon\options;

defined('ABSPATH') or exit;

$CustomFields = new CustomFields();

add_action('edit_user_profile', array($CustomFields, 'paywall_save_post_data'));
add_action('edit_user_profile', array($CustomFields, 'add_custom_user_fields'));
add_action('save_post', array($CustomFields, 'paywall_save_post_data'), 10, 2);
add_action('add_meta_boxes', array($CustomFields, 'paywall_add_custom_box'));
add_action('add_meta_boxes', array($CustomFields, 'tribe_add_codeyr'));
add_action('save_post', array($CustomFields, 'tribe_events_save_post_data'), 10, 2);

//NOTE:: We can do better here
class CustomFields
    {
    /**
     * Adding the custom roles for member and non-member accounts
     * @return void
     * */
    public function add_custom_roles(): void
        {
        add_role('member', 'Member', get_role('subscriber')->capabilities);
        add_role('non-member', 'Non-Member', get_role('subscriber')->capabilities);
        }

    /**
     * Adding a AMNet ID to user profile
     * @param mixed $user
     * @return void
     */
    public function add_custom_user_fields($user): void
        {

        if (empty($value = (get_user_meta($user->ID, 'cdsId', true))))
            {
            $value = 'Unlinked';
            }

        ?>
        <table id='cds' class='form-table' role='presentation'>
            <tbody>
                <tr id='cdsId' class='user-user-login-wrap'>
                    <th><label for='cdsId'>CDS AM.Net ID</label></th>
                    <td><input type='text' class='input-text form-control' name='cdsId' id='cdsId' value=<?php echo ($value) ?>
                            readonly /></td>
                </tr>
            </tbody>
        </table>
        <script>
            jQuery('#cds').insertBefore('form').first('table')
        </script>
    <?php
        }


    /**
     * Building a custom meta box Tribe Events
     * @return void  */
    public function tribe_add_codeyr()
        {

        add_meta_box(
            'cds_codeyr',
            'AM.Net Event Code Year',
            array(__NAMESPACE__ . '\\CustomFields', 'tribe_events_html'),
            'tribe_events',
            'side',
            'high'
        );
        }

    /**
     * Building a custom meta box Tribe Events
     * @return void  */
    public static function tribe_events_html($post)
        {

        $value = get_post_meta($post->ID, '_cdsCodeYear', true);

        ?>

        <div class="ajaxtag hide-if-no-js">
            <input name="cdsCodeYrField" id="cdsCodeYrField" type="text" value="<?php echo($value) ?>" class="newtag form-input-tip ui-autocomplete-input" readonly>
        </div>
        <?php
        }

    /**
     * Save paywall meta data to post
     * @param mixed $post_id
     * @return void
     */
    public function tribe_events_save_post_data($post_id)
        {
        update_post_meta(
            $post_id,
            '_cdsCodeYear',
            $_POST['cdsCodeYrField']
        );
        }



    /**
     * Building a custom meta box on pages and posts
     * @return void  */
    public function paywall_add_custom_box()
        {
        $post_types = ['post', 'page'];
        foreach ($post_types as $type)
            {
            add_meta_box(
                'cds_paywall_box_id',
                'Restrict Access',
                array(__NAMESPACE__ . '\\CustomFields', 'paywall_custom_box_html'),
                $type,
                'side',
                'high'
            );
            }
        }

    /**
     * Creating the options for the custom meta boxes
     * @param mixed $post
     * @return void
     */
    public static function paywall_custom_box_html($post)
        {
        $value = get_post_meta($post->ID, '_cdsPaywallMetaKey', true);

        ?>
        <label for="cdsPaywallField">Page Restrictions</label>
        <select name="cdsPaywallField" id="cdsPaywallField" class="postbox">
            <option value="">No Restrictions</option>
            <option value="login" <?php selected($value, 'login') ?>;>Login Required</option>
            <option value="member" <?php selected($value, 'member') ?>>Member Only</option>
        </select>
    <?php
        }

    /**
     * Save paywall meta data to post
     * @param mixed $post_id
     * @return void
     */
    public function paywall_save_post_data($post_id)
        {
        update_post_meta(
            $post_id,
            '_cdsPaywallMetaKey',
            $_POST['cdsPaywallField']
        );
        }
    }