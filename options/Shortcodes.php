<?php

namespace cds\options;

use cds\debug\Debug;
use cds\wordpress\Login;
use cds\wordpress\Password;
use cds\wordpress\Profile;

defined('ABSPATH') or exit;

class Shortcodes
    {

    }
$Login = new Login();
add_shortcode('LOGIN', array($Login, 'show_login'));

$Password = new Password();
add_shortcode('PASSWORD_RESET', array($Password, 'show_password_reset'));

$Profile = new Profile();
add_shortcode('PROFILE', array($Profile, 'show_profile'));

$Debug = new Debug();
add_shortcode('DEBUG', array($Debug, 'go'));