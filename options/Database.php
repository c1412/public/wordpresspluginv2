<?php

namespace cds\options;

//If called directly exit
defined('ABSPATH') or exit;

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

class Database
    {

    /**
     * Creating the names table
     * @return void
     */
    public function create_table_persons(): void
        {

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = " CREATE TABLE cds_persons (
            ID varchar(8) NOT NULL,
            LinkedFirmCode varchar(8) DEFAULT NULL,
            WordPressUserID varchar(8) DEFAULT NULL,
            FirstName varchar(15) DEFAULT NULL,
            MiddleInitial varchar(15) DEFAULT NULL,
            LastName varchar(20) DEFAULT NULL,
            Suffix varchar(10) DEFAULT NULL,
            SalutationFirst varchar(15) DEFAULT NULL,
            NickName varchar(15) DEFAULT NULL,
            Credentials varchar(50) DEFAULT NULL,
            Email varchar(50) NOT NULL,
            EmailAddress2 varchar(50) DEFAULT NULL,
            MemberTypeCode varchar(5) DEFAULT NULL,
            MemberTypeDescription varchar(50) DEFAULT NULL,
            MemberStatusCode varchar(5) NOT NULL,
            MemberStatusDescription varchar(50) NOT NULL,
            PreferredChapterCode varchar(5) DEFAULT NULL,
            ActualChapterCode varchar(5) DEFAULT NULL,
            MobilePhone varchar(12) DEFAULT NULL,
            HomePhone varchar(12) DEFAULT NULL,
            DirectPhone varchar(12) DEFAULT NULL,
            HomeAddressLine1 varchar(50) DEFAULT NULL,
            HomeAddressLine2 varchar(50) DEFAULT NULL,
            HomeAddressCity varchar(50) DEFAULT NULL,
            HomeAddressStateCode varchar(5) DEFAULT NULL,
            HomeAddressStreetZip varchar(50) DEFAULT NULL,
            HomeAddressLon Decimal(9,6) DEFAULT '0.0',
            HomeAddressLat Decimal(8,6) DEFAULT '0.0',
            CongressionalDistrictCode varchar(5) DEFAULT NULL,
            CongressionalRepresentativeID varchar(8) DEFAULT NULL,
            HouseDistrictCode varchar(5) DEFAULT NULL,
            HouseRepresentativeID varchar(8) DEFAULT NULL,
            SenateDistrictCode varchar(5) DEFAULT NULL,
            SenateRepresentativeID varchar(8) DEFAULT NULL,
            LicensedCode varchar(5) DEFAULT NULL,
            InStateCertificateNumber varchar(15) DEFAULT NULL,
            OutOfStateCertificationStateCode varchar(5) DEFAULT NULL,
            OutOfStateCertificateNumber varchar(15) DEFAULT NULL,
            AicpaNumber varchar(15) DEFAULT NULL,
            LeaderBio varchar(12000) DEFAULT NULL,
            InStateCertificationDate datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            OutOfStateCertificationDate datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            JoinDate datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            JoinDate2 datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            BirthDate datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            DuesPaidThrough datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            LastUpdate datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            LastImport datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            LastLogin datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            LastVisit datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            IsFirmAdmin tinyint(1) NOT NULL DEFAULT 0,
            AicpaMember tinyint(1) NOT NULL DEFAULT 0,
            IsSpeaker tinyint(1) NOT NULL DEFAULT 0,
            IsLegislator tinyint(1) NOT NULL DEFAULT 0,
            IsDirty tinyint(1) NOT NULL DEFAULT 1,
            PersonJSON JSON DEFAULT NULL,
            CartJSON JSON DEFAULT NULL,
            PRIMARY KEY (ID),
            UNIQUE KEY Email (Email)
          ) $charset_collate;";

        dbDelta($sql);
        }

    /**
     * Create the amnet_firms table
     * @return void
     */
    public function create_table_firms()
        {

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE cds_firms (
            ID varchar(8) NOT NULL,
            FirmCode varchar(8) NOT NULL,
            Indexname varchar(100) DEFAULT NULL,
            FirmName varchar(256) DEFAULT NULL,
            Phone varchar(12) DEFAULT NULL,
            Fax varchar(12) DEFAULT NULL,
            FirmAdminEmail varchar(50) DEFAULT NULL,
            Website varchar(100) DEFAULT NULL,
            ClientReferralContact1 varchar(50) DEFAULT NULL,
            ClientReferralContact2 varchar(50) DEFAULT NULL,
            ClientReferralYearsInBusiness varchar(100) DEFAULT NULL,
            GeneralBusinessCode varchar(5) DEFAULT NULL,
            SpecificBusinessCode varchar(5) DEFAULT NULL,
            ClientReferralActive tinyint(1) NOT NULL DEFAULT 1,
            LastUpdate datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            LastImport datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            IsDirty tinyint(1) NOT NULL DEFAULT 1,
            FirmJSON JSON DEFAULT NULL,
            MembersJSON JSON DEFAULT NULL,
            AddressJSON JSON DEFAULT NULL,
            PRIMARY KEY (ID)
          ) $charset_collate;";

        dbDelta($sql);
        }

    /**
     * create the amnet-sycod table
     * @return void
     */
    public function create_table_lists()
        {

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE cds_lists (
            ListType varchar(8) NOT NULL,
            ListJSON JSON DEFAULT NULL
          ) $charset_collate;";

        dbDelta($sql);
        }


    }