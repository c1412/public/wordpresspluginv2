<?php

namespace cds\options\models;

defined('ABSPATH') or exit;

class Page
    {
        public ?string $ParentSlug = null;
        public string $PageTitle;
        public ?string $PageText;
        public string $MenuSlug;
        public string $MenuTitle;
        public int|float $Position;

    /**
     * Creates a page object for use.
     * @param string|null $ParentSlug : Left unpopulated this creates a menu item when adding a submenu item the slug name for the parent menu (or the file name of a standard WordPress admin page).
     * @param string $PageTitle : The text to be displayed in the title tags of the page when the menu is selected.
     * @param string $MenuSlug : The text to be used for the menu.
     * @param string $MenuTitle : The slug name to refer to this menu by. Should be unique for this menu and only include lowercase alphanumeric, dashes, and underscores characters to be compatible with sanitize_key() .
     * @param string|null $PageText : The text to be displayed on page to describe the page..
     * @param int|float|null $Position : The position in the menu order this item should appear. Default 200
     */
    public function __construct(?string $ParentSlug = null,
                                string $PageTitle,
                                string $MenuSlug,
                                string $MenuTitle,
                                ?string $PageText = null,
                                int|float $Position = 200
                                )
        {
        $this->ParentSlug = $ParentSlug;
        $this->PageTitle = $PageTitle;
        $this->PageText = $PageText;
        $this->MenuSlug = $MenuSlug;
        $this->MenuTitle = $MenuTitle;
        $this->Position = $Position;
        }

    /**
     * Adds the page to the menu
     * @param Page $Page : The Page object to add.
     * @param null|string $Icon : Optional to add an icon to the menu. Only supported in parent menu pages, default is null,
     * @return void
     */
    public function add(Page $Page, ?string $Icon = null): void
        {
        if (empty($Page->ParentSlug))
            {
            add_menu_page(
                $Page->PageTitle,
                $Page->MenuTitle,
                'manage_options',
                $Page->MenuSlug,
                function () use ($Page)
                    {
                    $this->show_page($Page);
                    },
                $Icon,
                $Page->Position);
            }
        else
            {
            add_submenu_page(
                $Page->ParentSlug,
                $Page->PageTitle,
                $Page->MenuTitle,
                'manage_options',
                $Page->MenuSlug,
                function () use ($Page)
                    {
                    $this->show_page($Page);
                    },
                $Page->Position
            );
            }
        }


    /**
     * Callback for newly created page
     * @param mixed $Page
     * @return void
     */
    public function show_page($Page)
        {

        ?>
        <div class="wrap">
        <div style="background: white; padding: 5px 20px 5px 20px; border-color: #c3c4c7; border-style: solid; border-width: 1px;">
            <h1>
                <?php echo get_admin_page_title() ?>
            </h1>
            <div><?php echo $Page->PageText?></div>
        </div>
        <form style="padding: 10px;background: #f6f7f7;margin-top: 20px;border-style: solid;border-color: #c3c4c7; border-width: 1px;" method="post" action="options.php">
        <?php

            do_settings_sections($Page->MenuSlug,);
            settings_fields($Page->MenuSlug);
            submit_button();
        ?>
        </form>
        </div>
        <?php
        }
    }