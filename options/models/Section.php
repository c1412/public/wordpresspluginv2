<?php

namespace cds\options\models;

defined('ABSPATH') or exit;

class Section
    {
    public string $ID;
    public string $Page;
    public string $Title;
    public ?string $Text;

    /**
     * Used to create a new page section object
     * @param string $ID : Slug-name to identify the section. Used in the 'id' attribute of tags.
     * @param string $Page : The slug-name of the options page on which to show the section.
     * @param string $Title : Formatted title of the section. Shown as the heading for the section.
     * @param null|string $Text : Formatted text for the section. Shown as the description for the section.
     */
    public function __construct(string $ID, string $Page, string $Title, string $Text = null)
        {
        $this->ID = $ID;
        $this->Page = $Page;
        $this->Title = $Title;
        $this->Text = $Text;
        }


    /**
     * Adds a section to a settings page. Uses add_settings_section.
     * @param string|null $HtmlBefore : HTML content to prepend to the section's HTML output. Receives the section's class name as %s.
     * @param string|null $HtmlAfter : HTML content to append to the section's HTML output.
     * @param string|null $Class : The class name to use for the section.
     * @return void
     */
    public function add(Section $Section, string $HtmlBefore = null, string $HtmlAfter = null, string $Class = null)
        {
        add_settings_section(
            $Section->ID,
            $Section->Title,
            function () use ($Section)
                {
                $this->show_section($Section);
                },
            $Section->Page,
            array(
                'before_section' => "<div style='background-color: #ffffff;padding: 10px 10px 10px 10px;border-style: solid; border-color: #c3c4c7; border-width: 1px; margin: 15px 3px 15px 3px'>",
                'after_section'  => "</div>",
                'section_class'  => $Class
            )
            );
        }

    public function show_section($Section)
        {
        echo ("<div>{$Section->Text}</div>");
        }
    }