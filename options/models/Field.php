<?php

namespace cds\options\models;

use cds\helpers\Encryption;
use cds\options\Helpers;

defined('ABSPATH') or exit;

class Field
    {
    public string $ID;
    public string $Title;
    public string $Page;
    public ?string $Section;

    /**
     * Creates a new Field Object
     * @param string $ID : slug for the field
     * @param string $Title : label shown for the field
     * @param string $Page : slug of the containing page
     * @param string|null $Section : slug of the section the field is within default null
     */
    public function __construct(string $ID, string $Title, string $Page, string $Section = null)
        {
        $this->ID = $ID;
        $this->Title = $Title;
        $this->Page = $Page;
        $this->Section = $Section;
        }

    /**
     * Adds a field to a settings page
     * @param string|null $Type : type of field (Ex: Text or URL)
     * @param string|null $Pattern : html validation pattern
     * @param string|null $ToolTip : shown when hovering over the item
     * @param string|null $Description : displayed under the input box
     * @param string|null $PlaceHolder : shows as default text when unpopulated
     * @param int $MinLength : defaults to 0
     * @param int $MaxLength : defaults to 100
     * @param int $Size : defaults to 100
     * @param bool $IsRequired : sets the field required attribute default true
     * @return void
     */
    public function add(Field $Field,
                        string $Type,
                        string $Pattern = '*',
                        string $ToolTip = null,
                        string $Description = null,
                        string $PlaceHolder = null,
                        int $MinLength = 0,
                        int $MaxLength = 100,
                        int $Size = 100,
                        bool $IsRequired = true): void
        {
        // Register the Field
        register_setting($Field->Page, $Field->ID);

        // Check to see if the field should be marked required
        if ($IsRequired)
            {
            $IsRequired = "required";
            }
        else
            {
            $IsRequired = null;
            }

        // Build the arguments used in display
        $Args = [
            'id'          => $Field->ID,
            'type'        => $Type,
            'tooltip'     => $ToolTip,
            'description' => $Description,
            'placeholder' => $PlaceHolder,
            'min_length'  => $MinLength,
            'max_length'  => $MaxLength,
            'size'        => $Size,
            'pattern'     => $Pattern,
            'required'    => $IsRequired,
        ];

        // Add the field
        add_settings_field($Field->ID,
                            $Field->Title,
                            function () use ($Args)
                                {
                                $this->show($Args);
                                },
                            $Field->Page,
                            $Field->Section,
                            $Args
                        );

        }

    /**
     * Callback for Fields::addField()
     * @param mixed $Args
     * @return void
     */
    private function show($Args): void
        {
        echo
            (
            "
            <input
                id = '{$Args['id']}'
                name = '{$Args['id']}'
                type = '{$Args['type']}'
                pattern = '{$Args['pattern']}'
                title = '{$Args['tooltip']}'
                placeholder = '{$Args['placeholder']}'
                minlength = '{$Args['minimum_length']}'
                maxlength = '{$Args['maximum_length']}'
                size = '{$Args['size']}'
                value = '" . get_option($Args['id']) . "'
                {$Args['is_required']}
            ><br>
            <label for='{$Args['id']}'>{$Args['description']}</label><br>"
            );
        }

    /**
     * Adds an encrypted field to a settings page. Use add_filter('pre_update_option_[NAME]', array(__NAMESPACE__ . '\\Helpers', 'encrypt'), 10, 2); to enable encryption.
     * @param string|null $Pattern : html validation pattern
     * @param string|null $ToolTip : shown when hovering over the item
     * @param string|null $Description : displayed under the input box
     * @param string|null $PlaceHolder : shows as default text when unpopulated
     * @param int $MinLength : defaults to 0
     * @param int $MaxLength : defaults to 100
     * @param int $Size : defaults to 100
     * @param bool $IsRequired : sets the field required attribute default true
     * @return void
     */
    public function add_password(Field $Field,
                                string $Pattern = '*',
                                string $ToolTip = null,
                                string $Description = null,
                                string $PlaceHolder = null,
                                int $MinLength = 0,
                                int $MaxLength = 100,
                                int $Size = 100,
                                bool $IsRequired = true): void
        {
        // Register the Field
        register_setting($Field->Page, $Field->ID);

        // Check to see if the field should be marked required
        if ($IsRequired)
            {
            $IsRequired = "required";
            }
        else
            {
            $IsRequired = null;
            }

        // Build the arguments used in display
        $Args = [
        'id'          => $Field->ID,
        'type'        => 'password',
        'tooltip'     => $ToolTip,
        'description' => $Description,
        'placeholder' => $PlaceHolder,
        'min_length'  => $MinLength,
        'max_length'  => $MaxLength,
        'size'        => $Size,
        'pattern'     => $Pattern,
        'required'    => $IsRequired,
        ];

        // Add the field
        add_settings_field($Field->ID,
                $Field->Title,
                function () use ($Args)
                    {
                    $this->show_password($Args);
                    },
                $Field->Page,
                $Field->Section,
                $Args
            );
        }

    /**
     * Callback for Fields::add_password()
     * @param mixed $Args
     * @return void
     */
    private function show_password($Args): void
        {

        $Encryption = new Encryption();

        $Field = $Args['id'];
        $Value = get_option($Args['id']);
        $Encrypted = $Encryption->decrypt(get_option($Args['id']));

        // Check to see if the field has a value.
        if (!empty($Value))
            {
            // Is this value already encrypted?
            if (empty($Encrypted))
                {
                // Value should alway be encrypted
                $Value = $Encryption->encrypt($Value);
                update_option($Args['id'], $Value);
                }
            }
        echo
            (
            "<input
            id = '{$Args['id']}'
            name = '{$Args['id']}'
            type = '{$Args['type']}'
            pattern = '{$Args['pattern']}'
            title = '{$Args['tooltip']}'
            placeholder = '{$Args['placeholder']}'
            minlength = '{$Args['minimum_length']}'
            maxlength = '{$Args['maximum_length']}'
            size = '{$Args['size']}'
            value = '" . Encryption::decrypt(get_option($Args['id'])) . "'
            {$Args['is_required']}
            ><br>
            <label for='{$Args['id']}'>{$Args['description']}</label><br>"
            );
        }

    /**
     * Adds a text editor to the page.
     * @param Field  $Field
     * @param string|null $Description
     * @param int $MinLength : defaults to 0
     * @param int $MaxLength : defaults to 100
     * @param int $Height : defaults to 100
     * @param int $Rows : defaults to 20
     * @param bool $IsMediaEnabled : enabled media buttons.
     * @param bool $IsWpautopEnabled : enables wpautop default false.
     * @return void
     */
    public function add_editor(Field $Field,
                                string $Description = null,
                                int $MinLength = 0,
                                int $MaxLength = 100,
                                int $Height = 100,
                                int $Rows = 20,
                                bool $IsMediaEnabled = false,
                                bool $IsWpautopEnabled = false,
                                ): void
        {
        // Register the Field
        register_setting($Field->Page, $Field->ID);

        // Build the arguments used in display
        $Args = [
        'id'            => $Field->ID,
        'min_length'    => $MinLength,
        'max_length'    => $MaxLength,
        'height'        => $Height,
        'textarea_rows' => $Rows,
        'media_buttons' => $IsMediaEnabled,
        'wpautop'       => $IsWpautopEnabled,
        'description'   => $Description
        ];

        // Add the field
        add_settings_field($Field->ID,
                $Field->Title,
                function () use ($Args)
                    {
                    $this->show_editor($Args);
                    },
                $Field->Page,
                $Field->Section,
                $Args
            );
        }

    /**
     * Callback for Fields::add_editor
     * @param mixed $Args
     * @return void
     */
    private function show_editor($Args): void
        {
        echo
            (
            wp_editor(
                get_option($Args['id']),
                $Args['id'],
                array(
                    'height'        => $Args['height'],
                    'textarea_name' => $Args['id'],
                    'media_buttons' => $Args['media_buttons'],
                    'editor_height' => $Args['height'],
                    'wpautop'       => $Args['wpautop'],
                    'textarea_rows' => $Args['textarea_rows'],
                    )
                )
                . "<br><label for='{$Args['id']}'>{$Args['description']}</label><br>"
            );
        }

    /**
     * Adds a dropdown of pages
     * @param Field $Field
     * @return void
     */
    public function add_wp_page_dropdown(Field $Field, string $Description = null, int $ChildOf = 0): void
        {
        // Register the Field
        register_setting($Field->Page, $Field->ID);

        // Build the arguments used in display
        $Args = [
            'id'          => $Field->ID,
            'child_of'    => $ChildOf,
            'description' => $Description
        ];

        // Add the field
        add_settings_field($Field->ID,
                $Field->Title,
                function () use ($Args)
                    {
                    $this->show_wp_page_dropdown($Args);
                    },
                $Field->Page,
                $Field->Section,
                $Args
            );
        }

    /**
     * Callback for Fields::add_password()
     * @param mixed $Args
     * @return void
     */
    private function show_wp_page_dropdown($Args): void
        {
        echo (
        wp_dropdown_pages(
            array(
                'name'              => $Args['id'],
                'selected'          => get_option($Args['id']),
                'echo'              => false,
                'show_option_none'  => 'Select One',
                'option_none_value' => null,
                'value_field'       => get_option($Args['id']),
                'child_of'          => $Args['child_of']
            )
            )
        . "<br><label for='{$Args['id']}'>{$Args['description']}</label><br>"
            );
        }

    }