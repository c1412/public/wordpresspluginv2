<?php

namespace cds\options;

use cds\api\storage\LocalTables;
use cds\logon\options\CustomFields;
use cds\options\Cron;

//If called directly exit
defined('ABSPATH') or exit;

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

class Activate
    {

    public function data_actions()
        {

        $Storage = new LocalTables();

        $Storage->create_persons_table();
        $Storage->create_firms_table();
        $Storage->create_lists_table();
        $Storage->create_events_table();
        }

    public function custom_fields()
        {
        $CustomFields = new CustomFields();

        $CustomFields->add_custom_roles();
        }

    public function cron_events()
        {
        $Cron = new Cron();
        $Cron->schedule();
        }

    }