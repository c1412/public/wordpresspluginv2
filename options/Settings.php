<?php

namespace cds\options;


defined('ABSPATH') or exit;

use cds\helpers\Encryption;
use cds\options\models\Page;
use cds\options\models\Section;
use cds\options\models\Field;

$Settings = new Settings();
$Encryption = new Encryption();
add_filter('admin_menu', array($Settings, 'create'));
add_filter('admin_head', array($Settings, 'remove_duplicate_item'));
add_filter('pre_update_option_cdsKey', array($Encryption, 'encrypt'), 10, 2);

class Settings
    {

    public  function create()
        {
        $ParentPageSlug = 'cds';

        $CDSSettings = new Page(null, "CDS", $ParentPageSlug, "CDS", "", 4);
        $CDSSettings->add($CDSSettings, 'dashicons-controls-repeat');

        $PageSlug = 'api';

        $ApiSettings = new Page($ParentPageSlug, "CDS Base Settings", $PageSlug, "API Settings", "Hey! Welcome to the initial settings page for the CDS Plugin. These settings build a connection between your organization and the CDS AM.Net APIs.", 4);
        $ApiSettings->add($ApiSettings,);

        // Section Settings
        $OrgSection = new Section('org', $PageSlug, "Organizational Settings", "These general settings are used by subordinate plugins. Please ensure all data is correct of you may encounter unexpected behaviors.");
        $OrgSection->add($OrgSection);

        //Fields
        $OrlUrl = new Field('cdsSiteUrl', 'Production Website', $PageSlug, $OrgSection->ID);
        $OrlUrl->add($OrlUrl, 'url', 'https://.*', 'Requires a valid url starting with https://', 'Your production site domain EX: https://www.mydomain.com <br>this will determine if we are using the production or <br>test endpoints.', 'https://www.mydomain.com', 8, 25, 25, true);

        $OrgEmail = new Field('cdsErrorEmail', 'Receives Errors', $PageSlug, $OrgSection->ID);
        $OrgEmail->add($OrgEmail, 'email', '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$', 'Requires valid email address', 'When using the test endpoint API errors will be sent to this<br> email address.', 'support@cds-am.net', 1, 25, 25, true);

        $OrgFirm = new Field('cdsFirm', 'Firm ID', $PageSlug, $OrgSection->ID);
        $OrgFirm->add($OrgFirm, 'text', '[0-9]{5}', 'Please provide a FirmCode in numeric format.', 'The AM.Net Firm ID assigned to your organization', '00021', 0, 10000000, 10, true);

        $OrgOpenTime = new Field('cdsOpenTime', 'Open Time', $PageSlug, $OrgSection->ID);
        $OrgOpenTime->add($OrgOpenTime, 'time', '*', 'The time your office opens.', 'The time your office opens.','09:00',0,8,10,true);

        $OrgCloseTime = new Field('cdsCloseTime', 'Close Time', $PageSlug, $OrgSection->ID);
        $OrgCloseTime->add($OrgCloseTime, 'time', '*', 'The time your office closes.', 'The time your office closes.','17:00',0,8,10,true);

        // Section Settings
        $ApiSection = new Section('api', $PageSlug, "API Settings", "CDS Assigns each organization unique API settings to use. If you don't have the information required to complete this page email <a href='mailto:support@cds=am.net'>support@cds-am.net</a>");
        $ApiSection->add($ApiSection);

        //Field Settings
        $ApiUser = new Field('cdsUser', 'User', $PageSlug, $ApiSection->ID);
        $ApiUser->add($ApiUser, 'text', "*", 'User is required to access the API Endpoint.', 'API user assigned from CDS.', 'web', 2, 5, 3, true);

        $ApiKey = new Field('cdsKey', 'Key', $PageSlug, $ApiSection->ID);
        $ApiKey->add_password($ApiKey, '*', 'Key is required to access the API Endpoint', 'API key assigned from CDS.', '111BBBB2-3344-5566-7788-999900001AB', 15, 80, 30, true);

        $ApiUrl = new Field('cdsUrl', 'Production Endpoint', $PageSlug, $ApiSection->ID);
        $ApiUrl->add($ApiUrl, "url", 'https://.*', 'Requires a valid url starting with https://', 'Production URL to access the API assigned from CDS.', 'https://your_org.cdscloudhost.net/api/', 8, 100, 50, true);

        $ApiTestUrl = new Field('cdsTestUrl', 'Test Endpoint', $PageSlug, $ApiSection->ID);
        $ApiTestUrl->add($ApiTestUrl, 'url', 'https://.*', 'Requires a valid url starting with https://', 'Test URL to access the API assigned from CDS.', 'https://your_org_test.cdscloudhost.net/api/', 8, 100, 50, true);

        $PageSlug = 'api';

        // Add Settings to the parent Page
        $SyncSection = new Section('sync',$PageSlug, 'Process Runs', 'Shows the last time data was updated. Modifying these settings allows you to resync data, warning this comes at a cost to performance.');
        $SyncSection->add($SyncSection);

        $SyncPersons = new Field('cdsSyncedPersons', 'Last Person Sync', $PageSlug, $SyncSection->ID);
        $SyncPersons->add($SyncPersons, 'datetime-local', '*', 'Must be a valid date.', 'The last time the person data changed on the website.', '', 15, 80, 40, true);
        $SyncFirms = new Field('cdsSyncedFirms', 'Last Firm Sync', $PageSlug, $SyncSection->ID);
        $SyncFirms->add($SyncFirms, 'datetime-local', '*', 'Must be a valid date.', 'The last time the firm data changed on the website.', '', 15, 80, 40, true);
        $SyncLists = new Field('cdsSyncedLists', 'Last List Sync', $PageSlug, $SyncSection->ID);
        $SyncLists->add($SyncLists, 'datetime-local', '*', 'Must be a valid date.', 'The last time the sycod data changed on the website.', '', 15, 80, 40, true);
        $SyncEvents = new Field('cdsSyncedEvents', 'Last Event Sync', $PageSlug, $SyncSection->ID);
        $SyncEvents->add($SyncEvents, 'datetime-local', '*', 'Must be a valid date.', 'The last time the events data changed on the website.', '', 15, 80, 40, true);
        $SyncTribe = new Field('cdsSyncedTribe', 'Last Tribe Event Sync', $PageSlug, $SyncSection->ID);
        $SyncTribe->add($SyncTribe, 'datetime-local', '*', 'Must be a valid date.', 'The last time the tribe events data changed on the website.', '', 15, 80, 40, true);

        $PageSlug = 'cds_login';

        // Page Settings
        $LoginSettings = new Page('cds', "CDS Login Settings", $PageSlug, "Login Settings","Use these options to configure the CDS custom login." , 5);
        $LoginSettings->add($LoginSettings);

        $LoginSection = new Section('map',$PageSlug, 'Page Mapping', 'This section allows you to override the default login behaviors.');
        $LoginSection->add($LoginSection);

        $LoginRedirect = new Field('cdsLoginRedirect', 'Login Redirect', $PageSlug, $LoginSection->ID);
        $LoginRedirect->add_wp_page_dropdown($LoginRedirect, 'This is where users will go to login, or where they are <br>redirected when they hit a page that requires login.',0);

        $LoginMembershipRequired = new Field('cdsLoginMembershipRequired', 'Paywall Redirect', $PageSlug, $LoginSection->ID);
        $LoginMembershipRequired->add_wp_page_dropdown($LoginMembershipRequired, 'When a non-member attempts to access a member <br>restricted page, they will be redirected here.',0);

        $LoginRegistration = new Field('cdsLoginRegistration', 'New User Registration', $PageSlug, $LoginSection->ID);
        $LoginRegistration->add_wp_page_dropdown($LoginRegistration, 'Website visitors who would like to create an account will be <br>directed here.',0);

        $LoginPasswordReset = new Field('cdsPasswordReset', 'Password Reset', $PageSlug, $LoginSection->ID);
        $LoginPasswordReset->add_wp_page_dropdown($LoginPasswordReset, 'Users who need to change their password will be directed <br>here.',0);

        $LoginSectionText = new Section('text',$PageSlug, 'Message Control', 'This section is used to set the default messages user see for various actions');
        $LoginSectionText->add($LoginSectionText);

        if(empty(get_option('cdsPaywallLoginText')))
        {
            update_option('cdsPaywallLoginText','The page {$Link} your attempting to visit requires a valid login.');
        }
        $TextPaywallLogin = new Field('cdsPaywallLoginText', 'Login Required Text', $PageSlug, $LoginSectionText->ID);
        $TextPaywallLogin->add_editor($TextPaywallLogin,'The message to visitors who attempted to access a restricted page. Dynamic Value {$Link}.', 0, 256, 50, 2, false, false);

        if(empty(get_option('cdsPaywallMembershipRequiredText')))
        {
            update_option('cdsPaywallMembershipRequiredText','The page {$Link} your attempting to visit requires a valid membership.');
        }
        $TextPaywallMembershipRequired = new Field('cdsPaywallMembershipRequiredText', 'Membership Required Text', $PageSlug, $LoginSectionText->ID);
        $TextPaywallMembershipRequired->add_editor($TextPaywallMembershipRequired,'The message to show non-members who attempted to access a membership page. Dynamic Value {$Link}.', 0, 256, 50, 2, false, false);

        if(empty(get_option('$TextPasswordRecoveryText')))
        {
            update_option('$TextPasswordRecoveryText','{$User}, <p>This is an automated password recovery email.</p> <p>Please visit {$Link} within 15 minutes to reset your password. If you did not request this email please let us know.</p> </p>Thank you,</p> <p>SCACPA Staff</p>');
        }
        $TextPasswordRecoveryText = new Field('cdsPasswordRecoveryText', 'Membership Required Text', $PageSlug, $LoginSectionText->ID);
        $TextPasswordRecoveryText->add_editor($TextPasswordRecoveryText,'The password recovery email sent to the user. Dynamic values {$User} {$Link}.', 0, 1024, 300, 12, false, false);

        $PageSlug = 'affinipay';

        // Page Settings
        $AffinipaySettings = new Page('cds', "Cart Settings", $PageSlug, "Cart Settings","Use these options to configure payment processing." , 10);
        $AffinipaySettings->add($AffinipaySettings);

        $AffinipaySection = new Section('AffinipaySection',$PageSlug, 'Affinipay Payment Keys', 'Public and secret keys used for Affinipay connections.');
        $AffinipaySection ->add($AffinipaySection);

        $PublicKey = new Field('AffinipayPublicKey', 'Public Key', $PageSlug, $AffinipaySection->ID);
        $PublicKey->add($PublicKey,'text','*','Public Key is required, in order to enable payment processing','Public key provided from https://secure.affinipay.com/settings/advanced.','x_JJhSlbCvFGuvO8HHtFatHQ',10,30,70,true);

        $LiveSecretKey = new Field('cdsAffinipayLiveSecretKey', 'Live Secret Key', $PageSlug, $AffinipaySection->ID);
        $LiveSecretKey->add_password($LiveSecretKey,'*','The live secret key provided by Affinipay is required','The live secret key provided by Affinipay.','KTKadOwsyFGxynIq24GN3hs6Uf58Sk1yI1KHbLqjl41FGxyn6FN3JjTW',10,100,70,true);

        $TestSecretKey = new Field('cdsAffinipayTestSecretKey', 'Test Secret Key', $PageSlug, $AffinipaySection->ID);
        $TestSecretKey->add_password($TestSecretKey,'*','The test secret key provided by Affinipay is required','The test secret key provided by Affinipay.','KTKadOwsyFGxynIq24GN3hs6Uf58Sk1yI1KHbLqjl41FGxyn6FN3JjTW',10,100,70,true);

        $CartSection = new Section('CartSection',$PageSlug, 'Cart Settings', 'Settings for the shopping cart.');
        $CartSection ->add($CartSection);

        $CartPage = new Field('cdsCartPage', 'Location of the cart page.', $PageSlug, $CartSection->ID);
        $CartPage->add_wp_page_dropdown($CartPage, 'Page used to display items in the cart.',0);

        $CartCookie = new Field('cdsCartCookie', 'Text For Add Button', $PageSlug, $CartSection->ID);
        $CartCookie->add($CartCookie,'text','*','You must supply label text','The text shown for the add to cart button.','Add To Cart',10,30,30,true);

        $CartAdd = new Field('cdsCartAdd', 'Text For Add Button', $PageSlug, $CartSection->ID);
        $CartAdd->add($CartAdd,'text','*','You must supply label text','The text shown for the add to cart button.','Add To Cart',10,30,30,true);

        $CartRemove = new Field('cdsCartRemove', 'Text For Remove Button', $PageSlug, $CartSection->ID);
        $CartRemove->add($CartRemove,'text','*','You must supply label text.','The text shown for the remove from cart button.','Remove From Cart',10,30,30,true);

        $CartDelete = new Field('cdsCartDelete', 'Text For Empty Cart Button', $PageSlug, $CartSection->ID);
        $CartDelete->add($CartDelete,'text','*','You must supply label text.','The text shown on the delete all items button.','Delete All',10,30,30,true);


        }

        public  function remove_duplicate_item() {

            remove_submenu_page('cds','cds');
        }

    }