<?php

use cds\api\Lists;
use cds\storage\ListStorage;
use cds\wordpress\Profile;

$person = $args['person'];
$action = $args['action'];

?>

<style>
    .row-margin-05 {
        margin-top: 0.5em;
    }

    .row-margin-10 {
        margin-top: 1.0em;
    }

    .row-margin-20 {
        margin-top: 2.0em;
    }

    .row-margin-30 {
        margin-top: 3.0em;
        margin-bottom: 3.0em;
    }

    .error {
        color: red;
    }

    .section {
        background-color: white;
        width: 100%;
        border: 1px solid rgba(172, 172, 173, 0.75) !important;
        border-radius: 4px;
        padding-bottom: 10px;
        margin-top:1em;
    }

    .border {
        padding:.5em;
        width: 100%;
        border: 1px solid rgba(172, 172, 173, 0.75) !important;
        border-radius: 4px;
        padding-left: 10px;
    }

    input::placeholder {
        opacity: .7;
        color: #abb8c3;
    }

    label {
        font-weight: bold;
        display: block;
        margin-top: 15px;
        margin-bottom: 0px;
    }
</style>
    <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;">
        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;">
            <div id="alert-box" style="display:hidden"><?php echo $action === "complete" ? Profile::build_success_alert() : null ?></div>
            <div id="cds-profile-page" class="fusion-login-box fusion-login-box-1 fusion-login-box-login fusion-login-align-textflow fusion-login-field-layout-stacked" style="border-color:rgba(172,172,173,0.75);border-width:1px;">
                <form id="profile" class="fusion-login-form" style="background-color:#f2f3f5" name="reset-password-form" method="post" autocomplete="off">
                <div class="row">
                        <div class="col-md-12 col-lg-12 section">
                            <div style="font-size:2em;font-weight:600;">Website</div>
                            <div>Update the fields attached to your website account.</div>
                            <hr>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <label for="Email">Primary Email</label>
                        <input id="Email" type="email" name="Email" class="border" placeholder="you@yourdomain.com" <?php echo isset($person->Email) ? 'value="' . $person->Email .'"' : null ?> onkeyup="this.value = this.value.toLowerCase();" required>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label for="EmailAddress2">Additional Email</label>
                        <input id="EmailAddress2" type="email" name="EmailAddress2" class="border" placeholder="you@yourbackupdomain.com" <?php echo isset($person->EmailAddress2) ? 'value="' . $person->EmailAddress2 .'"' : null ?> onkeyup="this.value = this.value.toLowerCase();" autocomplete="nope">
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label for="Password">Website Password</label>
                        <input id="Password" type="password" name="Password" class="border" autocomplete="new-password" <?php echo $action === 'reset' ? 'required' : null ?>>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label for="ConfirmPassword">Confirm Password</label>
                        <input id="ConfirmPassword" type="password" name="ConfirmPassword" class="border" autocomplete="new-password" <?php echo $action === 'reset' ? 'required' : null ?>>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-12 col-lg-12 section">
                            <div style="font-size:2em;font-weight:600;">Contact</div>
                            <div>Let us know how we can contact you.</div>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <label for="FirstName">First</label>
                            <input id="FirstName" type="text" name="FirstName" class="border" placeholder="William" <?php echo isset($person->FirstName) ? 'value="' . $person->FirstName .'"': null ?>required>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <label for="MiddleInitial">Middle</label>
                            <input id="MiddleInitial" type="text" name="MiddleInitial" class="border" placeholder="Douglas" <?php echo isset($person->MiddleInitial) ? 'value="' . $person->MiddleInitial .'"': null ?>>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="LastName">Last</label>
                            <input id="LastName" type="text" name="LastName" class="border" placeholder="Smith" <?php echo isset($person->LastName) ? 'value="' . $person->LastName .'"': null ?> required>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <label for="Suffix">Suffix</label>
                            <input id="Suffix" type="text" name="Suffix" class="border" placeholder="Jr." <?php echo isset($person->Suffix) ? 'value="' . $person->Suffix .'"': null ?>>
                        </div>
                        <div class="col-md-2 col-lg-2">
                                <label for="NickName">Nickname</label>
                                <input id="NickName" type="text" name="NickName" class="border" placeholder="Bill" <?php echo isset($person->NickName) ? 'value="' . $person->NickName .'"': null ?>>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <label for="HomeAddressLine1">Home Address</label>
                            <input id="HomeAddressLine1" type="text" name="HomeAddressLine1" class="border" placeholder="1300 12th Street Suite D" <?php echo isset($person->HomeAddressLine1) ? 'value="' . $person->HomeAddressLine1 .'"': null ?> required>
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <label for="HomeAddressCity">City</label>
                                    <input id="HomeAddressCity" type="text" name="HomeAddressCity" class="border" placeholder="Cayce" <?php echo isset($person->HomeAddressCity) ? 'value="' . $person->HomeAddressCity .'"': null ?> required>
                                    </div>
                                <div class="col-md-6 col-lg-4">
                                    <label for="HomeAddressStateCode">State</label>
                                    <?php echo Lists::create_input_select_field("STAT", null, "HomeAddressStateCode", "border", "Choose One", isset($person->HomeAddressStateCode) ? $person->HomeAddressStateCode : null,true);?>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <label for="HomeAddressStreetZip">Zip Code</label>
                                    <input id="HomeAddressStreetZip" type="text" name="HomeAddressStreetZip" class="border" placeholder="29033" <?php echo isset($person->HomeAddressStreetZip) ? 'value="' . $person->HomeAddressStreetZip .'"': null ?> required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="MobilePhone">Mobile Phone</label>
                            <input id="MobilePhone" type="text" name="MobilePhone" class="border phone-number-field" placeholder="555-555-5555" <?php echo isset($person->MobilePhone) ? 'value="' . $person->MobilePhone .'"': null ?>>
                            <label for="DirectPhone">Office Phone</label>
                            <input id="DirectPhone" type="tel" name="DirectPhone" class="border phone-number-field" placeholder="555-555-5555" <?php echo isset($person->DirectPhone) ? 'value="' . $person->DirectPhone .'"': null ?>>
                            <label for="HomePhone">Home Phone</label>
                            <input id="HomePhone" type="tel" name="HomePhone" class="border phone-number-field" placeholder="555-555-5555" <?php echo isset($person->HomePhone) ? 'value="' . $person->HomePhone .'"': null ?>>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <label for="Credentials">Credentials</label>
                            <input id="Credentials" type="text" name="Credentials" class="border" placeholder="CFP" <?php echo isset($person->Credentials) ? 'value="' . $person->Credentials .'"': null ?>>
                            <label for="BirthDate">Birth Date</label>
                            <input id="BirthDate" type="date" name="BirthDate" class="border" placeholder="" <?php echo isset($person->BirthDate) ? 'value="' . date('Y-m-d', strtotime($person->BirthDate)) .'"': null ?>>
                            <label for="GenderCode">Gender</label>
                            <?php echo Lists::create_input_select_field("NAGN", null, "GenderCode", "border", "Choose One", isset($person->GenderCode) ? $person->GenderCode : null);?>
                        </div>
                    </div>
                    <div class="row row-margin-30">
                        <div class="col-md-3 col-lg-3">
                            <input id="ID" type="hidden" name="ID" <?php echo !empty($person->ID) ? 'value="' . $person->ID .'"': null ?>>
                            <button class="fusion-login-button fusion-button button-default fusion-button-default-size fusion-button-span-yes" type="submit" id="submit" name="submit"><?php echo empty($person->ID) ? 'Create Account' : 'Update Profile' ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
