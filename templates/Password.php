<div class="fusion-layout-column fusion_builder_column fusion-builder-column-0 fusion_builder_column_1_2 1_2 fusion-flex-column">
    <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;">
        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column"style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;">
            <div id="alert-box" style="display:hidden"></div>
            <div id="cds_core_login"
                class="fusion-login-box fusion-login-box-1 fusion-login-box-login fusion-login-align-textflow fusion-login-field-layout-stacked"
                id="cds_core_password_reset" style="border-color:rgba(172,172,173,0.75);border-width:1px;">
                <form class="fusion-login-form" style="background-color:#f2f3f5" name="reset-password-form"
                    id="reset-password-form" method="post">
                    <div class="fusion-login-fields">
                        <div id='verify-email' class="fusion-login-input-wrapper">
                            <label for="email_address" class="fusion-login-label" for="email_address">Email Address</label>
                            <label for="email_address" style="color:red;" class="fusion-login-label error"></label>
                            <input type="email" name="email_address" placeholder="yourname@domain.com" value="" size="20" class="fusion-login-username input-text" id="email_address" required>
                        </div>
                    </div>
                    <div class="fusion-login-additional-content">
                        <div class="fusion-login-submit-wrapper">
                            <button
                                class="fusion-login-button fusion-button button-default fusion-button-default-size fusion-button-span-yes"
                                type="submit" id="submit" name="submit">Verify Email Address</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>