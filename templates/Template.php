<?php

namespace cds\templates;

//If called directly exit
defined('ABSPATH') or exit;

$Template = new Template();

add_filter('template_include', array($Template, 'include_template'), 99);
class Template
    {

    public function __construct(string $TemplateName = null, array $args = null)
        {
        if (!empty($TemplateName))
            {
            $this->include_template($TemplateName, $args);
            }
        }

    /**
     * Summary of include_template
     * @param mixed $TemplateName
     * @return mixed
     */
    function include_template($TemplateName, array $args = null)
        {

        if (is_array($args) && isset($args)):
            extract($args);
        endif;

        if (file_exists(CDS_LOGIN_TEMPLATE_OVERRIDE_DIR . $TemplateName))
            {
            $TemplatePath = CDS_LOGIN_TEMPLATE_OVERRIDE_DIR . $TemplateName;
            }
        else
            {
            $TemplatePath = CDS_LOGIN_TEMPLATE_DIR . $TemplateName;
            }

        include $TemplatePath;

        }
    }