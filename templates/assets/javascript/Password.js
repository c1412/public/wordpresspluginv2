jQuery(function ($) {

    $("#reset-password-form").validate({
        rules: {
            email_address: {
                required: true,
                email: true,
            }
        },
        messages: {
            email_address: {
                required: "Required",
                email: "email"
            }
        }
    });

    $("#submit").on('click', function (event) {
        event.preventDefault();
        $('#submit').prop('disabled', true);
        if ($("#reset-password-form").valid()) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: ajax_vars.wp_ajax_url,
                beforeSend: function () {
                    $('#loading').show();
                    $('#submit').html('Processing<img id="loading" style="height:20px;width:20px;padding-left:5px;" src="/wp-content/plugins/cds/templates/assets/images/loading-gif.gif">');
                },
                data: {
                    action: ajax_vars.wp_ajax_return_function,
                    security: ajax_vars.wp_ajax_nonce,
                    form_data: $("#reset-password-form").serialize()
                },
                complete: function () {
                },
                success: function (response) {
                    if (response.success) {
                        $('#verify-email').hide();
                        $('#alert-box').show();
                        $('#alert-box').removeClass('alert-danger').addClass('alert-secondary');
                        $('#alert-box').html(response.data);
                        $('#submit').prop('disabled', false);
                        $('#submit').html('Resend Email');
                    } else {
                        $('#submit').html('Verify Email Address');
                        $('#alert-box').show();
                        $('#submit').prop('disabled', false);
                        $('#alert-box').html(response.data);
                    }
                },
            });
        }
    });
});

