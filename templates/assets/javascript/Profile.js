jQuery(function ($) {


    $('.phone-number-field').keyup(function () {
        $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'))
    });

    $.validator.addMethod("strong_password", function (value, element) {
        let password = value;
        if (!(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%&])(.{8,20}$)/.test(password))) {
            return false;
        }
        return true;
    }, function (value, element) {
        let password = $(element).val();
        if (!(/^(.{6,20}$)/.test(password))) {
            return 'Must be between 6 to 20 characters long.';
        }
        else if (!(/^(?=.*[A-Za-z])/.test(password))) {
            return 'Must contain at least one or more letters.';
        }
        else if (!(/^(?=.*[0-9])/.test(password))) {
            return 'Must contain at least one or more numbers.';
        }
        else if (!(/^(?=.*[><?@+'`~^%&\*\[\]\{\}.!#|\\\"$';,:;=/\(\),\-*])/.test(password))) {
            return "Must contain one or more special characters.";
        }
        return false;
    });

    // Submit button was clicked start check the account credentials.
    $("#profile").on('submit', function (event) {

        // Stop the form until ajax is successful;
        event.preventDefault();

        // Are the values valid?
        if ($("#profile").valid()) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: ajax_vars.wp_ajax_url,
                beforeSend: function () {
                    $('#submit').html('Processing<img id="loading" style="height:20px;width:20px;padding-left:5px;" src="/wp-content/plugins/cds/templates/assets/images/loading-gif.gif">');
                    $('#submit').prop('disabled', true);
                },
                data: {
                    action: ajax_vars.wp_ajax_return_function,
                    security: ajax_vars.wp_ajax_nonce,
                    form_data: $("#profile").serialize(),
                },
                complete: function () {
                },
                success: function (response) {
                    if (response.success) {
                        // We use login programmatically so a reload is required.

                        uri = location.href;

                        if (uri.indexOf("?") > 0) {
                            uri = uri.substring(0, uri.indexOf("?"));
                        }

                        location.href = uri + "?action=complete"
                    } else {
                        $('#submit').prop('disabled', false);
                        $('#submit').html('Update Profile');
                        $('#alert-box').html(response.data);
                        $('html,body').animate({
                            scrollTop: $("#alert-box").offset().top
                         });
                    }
                },
            });
        }
    });

    $("#profile").validate({
        rules: {
            Password: {
                minlength: 6,
                //strong_password: true
            },
            ConfirmPassword: {
                minlength: 6,
                equalTo: "#Password"
            },
            MobilePhone: {
                required: true,
                phoneUS: true,
            },
            EmailAddress2: {
                notEqualTo: "#Email"
            }
        },
        messages: {
            Email2: {
                notEqualTo: "Primary Email and Additional Email can't match."
            }
        },
        submitHandler: function (form) {
            {
                $("#Email").val($("#Email").val().toLowerCase());
                $("#EmailAddress2").val($("#EmailAddress2").val().toLowerCase());
            }
        }
    });
});

