jQuery(function ($) {

    // Override Validation Messages
    $.validator.messages.required = 'Required';
    $.validator.messages.email = 'Invalid';

    // Quick Validation
    $("#loginform").validate({
        rules: {
            user_login: {
                required: true,
                email: true,
            },
            user_pass: {
                required: true,
            }
        }
    });

    // Submit button was clicked start check the account credentials.
    $("#wp-submit").on('click', function (event) {

        // Stop the form until ajax is successful;
        event.preventDefault();

        // Are the values valid?
        if ($("#loginform").valid()) {
            $.ajax({
                type: "post",
                dataType: "json",
                url: ajax_vars.wp_ajax_url,
                beforeSend: function () {
                    $('#wp-submit').html('Processing<img id="loading" style="height:20px;width:20px;padding-left:5px;" src="/wp-content/plugins/cds/templates/assets/images/loading-gif.gif">');
                    $('#wp-submit').prop('disabled', true);
                },
                data: {
                    action: ajax_vars.wp_ajax_return_function,
                    security: ajax_vars.wp_ajax_nonce,
                    form_data: $("#loginform").serialize()
                },
                complete: function () {
                },
                success: function (response) {
                    if (response.success) {
                        $('#loginform').trigger("submit");
                    } else {
                        $('#wp-submit').prop('disabled', false);
                        $('#wp-submit').html('Log In');
                        $('#alert-box').show();
                        $('#alert-box').html(response.data);
                    }
                },
            });
        }
    });
});


