<?php

namespace cds\templates\assets\parts;

defined('ABSPATH') or exit;

class Alerts
    {
        public ?string $Content;
    /**
     * Summary of __construct
     * @param string|null $Type General, Success, Warning, Error
     * @param string|null $ID ID for the on page container
     * @param string|null $Subject Subject of the alert
     * @param string|null $Body Body text for the alert
     * @param bool $Dismissible Can the box be dismissed default true
     */
    public function __construct(string $Type = null, string $ID = null, string $Subject = null, string $Body = null, bool $Dismissible = true)
        {
        switch ($Type)
            {
            case "General":
                $this->Content = self::get_general_alert($ID, $Subject, $Body, $Dismissible);
                break;
            case "Success":
                $this->Content = self::get_success_alert($ID, $Subject, $Body, $Dismissible);
                break;
            case "Warning":
                $this->Content = self::get_warning_alert($ID, $Subject, $Body, $Dismissible);
                break;
            case "Error":
                $this->Content = self::get_error_alert($ID, $Subject, $Body, $Dismissible);
                break;
            default:
                $this->Content = null;
            }
        }

    /**
     * Create and return a general alert as HTML String
     * @param string $ID
     * @param string $Subject
     * @param string|null $Body
     * @param bool $Dismissible
     * @return bool|string
     */
    public function get_general_alert(string $ID, string $Subject = 'General Information', string $Body = null, bool $Dismissible = true)
        {
        ob_start();
?>
<style type="text/css">
    #alert-general-body a {
        color: var(--awb-color8) !important;
        font-weight: bold;
        text-decoration: underline;
    }
</style>
<div <?php echo (!empty($ID) ? 'id="' . $ID . '"' : null) ?> class="fusion-alert alert general alert-info
    fusion-alert-center alert-dismissable" role="alert"
    style="background-color:var(--awb-color1);color:var(--awb-color8);border-color:var(--awb-color8);border-width:1px;">
    <div class="fusion-alert-content-wrapper">
        <span class="alert-icon">
            <div style="display:table-cell; vertical-align:middle; font-weight:bold; font-size: 1.2em;">
                <i class="awb-icon-info-circle" style="padding: 0px 5px 3px 0px;" aria-hidden="true"></i>
                <?php echo ($Subject) ?>
            </div>
        </span>
        <span class="fusion-alert-content">
            <hr style="border-color:var(--awb-color8);width:75%">
            <div id='alert-general_body'>
                <?php echo ($Body) ?>
            </div>
        </span>
    </div>
    <button type="button" class="close toggle-alert" data-dismiss="alert" aria-label="Close" <?php echo $Dismissible ?
        "" : "style='display:none;'" ?>>×</button>
</div>
<?php

        return ob_get_clean();
        }

    /**
     * Create and return an error alert as HTML String
     * @param string $ID
     * @param string $Subject
     * @param string|null $Body
     * @param bool $Dismissible
     * @return bool|string
     */
    public  function get_error_alert(string $ID, string $Subject = 'An Error Occurred', string $Body = null, bool $Dismissible = true)
        {
        ob_start();
        ?>
<style type="text/css">
    #alert-error-body a {
        color: #db4b68 !important;
        font-weight: bold;
        text-decoration: underline;
    }
</style>
<div <?php echo (!empty($ID) ? 'id="' . $ID . '"' : null) ?> class="fusion-alert alert error alert-danger
    fusion-alert-center alert-dismissable" role="alert"
    style="background-color:rgba(219,75,104,0.1);color:#db4b68;border-color:#db4b68;border-width:1px;">
    <div class="fusion-alert-content-wrapper">
        <span class="alert-icon">
            <div style="display:table-cell; vertical-align:middle; font-weight:bold; font-size: 1.2em">
                <i class="awb-icon-exclamation-triangle" style="padding: 0px 5px 3px 0px;" aria-hidden="true"></i>
                <?php echo ($Subject) ?>
            </div>
        </span>
        <span class="fusion-alert-content error">
            <hr style="border-color:#db4b68;width:75%">
            <div id='alert-error-body'>
                <?php echo ($Body) ?>
            </div>
        </span>
    </div>
    <button type="button" class="close toggle-alert" data-dismiss="alert" aria-label="Close" <?php echo $Dismissible ?
        "" : "style='display:none;'" ?>>×</button>
</div>
<?php

        return ob_get_clean();
        }

    /**
     * Create and return a success alert as HTML String
     * @param string $ID
     * @param string $Subject
     * @param string|null $Body
     * @param bool $Dismissible
     * @return bool|string
     */
    public  function get_success_alert(string $ID, string $Subject = 'Action Succeeded', string $Body = null, bool $Dismissible = true)
        {
        ob_start();
        ?>
<style type="text/css">
    #alert-success-body a {
        color: #12b878 !important;
        font-weight: bold;
        text-decoration: underline;
    }
</style>
<div <?php echo (!empty($ID) ? 'id="' . $ID . '"' : null) ?> class="fusion-alert alert success alert-success
    fusion-alert-center alert-dismissable" role="alert"
    style="background-color:rgba(18,184,120,0.1);color:#12b878;border-color:#12b878;border-width:1px;">
    <div class="fusion-alert-content-wrapper">
        <span class="alert-icon">
            <div style="display:table-cell; vertical-align:middle; font-weight:bold; font-size: 1.2em">
                <i class="awb-icon-check-circle" style="padding: 0px 5px 3px 0px;" aria-hidden="true"></i>
                <?php echo ($Subject) ?>
            </div>
        </span>
        <span class="fusion-alert-content">
            <hr style="border-color:#12b878;width:75%">
            <div id='alert-success-body'>
                <?php echo ($Body) ?>
            </div>
        </span>
    </div>
    <button type="button" class="close toggle-alert" data-dismiss="alert" aria-label="Close" <?php echo $Dismissible ?
        "" : "style='display:none;'" ?>>×</button>
</div>
<?php

        return ob_get_clean();
        }

    /**
     * Create and return a warning alert as HTML String
     * @param string $ID
     * @param string $Subject
     * @param string|null $Body
     * @param bool $Dismissible
     * @return bool|string
     */
    public  function get_warning_alert(string $ID, string $Subject = 'Warning', string $Body = null, bool $Dismissible = true)
        {
        ob_start();
        ?>
<style type="text/css">
    #alert-warning-body a {
        color: #f1ae2a !important;
        font-weight: bold;
        text-decoration: underline;
    }
</style>
<div <?php echo (!empty($ID) ? 'id="' . $ID . '"' : null) ?> class="fusion-alert alert notice alert-warning
    fusion-alert-center alert-dismissable" role="alert"
    style="background-color:rgba(241,174,42,0.1);color:#f1ae2a;border-color:#f1ae2a;border-width:1px;">
    <div class="fusion-alert-content-wrapper">
        <span class="alert-icon">
            <div style="display:table-cell; vertical-align:middle; font-weight:bold; font-size: 1.2em;">
                <i class="awb-icon-cog" style="padding: 0px 5px 3px 0px;" aria-hidden="true"></i>
                <?php echo ($Subject) ?>
            </div>
        </span>
        <span class="fusion-alert-content">
            <hr style="border-color:#f1ae2a;width:75%">
            <div id="alert-warning-body">
                <?php echo ($Body) ?>
            </div>
        </span>
    </div>
    <button type="button" class="close toggle-alert" data-dismiss="alert" aria-label="Close" <?php echo $Dismissible ?
        "" : "style='display:none;'" ?>>×</button>
</div>
<?php

        return ob_get_clean();
        }
    }