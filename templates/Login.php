<div class="fusion-layout-column fusion_builder_column fusion-builder-column-0 fusion_builder_column_1_2 1_2 fusion-flex-column">
    <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;">
        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column"
            style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;">
            <div id="alert-box" style="display:hidden"><?php echo(!empty($args['notice']) ? $args['notice'] : null) ?></div>
            <div id="cds_core_login"
                class="fusion-login-box fusion-login-box-1 fusion-login-box-login fusion-login-align-textflow fusion-login-field-layout-stacked"
                id="cds_core_login" style="border-color:rgba(172,172,173,0.75);border-width:1px;">
                <form class="fusion-login-form" style="background-color:#f2f3f5;" name="loginform" id="loginform"
                    method="post" action="<?php echo (site_url()) ?>/wp-login.php">
                    <div class="fusion-login-fields">
                        <div class="fusion-login-input-wrapper">
                            <label class="fusion-login-label" for="user_login">Email Address</label>
                            <label for="user_login" style="color:red;" class="fusion-login-label error"></label>
                            <input type="email" name="log" placeholder="yourname@domain.com" value="" size="20"
                                class="fusion-login-username input-text" id="user_login" required >
                        </div>
                        <div class="fusion-login-input-wrapper">
                            <label class="fusion-login-label" for="user_pass">Password</label>
                            <label for="user_pass" style="color:red;" class="fusion-login-label error"></label>
                            <input type="password" name="pwd" placeholder="" value="" size="20"
                                class="fusion-login-password input-text" id="user_pass" required>
                        </div>
                    </div>
                    <div class="fusion-login-additional-content">
                        <div class="fusion-login-submit-wrapper">
                            <button class="fusion-login-button fusion-button button-default fusion-button-default-size fusion-button-span-yes" type="submit" id="wp-submit" name="wp-submit">Log in</button>
                            <input type="hidden" name="user-cookie" value="1">
                            <input type="hidden" name="redirect_to" value="<?php echo ($args["redirect_to"]) ?>">
                            <span class="fusion-login-nonce" style="display:none;">
                                <input type="hidden" id="_wpnonce" name="_wpnonce" value="<?php echo (wp_create_nonce()) ?>"></span>
                        </div>
                        <div class="fusion-login-links">
                            <label class="fusion-login-remember-me"><input name="rememberme" type="checkbox" id="rememberme"
                                    value="forever" checked="checked">Remember Me</label>
                            <a class="fusion-login-lost-passowrd" target="_self" href="<?php echo (wp_lostpassword_url()) ?>">Forgot Password</a>
                            <a class="fusion-login-register" target="_self" href="<?php echo (wp_registration_url()) ?>">Create Account</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>