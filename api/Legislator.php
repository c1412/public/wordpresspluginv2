<?php

namespace cds\api;
use cds\helpers\ApiHelper;

defined('ABSPATH') or exit;

class Legislator
    {

    public ?string $ID;
    public ?string $NationalId;
    public ?string $FirstName;
    public ?string $LastName;
    public ?string $MiddleName;
    public ?string $Title;
    public ?string $Suffix;
    public ?string $GenderCode;
    public ?string $LegislatorTypeCode;
    public ?string $StatusCode;
    public ?string $LastYearInOffice;
    public ?string $PositionCode;
    public ?string $PartyCode;
    public ?string $DistrictCode;
    /** @var Address|null */
    public ? Address $GovernmentAddress;
    /** @var Address|null */
    public ? Address $DistrictAddress;
    public ?string $EmailAddress;
    /** @var Contact[]|null */
    public ?array $Contacts;
    public ?string $WebAddress;

    /**
	 * @param object|null $Data
	 */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ID = trim($Data->ID);
            $this->NationalId = trim($Data->NationalId);
            $this->FirstName = $Data->FirmName;
            $this->LastName = $Data->LastName;
            $this->MiddleName = $Data->MiddleName;
            $this->Title = $Data->Title;
            $this->Suffix = $Data->Suffix;
            $this->PartyCode = $Data->PartyCode;
            $this->GenderCode = $Data->GenderCode;
            $this->LegislatorTypeCode = $Data->LegislatorTypeCode;
            $this->StatusCode = $Data->StatusCode;
            $this->LastYearInOffice = $Data->LastYearInOffice;
            $this->PositionCode = $Data->PositionCode;
            $this->DistrictCode = $Data->DistrictCode;
            $this->GovernmentAddress = new Address($Data->GovernmentAddress);
            $this->DistrictAddress = new Address($Data->DistrictAddress);
            $this->EmailAddress = $Data->EmailAddress;
            $this->Contacts = ApiHelper::array_of_type('ContactDto', $Data->Contacts);
            $this->WebAddress = $Data->WebAddress;
            }
        }
    }