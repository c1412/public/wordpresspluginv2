<?php

namespace cds\api;

defined('ABSPATH') or exit;

class ListKey
	{

	public ?string $ListKey;
	public ?string $ListUsage;

	/**
	 * @param object|null $Data
	 * @suppress 0416
	 */
	public function __construct(?object $Data = null)
		{
		if (!is_wp_error($Data) && is_object($Data))
			{
			$this->ListKey = $Data->ListKey;
			$this->ListUsage = $Data->ListUsage;
			}
		}
	}