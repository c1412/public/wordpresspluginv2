<?php

namespace cds\api;

use DateTime;
use DateTimeZone;
use WP_Error;

defined('ABSPATH') or exit;

class Logging
    {
    public function __construct(WP_Error $Error)

        {
        $this->log($Error);
        }


    public function log(WP_Error $wp_error)
        {
        $Time = new DateTime("now", new DateTimeZone(wp_timezone_string()));
        $Time = date_format($Time, "d/m/Y g:i:s A");
        $Trace = debug_backtrace(0, 12);
        $File = "File: {$Trace[1]['file']} ({$Trace[1]['line']})";
        $Function = "Function: {$Trace[2]['class']}::{$Trace[2]['function']}";
        $ErrorCode = $wp_error->get_error_code();
        $ErrorMessage = $wp_error->get_error_message();
        $ErrorData = $wp_error->get_error_data($wp_error->get_error_code());
        $Args = PHP_EOL;
        if (is_array($ErrorData))
            {
            array_walk_recursive(
                            $ErrorData,
                                function ($value, $key) use (&$Args)
                                    {
                                    $Args .= '| ' . $key . ": " . $value . PHP_EOL;
                                    }
                    );
            $Data = json_encode($ErrorData);
            }
        if (is_object($ErrorData))
            {
            $Args = null;
            $Data = json_encode($ErrorData);
            }
        if (empty($ErrorData))
            {
            $Args = null;
            $Data = null;
            }

        $FormattedTrace = $this->get_trace($Trace);

        $Output =
        "
._____________________________________________________________________________________________
| {$File}
| {$Function}
| Error Code: {$ErrorCode}
| Error Message: {$ErrorMessage}
| Local Time: {$Time}
| Arguments: {$Args}
|
| START TRACE ________________________________________________________________________________

{$FormattedTrace}
  END TRACE __________________________________________________________________________________";
        if (!empty($Data) && is_string($Data))
            {
            $Output = $Output . '
  START JSON _________________________________________________________________________________' . PHP_EOL .

            json_encode(json_decode($Data), JSON_PRETTY_PRINT) . PHP_EOL . '

  END JSON____________________________________________________________________________________';
            }

        error_log($Output);

        $Output = preg_replace("/\n|$/", "<br />" . PHP_EOL, $Output);

        $To = get_option('cdsErrorEmail');
        $Subject = 'WordPress error';
        wp_mail($To, $Subject, $Output, array('Content-Type: text/html; charset=UTF-8'));
        }

    public function get_trace($trace)
        {
        $stack = '';
        $i = 1;

        unset($trace[0],$trace[1]); //Remove call to this function from stack trace
        foreach ($trace as $node)
            {
            $stack .= "#$i " . basename($node['file']) . "(" . $node['line'] . "): ";
            if (isset($node['class']))
                {
                $stack .= $node['class'] . "->";
                }
            $stack .= $node['function'] . "()" . PHP_EOL;
            $i++;
            }
        return $stack;
        }
    }