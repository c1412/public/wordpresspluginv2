<?php

namespace cds\api;

use cds\helpers\ApiHelper;

class DuesPayment
    {


    public ?string $id;
    public ?string $tranDate;
    public ?int $duesPayment;
    public ?string $cardno;
    public ?int $cardExpiresMonth;
    public ?int $cardExpiresYear;
    public ?string $ms;
    public ?string $note;
    public ?string $year;
    public ?string $payor;
    public ?string $payBy;
    public ?int $ccAmount;
    public ?string $authCode;
    public ?string $refNbr;
    public ?string $storedCardToken;
    public ?string $adjustedBillingClass;
    public ?bool $setupPaymentPlan;
    public ?bool $autoRenew;
    /** @var Contributions[]|null */
    public ?array $contributions;
    public ?string $duesPaidThru;
    public ?bool $reinstate;
    public ?string $reinstateMemberTypeCode;
    public ?int $duesBilling;
    public ?int $duesAdjustment;
    public ?string $originalBillingClass;
    public ?bool $noTranDateEdit;
    public ?bool $postZeroDues;
    public ?int $namesMoa;
    public ?int $firmsMoa;
    public ?bool $sendPaymentReceipt;

    /**
     * @param Contributions[]|null $contributions
     */
    public function __construct($Data = null)
        {
        if (is_string($Data))
            {
            $Data = $this->init($Data);
            }
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->id = $Data->id;
            $this->tranDate = $Data->tranDate;
            $this->duesPayment = $Data->duesPayment;
            $this->cardno = $Data->cardno;
            $this->cardExpiresMonth = $Data->cardExpiresMonth;
            $this->cardExpiresYear = $Data->cardExpiresYear;
            $this->ms = $Data->ms;
            $this->note = $Data->note;
            $this->year = $Data->year;
            $this->payor = $Data->payor;
            $this->payBy = $Data->payBy;
            $this->ccAmount = $Data->ccAmount;
            $this->authCode = $Data->authCode;
            $this->refNbr = $Data->refNbr;
            $this->storedCardToken = $Data->storedCardToken;
            $this->adjustedBillingClass = $Data->adjustedBillingClass;
            $this->setupPaymentPlan = $Data->setupPaymentPlan;
            $this->autoRenew = $Data->autoRenew;
            $this->contributions = ApiHelper::array_of_type('Contributions', $Data->Contributions);
            $this->duesPaidThru = $Data->duesPaidThru;
            $this->reinstate = $Data->reinstate;
            $this->reinstateMemberTypeCode = $Data->reinstateMemberTypeCode;
            $this->duesBilling = $Data->duesBilling;
            $this->duesAdjustment = $Data->duesAdjustment;
            $this->originalBillingClass = $Data->originalBillingClass;
            $this->noTranDateEdit = $Data->noTranDateEdit;
            $this->postZeroDues = $Data->postZeroDues;
            $this->namesMoa = $Data->namesMoa;
            $this->firmsMoa = $Data->firmsMoa;
            $this->sendPaymentReceipt = $Data->sendPaymentReceipt;
            }
        }
    private function init(string $Data)
        {
        return json_decode($Data);
        }

    public function getId(): ?string
        {
        return $this->id;
        }

    public function getTranDate(): ?string
        {
        return $this->tranDate;
        }

    public function getDuesPayment(): ?int
        {
        return $this->duesPayment;
        }

    public function getCardno(): ?string
        {
        return $this->cardno;
        }

    public function getCardExpiresMonth(): ?int
        {
        return $this->cardExpiresMonth;
        }

    public function getCardExpiresYear(): ?int
        {
        return $this->cardExpiresYear;
        }

    public function getMs(): ?string
        {
        return $this->ms;
        }

    public function getNote(): ?string
        {
        return $this->note;
        }

    public function getYear(): ?string
        {
        return $this->year;
        }

    public function getPayor(): ?string
        {
        return $this->payor;
        }

    public function getPayBy(): ?string
        {
        return $this->payBy;
        }

    public function getCcAmount(): ?int
        {
        return $this->ccAmount;
        }

    public function getAuthCode(): ?string
        {
        return $this->authCode;
        }

    public function getRefNbr(): ?string
        {
        return $this->refNbr;
        }

    public function getStoredCardToken(): ?string
        {
        return $this->storedCardToken;
        }

    public function getAdjustedBillingClass(): ?string
        {
        return $this->adjustedBillingClass;
        }

    public function getSetupPaymentPlan(): ?bool
        {
        return $this->setupPaymentPlan;
        }

    public function getAutoRenew(): ?bool
        {
        return $this->autoRenew;
        }

    /**
     * @return Contributions[]|null
     */
    public function getContributions(): ?array
        {
        return $this->contributions;
        }

    public function getDuesPaidThru(): ?string
        {
        return $this->duesPaidThru;
        }

    public function getReinstate(): ?bool
        {
        return $this->reinstate;
        }

    public function getReinstateMemberTypeCode(): ?string
        {
        return $this->reinstateMemberTypeCode;
        }

    public function getDuesBilling(): ?int
        {
        return $this->duesBilling;
        }

    public function getDuesAdjustment(): ?int
        {
        return $this->duesAdjustment;
        }

    public function getOriginalBillingClass(): ?string
        {
        return $this->originalBillingClass;
        }

    public function getNoTranDateEdit(): ?bool
        {
        return $this->noTranDateEdit;
        }

    public function getPostZeroDues(): ?bool
        {
        return $this->postZeroDues;
        }

    public function getNamesMoa(): ?int
        {
        return $this->namesMoa;
        }

    public function getFirmsMoa(): ?int
        {
        return $this->firmsMoa;
        }

    public function getSendPaymentReceipt(): ?bool
        {
        return $this->sendPaymentReceipt;
        }

    public function setId(?string $id): self
        {
        $this->id = $id;
        return $this;
        }

    public function setTranDate(?string $tranDate): self
        {
        $this->tranDate = $tranDate;
        return $this;
        }

    public function setDuesPayment(?int $duesPayment): self
        {
        $this->duesPayment = $duesPayment;
        return $this;
        }

    public function setCardno(?string $cardno): self
        {
        $this->cardno = $cardno;
        return $this;
        }

    public function setCardExpiresMonth(?int $cardExpiresMonth): self
        {
        $this->cardExpiresMonth = $cardExpiresMonth;
        return $this;
        }

    public function setCardExpiresYear(?int $cardExpiresYear): self
        {
        $this->cardExpiresYear = $cardExpiresYear;
        return $this;
        }

    public function setMs(?string $ms): self
        {
        $this->ms = $ms;
        return $this;
        }

    public function setNote(?string $note): self
        {
        $this->note = $note;
        return $this;
        }

    public function setYear(?string $year): self
        {
        $this->year = $year;
        return $this;
        }

    public function setPayor(?string $payor): self
        {
        $this->payor = $payor;
        return $this;
        }

    public function setPayBy(?string $payBy): self
        {
        $this->payBy = $payBy;
        return $this;
        }

    public function setCcAmount(?int $ccAmount): self
        {
        $this->ccAmount = $ccAmount;
        return $this;
        }

    public function setAuthCode(?string $authCode): self
        {
        $this->authCode = $authCode;
        return $this;
        }

    public function setRefNbr(?string $refNbr): self
        {
        $this->refNbr = $refNbr;
        return $this;
        }

    public function setStoredCardToken(?string $storedCardToken): self
        {
        $this->storedCardToken = $storedCardToken;
        return $this;
        }

    public function setAdjustedBillingClass(?string $adjustedBillingClass): self
        {
        $this->adjustedBillingClass = $adjustedBillingClass;
        return $this;
        }

    public function setSetupPaymentPlan(?bool $setupPaymentPlan): self
        {
        $this->setupPaymentPlan = $setupPaymentPlan;
        return $this;
        }

    public function setAutoRenew(?bool $autoRenew): self
        {
        $this->autoRenew = $autoRenew;
        return $this;
        }

    /**
     * @param Contributions[]|null $contributions
     */
    public function setContributions(?array $contributions): self
        {
        $this->contributions = $contributions;
        return $this;
        }

    public function setDuesPaidThru(?string $duesPaidThru): self
        {
        $this->duesPaidThru = $duesPaidThru;
        return $this;
        }

    public function setReinstate(?bool $reinstate): self
        {
        $this->reinstate = $reinstate;
        return $this;
        }

    public function setReinstateMemberTypeCode(?string $reinstateMemberTypeCode): self
        {
        $this->reinstateMemberTypeCode = $reinstateMemberTypeCode;
        return $this;
        }

    public function setDuesBilling(?int $duesBilling): self
        {
        $this->duesBilling = $duesBilling;
        return $this;
        }

    public function setDuesAdjustment(?int $duesAdjustment): self
        {
        $this->duesAdjustment = $duesAdjustment;
        return $this;
        }

    public function setOriginalBillingClass(?string $originalBillingClass): self
        {
        $this->originalBillingClass = $originalBillingClass;
        return $this;
        }

    public function setNoTranDateEdit(?bool $noTranDateEdit): self
        {
        $this->noTranDateEdit = $noTranDateEdit;
        return $this;
        }

    public function setPostZeroDues(?bool $postZeroDues): self
        {
        $this->postZeroDues = $postZeroDues;
        return $this;
        }

    public function setNamesMoa(?int $namesMoa): self
        {
        $this->namesMoa = $namesMoa;
        return $this;
        }

    public function setFirmsMoa(?int $firmsMoa): self
        {
        $this->firmsMoa = $firmsMoa;
        return $this;
        }

    public function setSendPaymentReceipt(?bool $sendPaymentReceipt): self
        {
        $this->sendPaymentReceipt = $sendPaymentReceipt;
        return $this;
        }

    }