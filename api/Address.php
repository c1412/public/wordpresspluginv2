<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Address
	{

	public ?string $Room;
	public ?string $Line1;
	public ?string $Line2;
	public ?string $Line3;
	public ?string $City;
	public ?string $StateCode;
	public ?string $Zip;
	public ?string $Phone;
	public ?string $PhoneExt;
	public ?string $Fax;

	public function __construct(?object $Data = null)
		{
		if (is_object($Data) && !is_wp_error($Data))
			{
			$this->Room = $Data->Room;
			$this->Line1 = $Data->Line1;
			$this->Line2 = $Data->Line2;
			$this->Line3 = $Data->Line3;
			$this->City = $Data->City;
			$this->StateCode = $Data->StateCode;
			$this->Zip = $Data->Zip;
			$this->Phone = $Data->Phone;
			$this->PhoneExt = $Data->PhoneExt;
			$this->Fax = $Data->Fax;
			}
		}
	}