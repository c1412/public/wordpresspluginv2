<?php

namespace cds\api;

use cds\helpers\ApiHelper;

defined('ABSPATH') or exit;

class UserDefinedLists
    {


    public ?string $Field;

    public ?string $Caption;

    public ?string $ListKey;
    /** @var Values[]|null */
    public ?array $Values;

    /**
     * Summary of __construct
     * @param object|null $Data
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->Field = $Data->Field;
            $this->Caption = $Data->Caption;
            $this->Caption = $Data->ListKey;
            $this->Values = ApiHelper::array_of_type('Values', $Data->Values);
            }
        }
    }