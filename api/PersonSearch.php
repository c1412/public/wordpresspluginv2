<?php

namespace cds\api;
use cds\helpers\ApiHelper;

defined('ABSPATH') or exit;

class PersonSearch
    {

    public ?int $ResultCount;
    public ?string $PersonID;
    public ?string $EmailAddress;
    public ?string $EmailAddress2;
    public ?string $FirstName;
    public ?string $LastName;
    public ?string $FirmName;
    public ?string $HomeZip;
    public ?string $FirmZip;
    /** @var PersonSearchHit[]|null */
    public ?array $SearchHits;

    public function __construct(?string $Email = null, ?string $LastName = null, ?string $FirstName = null, ?string $MobileNumber = null, string $FirmCode = null)
        {

        if (!empty($Email) || !empty($LastName) || !empty($FirstName) || !empty($FirstName) || !empty($MobileNumber) || !empty($FirmCode))
            {
            $Data = $this->init($Email, $LastName, $FirstName, $MobileNumber, $FirmCode);
            }
        else
            {
            $Data = null;
            }

        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ResultCount = $Data->ResultCount;
            $this->PersonID = $Data->PersonID;
            $this->EmailAddress = $Data->EmailAddress;
            $this->EmailAddress2 = $Data->EmailAddress2;
            $this->FirmName = $Data->FirmName;
            $this->LastName = $Data->LastName;
            $this->FirmName = $Data->FirmName;
            $this->HomeZip = $Data->HomeZip;
            $this->FirmZip = $Data->FirmZip;
            $this->SearchHits = ApiHelper::array_of_type('PersonSearchHit',$Data->SearchHits);
            }
        }

    private function init(?string $Email = null, ?string $LastName = null, ?string $FirstName = null, ?string $MobileNumber = null, string $FirmCode = null)
        {
        $Connection = new Connection('GET', 'PersonSearch', '?email=' . $Email . '&lastname=' . $LastName . '&firstname=' . $FirstName . '&mobileNumber=' . $MobileNumber . '&firmCode=' . $FirmCode);
        return $Connection->Response;
        }

    /**
     * Returns a PersonSearch object based on the criteria provided.
     * @param string|null $Email
     * @param string|null $LastName
     * @param string|null $FirstName
     * @param string|null $MobileNumber
     * @param string $FirmCode
     * @return \WP_Error|PersonSearch
     */
    public function get(?string $Email = null, ?string $LastName = null, ?string $FirstName = null, ?string $MobileNumber = null, string $FirmCode = null): \WP_Error|PersonSearch
        {
        $Data = $this->init($Email, $LastName, $FirstName, $MobileNumber, $FirmCode);
        return new self($Data);
        }
    }