<?php

namespace cds\api;

defined('ABSPATH') or exit;

class ListItem
	{

	public ?string $Code;
	public ?string $Description;
	public ?string $Misc1;

	/**
	 * @param object|null $Data
	 * @suppress 0416
	 */
	public function __construct(?object $Data = null)
		{
		if (!is_wp_error($Data) && is_object($Data))
			{
			$this->Code = $Data->Code;
			$this->Description = $Data->Description;
			$this->Misc1 = $Data->Misc1;
			}
		}
	}