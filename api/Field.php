<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Field
    {

    public ?string $DtoElement;
    public ?bool $IsString;
    public ?bool $IsInteger;
    public ?bool $IsDate;
    public ?bool $IsBoolean;
    public ?bool $IsCodeLookup;
    public ?string $IsCodeListKey;
    public ?int $MaxCodes;
    public ?int $StringMaxLength;
    public ?string $ResetValue;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->DtoElement = $Data->DtoElement;
            $this->IsString = $Data->IsString;
            $this->IsInteger = $Data->IsInteger;
            $this->IsDate = $Data->IsDate;
            $this->IsBoolean = $Data->IsBoolean;
            $this->IsCodeLookup = $Data->IsCodeLookup;
            $this->IsCodeListKey = $Data->IsCodeListKey;
            $this->MaxCodes = $Data->MaxCodes;
            $this->StringMaxLength = $Data->StringMaxLength;
            $this->ResetValue = $Data->ResetValue;
            }
        }
    }