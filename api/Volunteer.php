<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Volunteer
    {


    public ?string $ID;

    public ?string $VolunteerAgencyCodes;

    public ?string $VolunteerBio;

    public ?string $VolunteerCredentials;

	/**
     * Summary of __construct
     * @param object|null $Data
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ID = trim($Data->ID);
            $this->VolunteerAgencyCodes = $Data->VolunteerAgencyCodes;
            $this->VolunteerBio = $Data->VolunteerBio;
            $this->VolunteerCredentials = $Data->VolunteerCredentials;
            }
        }
    }