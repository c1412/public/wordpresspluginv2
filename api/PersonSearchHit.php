<?php

namespace cds\api;

defined('ABSPATH') or exit;

class PersonSearchHit
    {
    public ?string $PersonID;
    public ?string $EmailAddress;
    public ?string $EmailAddress2;
    public ?string $FirstName;
    public ?string $LastName;
    public ?string $FirmName;
    public ?string $HomeZip;
    public ?string $FirmZip;

    /**
     * Summary of __construct
     * @param object|null $Data
     * @suppress 0416
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->PersonID = $Data->PersonID;
            $this->EmailAddress = $Data->EmailAddress;
            $this->EmailAddress2 = $Data->EmailAddress2;
            $this->EmailAddress = $Data->EmailAddress;
            $this->FirmName = $Data->FirmName;
            $this->LastName = $Data->LastName;
            $this->FirmName = $Data->FirmName;
            $this->HomeZip = $Data->omeZip;
            $this->FirmZip = $Data->FirmZip;
            }
        }
    }