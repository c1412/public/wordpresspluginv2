<?php

namespace cds\api;

defined('ABSPATH') or exit;

class FirmChange
    {

    public ?string $ChangeDate;
    public ? FirmKey $Firm;

    /**
     * @param object|null $Data
     * @suppress 0416
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ChangeDate = $Data->ChangeDate;
            $this->Firm = new FirmKey($Data->Firm);
            }
        }
    }