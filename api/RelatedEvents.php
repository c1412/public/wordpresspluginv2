<?php

namespace cds\api;

defined('ABSPATH') or exit;

class RelatedEvents
    {
    public ?string $EventCode;
    public ?string $EventYear;
    public ?string $EventCodeYear;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->EventCode = $Data->EventCode;
            $this->EventYear = $Data->EventYear;
            $this->EventCodeYear = $Data->EventCodeYear;
            }
        }
    }