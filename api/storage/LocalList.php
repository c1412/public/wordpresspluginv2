<?php

namespace cds\api\storage;

use cds\api\Logging;
use WP_Error;

class LocalList
    {
    public ?string $ListType;
    public ?string $ListJSON;
    public ?bool $isDirty;

    public function __construct(?string $ListType = null)
        {

        if (!is_string($ListType))
            {
            $Data = $this->init($ListType);
            }

        // Don't try to process an error or null
        if (!empty($Data))
            {
            $this->ListType = $Data->ListType;
            $this->ListJSON = $Data->ListJSON;
            }
        }

    /**
     * Used to populate the FirmStorage object.
     * @param string $ListType
     * @return mixed
     */
    private function init(string $ListType)
        {
        global $wpdb;

        $Result = null;

        $Query = $wpdb->prepare("SELECT * FROM cds_lists WHERE ListType = %s", array('ListType' => $ListType));
        $Result = $wpdb->get_results($Query)[0];

        if ($wpdb->last_error)
            {
            $Data = [];
            $Data = ['Query:' => $Query];
            $Data = ['Error:' => $wpdb->last_error];
            new Logging(new WP_Error('SQL', __METHOD__, $Data));
            }

        return $Result;
        }

    /**
     * Delete a list storage record.
     * @param string $ListType
     * @return void
     */
    public function delete(string $ListType)
        {

        global $wpdb;

        $Query = $wpdb->prepare("DELETE FROM cds_lists WHERE ListType = %s LIMIT 1", array(trim($ListType)));
        $wpdb->query($Query);

        if ($wpdb->last_error)
            {
            $Data = [];
            $Data = ['Query:' => $Query];
            $Data = ['Error:' => $wpdb->last_error];
            $Result = new WP_Error('SQL', __METHOD__, $Data);
            new Logging($Result);
            }
        }

    }