<?php

namespace cds\api\storage;

class LocalTables
    {
    public function create_persons_table()
        {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = " CREATE TABLE cds_persons (
                ID varchar(8) NOT NULL,
                LinkedFirmCode varchar(8) DEFAULT NULL,
                WordPressUserID varchar(8) DEFAULT NULL,
                Email varchar(50) NOT NULL,
                EmailAddress2 varchar(50) DEFAULT NULL,
                PersonJSON JSON DEFAULT NULL,
                LastLogin datetime NULL DEFAULT NULL,
                LastVisit datetime NULL DEFAULT NULL,
                IsDirty tinyint(1) NOT NULL DEFAULT 0,
                PRIMARY KEY (ID),
                UNIQUE KEY Email (Email),
                UNIQUE KEY WordPressUserID (WordPressUserID)
              ) $charset_collate;";

        dbDelta($sql);
        }

    public function create_firms_table()
        {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE cds_firms (
                ID varchar(15) NOT NULL,
                IsDirty tinyint(1) NOT NULL DEFAULT 0,
                FirmJSON JSON DEFAULT NULL,
                PRIMARY KEY (ID)
              ) $charset_collate;";

        dbDelta($sql);
        }

    public function create_lists_table()
        {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE cds_lists (
                ListType varchar(8) NOT NULL,
                ListJSON JSON DEFAULT NULL,
                IsDirty tinyint(1) NOT NULL DEFAULT 0
              ) $charset_collate;";

        dbDelta($sql);
        }

    public function create_events_table()
        {
        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE cds_events (
                    ID varchar(15) NOT NULL,
                    IsDirty tinyint(1) NOT NULL DEFAULT 0,
                    EventJSON JSON DEFAULT NULL,
                    PRIMARY KEY (ID)
                  ) $charset_collate;";

        dbDelta($sql);
        }
    }