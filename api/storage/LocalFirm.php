<?php

namespace cds\api\storage;

use cds\api\Logging;
use WP_Error;

class LocalFirm
    {
    public ?string $ID;
    public ?string $IsDirty;
    public ?string $FirmJSON;

    public function __construct(mixed $ID = null)
        {
        // Allowed to use PersonID
        if (is_string($ID))
            {
            $Data = self::init($ID);
            }

        // Don't try to process an error or null
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ID = trim($Data->ID);
            $this->IsDirty = $Data->IsDirty;
            $this->FirmJSON = $Data->FirmJSON;
            }
        }

    private function init(string $ID)
        {
        global $wpdb;

        $Result = null;
        // The SQL query
        $Query = $wpdb->prepare("SELECT * FROM cds_firms WHERE ID = %s LIMIT 1", array(trim($ID)));
        // The results
        $Result = $wpdb->get_results($Query)[0];

        // Was a database error spawned
        if ($wpdb->last_error)
            {
            $Data = [];
            $Data = ['Query:' => $Query];
            $Data = ['Error:' => $wpdb->last_error];
            new Logging(new WP_Error('SQL', __METHOD__, $Data));
            }
        return $Result;
        }
    }