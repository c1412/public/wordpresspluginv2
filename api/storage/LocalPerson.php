<?php

namespace cds\api\storage;

use cds\api\Logging;
use WP_Error;

class LocalPerson
    {
    public ?string $ID;
    public ?string $LinkedFirmCode;
    public ?string $WordPressUserID;
    public ?string $Email;
    public ?string $EmailAddress2;
    public ?string $IsDirty;
    public ?string $PersonJSON;
    public ?string $LastLogin;
    public ?string $LastVisit;

    public function __construct($ID = null)
        {
        // Allowed to use PersonID
        if (is_string($ID))
            {
            $Data = $this->init($ID);
            }

        // Don't try to process an error or null
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ID = trim($Data->ID);
            $this->LinkedFirmCode = trim($Data->LinkedFirmCode);
            $this->WordPressUserID = trim($Data->WordPressUserID);
            $this->Email = $Data->Email;
            $this->EmailAddress2 = $Data->EmailAddress2;
            $this->IsDirty = $Data->IsDirty;
            $this->PersonJSON = $Data->PersonJSON;
            $this->LastLogin = $Data->LastLogin;
            $this->LastVisit = $Data->LastVisit;
            }
        }
    /**
     * Private function to get a storage object.
     * @param string $PersonID
     * @return mixed
     */
    private function init(string $ID)
        {
        $Result = null;

        global $wpdb;
        // The SQL query
        $Query = $wpdb->prepare("SELECT * FROM cds_persons WHERE ID = %s LIMIT 1", array(trim($ID)));
        // The results
        $Result = $wpdb->get_results($Query)[0];

        // Was a database error spawned
        if ($wpdb->last_error)
            {
            // Yes, so we log it
            $Data = [];
            $Data = ['Query:' => $Query];
            $Data = ['Error:' => $wpdb->last_error];
            new Logging($Result = new WP_Error('SQL', __METHOD__, $Data));
            }

        return $Result;

        }

    /**
     * Deletes the persons storage object.
     * @param string $PersonID
     * @return void
     */
    public function delete(string $PersonID): void
        {
        global $wpdb;

        $Query = $wpdb->prepare("DELETE FROM cds_persons WHERE ID = %s LIMIT 1", array(trim($PersonID)));
        $wpdb->query($Query);

        if ($wpdb->last_error)
            {

            $Data = [];
            $Data = ['Query:' => $Query];
            $Data = ['Error:' => $wpdb->last_error];
            $Result = new WP_Error('SQL', __METHOD__, $Data);
            new Logging($Result);
            }
        }

    }