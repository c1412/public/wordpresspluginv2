<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Values
    {

    public ?string $Value;
    /**
     * Summary of __construct
     * @param object|null $Data
     * @suppress 0416
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->Value = $Data->Value;
            }
        }
    }