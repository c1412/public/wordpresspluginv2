<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Contact
    {

    public ?string $Id;

    public ?string $NamesId;

    public ?string $LegislatorNamesId;
    /** @var int|null */
    public ?int $ContactLevel;

    public ?string $RecordAdded;

    public ?string $RecordAddedBy;

    public ?string $RecordLastUpdated;

    public ?string $RecordLastUpdatedBy;
    /** @var ContactLog[]|null */
    public ?array $ContactLog;
    /** @var ContactLog[]|null */
    public ?array $AddContactLog;
    /** @var ContactLog[]|null */
    public ?array $RemoveContactLog;

    public function __construct(object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->Id = trim($Data->NamesID);
            $this->NamesId = trim($Data->NamesID);
            $this->LegislatorNamesId = trim($Data->LegislatorNamesId);
            $this->ContactLog = $Data->ContactLog;
            $this->RecordAdded = $Data->RecordAdded;
            $this->RecordAddedBy = $Data->RecordAddedBy;
            $this->RecordLastUpdated = $Data->RecordLastUpdated;
            $this->ContactLog = new ContactLog($Data->ContactLog);
            $this->AddContactLog = new ContactLog($Data->AddContactLog);
            $this->RemoveContactLog = new ContactLog($Data->RemoveContactLog);
            }
        }
    }