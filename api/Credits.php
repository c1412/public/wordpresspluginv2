<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Credits
    {
    public ?string $CreditCategoryCode;
    public ?int $Credits;
    public ?string $ApprovalCode;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->CreditCategoryCode = $Data->CreditCategoryCode;
            $this->Credits = $Data->Credits;
            $this->ApprovalCode = $Data->ApprovalCode;
            }

        }
    }
