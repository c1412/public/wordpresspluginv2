<?php

namespace cds\api;

class Contributions
    {
    public ?string $code;
    public ?string $fund;
    public ?string $anonymous;
    public ?int $amt;

    public function __construct(object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->code = $Data->code;
            $this->fund = $Data->fund;
            $this->anonymous = $Data->anonymous;
            $this->amt = $$Data->amt;
            }
        }

    public function getCode(): ?string
        {
        return $this->code;
        }

    public function getFund(): ?string
        {
        return $this->fund;
        }

    public function getAnonymous(): ?string
        {
        return $this->anonymous;
        }

    public function getAmt(): ?int
        {
        return $this->amt;
        }

    public function setCode(?string $code): self
        {
        $this->code = $code;
        return $this;
        }

    public function setFund(?string $fund): self
        {
        $this->fund = $fund;
        return $this;
        }

    public function setAnonymous(?string $anonymous): self
        {
        $this->anonymous = $anonymous;
        return $this;
        }

    public function setAmt(?int $amt): self
        {
        $this->amt = $amt;
        return $this;
        }
    }