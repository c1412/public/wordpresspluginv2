<?php

namespace cds\api;

defined('ABSPATH') or exit;

class ContactLog
	{

	public ?string $Id;
	public ?string $NamesId;
	public ?string $LegislatorId;
	public ?string $ContactSourceCode;
	public ?string $Year;
	public ?string $PoliticalPartyCode;
	public ?string $ContactTypeCodes;
	public ?string $Note;

	public function __construct(?object $Data = null)
		{
		if (is_object($Data) && !is_wp_error($Data))
			{
			$this->Id = trim($Data->NamesID);
			$this->NamesId = trim($Data->NamesID);
			$this->LegislatorId = trim($Data->LegislatorId);
			$this->ContactSourceCode = $Data->ContactSourceCode;
			$this->Year = $Data->Year;
			$this->PoliticalPartyCode = $Data->PoliticalPartyCode;
			$this->ContactTypeCodes = $Data->ContactTypeCodes;
			$this->Note = $Data->Note;
			}
		}
	}