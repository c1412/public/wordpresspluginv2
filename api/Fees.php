<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Fees
    {
    public ?string $Ty;
    public ?string $Ty2;
    public ?int $Amount;
    public ?string $FeeDate;
    public ?string $Description;
    public ?string $ApplyToMemberType;
    public ?string $RecordAdded;
    public ?string $RecordAddedBy;
    public ?string $RecordLastUpdated;
    public ?string $RecordLastUpdatedBy;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->Ty = $Data->Ty;
            $this->Ty2 = $Data->Ty2;
            $this->Amount = $Data->Amount;
            $this->FeeDate = $Data->FeeDate;
            $this->Description = $Data->Description;
            $this->ApplyToMemberType = $Data->ApplyToMemberType;
            $this->RecordAdded = $Data->RecordAdded;
            $this->RecordAddedBy = $Data->RecordAddedBy;
            $this->RecordLastUpdated = $Data->RecordLastUpdated;
            $this->RecordLastUpdatedBy = $Data->RecordLastUpdatedBy;
            }
        }
    }