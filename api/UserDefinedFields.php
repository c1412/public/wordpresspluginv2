<?php

namespace cds\api;

defined('ABSPATH') or exit;

class UserDefinedFields
    {


    public ?string $Value;

    public ?string $Caption;

    public ?string $Field;

    public ?bool $IsString;

    public ?bool $IsInteger;

    public ?bool $IsDecimal;

    public ?bool $IsDate;
    /** @var int|null */
    public ?int $DecimalScale;
    /** @var int|null */
    public ?int $DecimalPrecision;
    /** @var int|null */
    public ?int $MaxLength;

    /**
     * Summary of __construct
     * @param object|null $Data
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->Value = $Data->Value;
            $this->Caption = $Data->Caption;
            $this->Field = $Data->Caption;
            $this->IsString = $Data->IsString;
            $this->IsInteger = $Data->IsInteger;
            $this->IsDecimal = $Data->IsDecimal;
            $this->IsDate = $Data->IsDate;
            $this->DecimalScale = $Data->DecimalScale;
            $this->DecimalPrecision = $Data->DecimalPrecision;
            $this->MaxLength = $Data->MaxLength;
            }
        }
    }