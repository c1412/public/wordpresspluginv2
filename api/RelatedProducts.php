<?php

namespace cds\api;

defined('ABSPATH') or exit;

class RelatedProducts
    {
    public ?string $ProductCode;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->ProductCode = $Data->ProductCode;
            }
        }
    }