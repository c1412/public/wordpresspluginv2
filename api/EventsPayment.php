<?php

namespace cds\api;

class EventsPayment
    {
    public ?string $id;
    public ?string $payFirm;
    public ?string $tranDate;
    public ?string $cardno;
    public ?string $ccv;
    public ?string $exp;
    public ?string $note;
    public ?string $yr;
    public ?string $code1;
    public ?string $payor;
    public ?string $payBy;
    public ?int $ccAmount;
    public ?bool $noTranDateEdit;
    public ?bool $postCCtoPending;
    public ?int $namesKitty;
    public ?int $firmsKitty;
    public ?string $authCode;
    public ?string $refNbr;
    public ?string $ccAccount;

    public function __construct($Data = null)
        {
        if (is_string($Data))
            {
            $Data = $this->init($Data);
            }
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->id = $Data->id;
            $this->payFirm = $Data->payFirm;
            $this->tranDate = $Data->tranDate;
            $this->cardno = $Data->cardno;
            $this->ccv = $Data->ccv;
            $this->exp = $Data->exp;
            $this->note = $Data->note;
            $this->yr = $Data->yr;
            $this->code1 = $Data->code1;
            $this->payor = $Data->payor;
            $this->payBy = $Data->payBy;
            $this->ccAmount = $Data->ccAmount;
            $this->noTranDateEdit = $Data->noTranDateEdit;
            $this->postCCtoPending = $Data->postCCtoPending;
            $this->namesKitty = $Data->namesKitty;
            $this->firmsKitty = $Data->firmsKitty;
            $this->authCode = $Data->authCode;
            $this->refNbr = $Data->refNbr;
            $this->ccAccount = $Data->ccAccount;
            }
        }
    private function init(string $Data)
        {
        return json_decode($Data);
        }

    public function setId(?string $id): void
        {
        $this->id = $id;
        }

    public function setPayFirm(?string $payFirm): void
        {
        $this->payFirm = $payFirm;
        }

    public function setTranDate(?string $tranDate): void
        {
        $this->tranDate = $tranDate;
        }

    public function setCardno(?string $cardno): void
        {
        $this->cardno = $cardno;
        }

    public function setCcv(?string $ccv): void
        {
        $this->ccv = $ccv;
        }

    public function setExp(?string $exp): void
        {
        $this->exp = $exp;
        }

    public function setNote(?string $note): void
        {
        $this->note = $note;
        }

    public function setYr(?string $yr): void
        {
        $this->yr = $yr;
        }

    public function setCode1(?string $code1): void
        {
        $this->code1 = $code1;
        }

    public function setPayor(?string $payor): void
        {
        $this->payor = $payor;
        }

    public function setPayBy(?string $payBy): void
        {
        $this->payBy = $payBy;
        }

    public function setCcAmount(?int $ccAmount): void
        {
        $this->ccAmount = $ccAmount;
        }

    public function setNoTranDateEdit(?bool $noTranDateEdit): void
        {
        $this->noTranDateEdit = $noTranDateEdit;
        }

    public function setPostCCtoPending(?bool $postCCtoPending): void
        {
        $this->postCCtoPending = $postCCtoPending;
        }

    public function setNamesKitty(?int $namesKitty): void
        {
        $this->namesKitty = $namesKitty;
        }

    public function setFirmsKitty(?int $firmsKitty): void
        {
        $this->firmsKitty = $firmsKitty;
        }

    public function setAuthCode(?string $authCode): void
        {
        $this->authCode = $authCode;
        }

    public function setRefNbr(?string $refNbr): void
        {
        $this->refNbr = $refNbr;
        }

    public function setCcAccount(?string $ccAccount): void
        {
        $this->ccAccount = $ccAccount;
        }
    }