<?php

namespace cds\api;

defined('ABSPATH') or exit;

class FirmKey
	{

	public ?string $ID;
	public ?string $FirmCode;
	public ?string $LastModified;

    /**
     * @param object|null $Data
     * @suppress 0416
     */
	public function __construct(?object $Data = null)
		{
		if (!is_wp_error($Data) && is_object($Data))
			{
			$this->ID = trim($Data->FirmCode);
			$this->FirmCode = trim($Data->FirmCode);
			$this->LastModified = $Data->LastModified;
			}
		}
	}