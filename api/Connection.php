<?php

namespace cds\api;

use cds\helpers\Encryption;

defined('ABSPATH') or exit;

use WP_Error;

/**
 * Used to build a connection to the AM.Net API.
 */
class Connection
    {

    private string $Url;
    private string $User;
    private string $Key;
    private bool $IsDebugOn;
    public mixed $Response;

    /**
     * Connect to the API EX: $http_response = Connection::get('GET', 'Person','/559150/?includeLegislative=false');
     * @param string|null $Verb Accepts 'GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'TRACE', 'OPTIONS', or 'PATCH'
     * @param string|null $EndPoint The endpoint of the API (EX: Person).
     * @param string|null $Filter The filter for the endpoint (EX /559150/?includeLegislative=true)
     * @param string|null $Body $Body Well formed JSON.
     */
    public function __construct(?string $Verb = null, ?string $EndPoint = null, ?string $Filter = null, ?string $Body = null)
        {

        if (get_option('cdsSiteUrl') === get_site_url())
            {
            $this->Url = get_option('cdsUrl');
            $this->IsDebugOn = false;
            }
        else
            {
            $this->Url = get_option('cdsTestUrl');
            $this->IsDebugOn = true;
            }

        $this->User = get_option('cdsUser');
        $this->Key = Encryption::decrypt(get_option('cdsKey'));

        if (!empty($Verb))
            {
            $this->Response = $this->get($Verb, $EndPoint, $Filter, $Body);
            }
        }

    /**
     * Call the API with defined options. EX: $http_response = Connection::get('GET', 'Person','/559150/?includeLegislative=false');
     * @param string $Verb Accepts 'GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'TRACE', 'OPTIONS', or 'PATCH'
     * @param string $EndPoint The endpoint of the API (EX: Person).
     * @param string|null $Filter The filter for the endpoint (EX /559150/?includeLegislative=true)
     * @param string|null $Body $Body Well formed JSON.
     * @return \WP_Error|mixed
     */
    public function get(string $Verb, string $EndPoint, ?string $Filter = null, ?string $Body = null)
        {

        $Url = $this->Url . $EndPoint . $Filter;
        $Args = [
            'method'  => $Verb,
            'timeout' => 30,
            'headers' => array(
                'ApiUser'      => $this->User,
                'ApiKey'       => $this->Key,
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json; charset=utf-8',
            ),
        ];

        if (!empty($Body))
            {
            $Args['body'] = $Body;
            }

        $Response = wp_remote_request($Url, $Args);
        $ResponseChecked = $this->extended_error_check($Url, $Args, $Response, $this->IsDebugOn);

        if (is_wp_error($ResponseChecked))
            {
            if ($this->IsDebugOn)
                {
                new Logging($ResponseChecked);
                }
            return $ResponseChecked;
            }
        else
            {
            return $ResponseChecked;
            }
        }

    /**
     * Checking the response to see if we have any errors.
     * @param string $Url
     * @param array $Args
     * @param mixed $Response Accepts the results of wp_remote_request
     * @return mixed \WP_error|array Returns WP_Error or and array from wp_remote_request.
     */
    private function extended_error_check(string $Url, array $Args, $Response, bool $IsDebugOn)
        {

        // Remove the API Key so we don't log it publicly.
        if (is_array($Args) && array_key_exists('ApiKey', $Args['headers']))
            {
            $Args['headers']['ApiKey'] = 'Removed Intentionally While Logging';
            }

        // Did Curl Fail?
        if (is_wp_error($Response))
            {
            if ($IsDebugOn)
                {
                new Logging($Response);
                }
            return $Response;
            }

        // If we have a successful response
        if (in_array($Response['response']['code'], array(100, 200, 201, 202, 204)))
            {
            $ResponseBody = wp_remote_retrieve_body($Response);
            return json_decode($ResponseBody);
            }
        else
            {
            $ResponseBody = json_decode($Response['response']['body']);
            $ErrorCode = $Response['response']['code'];
            $ErrorMessage = $Response['response']['message'];
            $ErrorDetails = array(
                'URL'  => $Url,
                'ARGS' => $Args
            );

            if ($ResponseBody->Message)
                {
                $ErrorMessage = $ResponseBody->Message;
                }

            if ($ResponseBody->Description)
                {
                $ErrorMessage = $ResponseBody->Description;
                }

            if ($ResponseBody->ErrorCode)
                {
                $ErrorCode = $ResponseBody->ErrorCode;
                }

            $Error = new WP_Error($ErrorCode, $ErrorMessage, $ErrorDetails);

            if ($IsDebugOn)
                {
                new Logging($Error);
                }

            return $Error;
            }
        }
    }