<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Leaders
    {
    public ?string $NamesId;
    public ?string $SessionCode;
    public ?int $RatingKnowledge;
    public ?int $RatingMaterials;
    public ?int $RatingPresentationMaterials;
    public ?int $RatingRelevance;
    public ?int $RatingOverall;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->NamesId = $Data->NamesId;
            $this->SessionCode = $Data->SessionCode;
            $this->RatingKnowledge = $Data->RatingKnowledge;
            $this->RatingMaterials = $Data->RatingMaterials;
            $this->RatingPresentationMaterials = $Data->RatingPresentationMaterials;
            $this->RatingRelevance = $Data->RatingRelevance;
            $this->RatingOverall = $Data->RatingOverall;
            }
        }
    }