<?php

namespace cds\api;

use cds\api\storage\LocalFirm;
use cds\helpers\ApiHelper;
use cds\options\Cron;
use DateTimeZone;
use WP_Error;

defined('ABSPATH') or exit;

use DateTime;

class Firm
    {
    public ?string $ID;
    public ?string $FirmCode;
    public ?string $FirmName;
    public ?string $FirmName1;
    public ?string $FirmName2;
    public ?string $IndexName;
    public ?string $AICPANumber;
    public ?string $Phone;
    public ?string $Fax;
    public ?string $Flag;
    public ?string $APTermsCode;
    public ?bool $APFlag;
    public ?bool $PaysDues;
    public ?int $MemberCount;
    public ?int $NonmemberCount;
    public ?string $MailingListCodes;
    public ?FirmKey $MainOffice;
    public ?string $GeneralBusinessCode;
    public ?string $SpecificBusinessCode;
    public ?string $FirmAdminEmail;
    public ?string $PersonInChargeName;
    public ?string $PersonInChargeId;
    /** @var FirmKey[] */
    public ?array $BranchOffices;
    public ?Address $Address;
    public ?string $Website;
    public ?int $NumberOfPartners;
    public ?int $NumberOfProfessionalStaff;
    public ?bool $ClientReferralActive;
    public ?array $ClientReferralChapters;
    public array $ClientReferralCities;
    /** @var ?string[] */
    public ?array $ClientReferralIndustries;
    public ?array $ClientReferralServicesOffered;
    public ?string $ClientReferralContact1;
    public ?string $ClientReferralContact2;
    public ?string $ClientReferralContact3;
    public ?string $ClientReferralContact4;
    public ?string $ClientReferralMarketingDescription;
    public ?string $ClientReferralMissionStatement;
    public ?string $ClientReferralTagline;
    public ?PersonKey $ClientReferralContact1Link;
    public ?PersonKey $ClientReferralContact2Link;
    public ?string $ClientReferralStaffSize;
    public ?Facility $Facility;
    public ?array $LinkedPersons;
    /** @var UserDefinedFields[] */
    public ?array $UserDefinedFields;
    /** @var UserDefinedLists[] */
    public ?array $UserDefinedLists;
    public ?string $ClientReferralYearsInBusiness;

    /**
     * Returns a Firm from the API
     * @param string|object|null
     * @suppress 0416
     */
    public function __construct(mixed $Data = null)
        {

        if (is_string($Data))
            {
            $Data = $this->init($Data);
            }

        // Don't try to process an error or null
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ID = trim($Data->FirmCode); // Added for consistency
            $this->FirmCode = trim($Data->FirmCode);
            $this->IndexName = $Data->Indexname ?? $Data->IndexName;
            $this->FirmName = trim($Data->FirmName1) . ' ' . trim($Data->FirmName2); // Added because it should exist by default.
            $this->FirmName1 = $Data->FirmName1;
            $this->FirmName2 = $Data->FirmName2;
            $this->AICPANumber = $Data->AICPANumber;
            $this->Phone = $Data->Phone;
            $this->Fax = $Data->Fax;
            $this->Flag = $Data->Flag;
            $this->APTermsCode = $Data->APTermsCode;
            $this->APFlag = $Data->APFlag;
            $this->PaysDues = $Data->PaysDues;
            $this->MemberCount = $Data->MemberCount;
            $this->NonmemberCount = $Data->NonmemberCount;
            $this->MailingListCodes = $Data->MailingListCodes;
            $this->MainOffice = new FirmKey($Data->MainOffice);
            $this->GeneralBusinessCode = $Data->GeneralBusinessCode;
            $this->SpecificBusinessCode = $Data->SpecificBusinessCode;
            $this->FirmAdminEmail = $Data->FirmAdminEmail;
            $this->PersonInChargeName = $Data->PersonInChargeName;
            $this->PersonInChargeId = $Data->PersonInChargeId;
            $this->BranchOffices = ApiHelper::array_of_type('FirmKey', $Data->BranchOffices);
            $this->Address = new Address($Data->Address);
            $this->Website = $Data->Website;
            $this->NumberOfPartners = $Data->NumberOfPartners;
            $this->NumberOfProfessionalStaff = $Data->NumberOfProfessionalStaff;
            $this->ClientReferralActive = $Data->ClientReferralActive;
            $this->ClientReferralChapters = ApiHelper::array_of_type(null, $Data->ClientReferralChapters);
            $this->ClientReferralCities = ApiHelper::array_of_type(null, $Data->ClientReferralCities);
            $this->ClientReferralIndustries = ApiHelper::array_of_type(null, $Data->ClientReferralIndustries);
            $this->ClientReferralServicesOffered = ApiHelper::array_of_type(null, $Data->ClientReferralServicesOffered);
            $this->ClientReferralContact1 = $Data->ClientReferralContact1;
            $this->ClientReferralContact2 = $Data->ClientReferralContact2;
            $this->ClientReferralContact3 = $Data->ClientReferralContact3;
            $this->ClientReferralContact4 = $Data->ClientReferralContact4;
            $this->ClientReferralMarketingDescription = $Data->ClientReferralMarketingDescription;
            $this->ClientReferralMissionStatement = $Data->ClientReferralMissionStatement;
            $this->ClientReferralTagline = $Data->ClientReferralTagline;
            $this->ClientReferralContact1Link = new PersonKey($Data->ClientReferralContact1Link);
            $this->ClientReferralContact2Link = new PersonKey($Data->ClientReferralContact2Link);
            $this->ClientReferralStaffSize = $Data->ClientReferralStaffSize;
            $this->Facility = new Facility($Data->Facility);
            $this->LinkedPersons = ApiHelper::array_of_type(null, $Data->LinkedPersons);
            $this->UserDefinedFields = ApiHelper::array_of_type('UserDefinedFields', $Data->UserDefinedFields);
            $this->UserDefinedLists = ApiHelper::array_of_type('UserDefinedLists', $Data->UserDefinedLists);
            $this->ClientReferralYearsInBusiness = $Data->ClientReferralYearsInBusiness;
            }
        }

    /**
     * Initialize a new firm object
     * @param string $FirmID
     * @return object
     */
    private function init(string $FirmID): object
        {
            
        $Result = new LocalFirm($FirmID);

        if (isset($Result->FirmJSON))
            {
            $Result = json_decode($Result->FirmJSON);
            }
        else
            {
            $Connection = new Connection('GET', 'Firm', '?firm=' . $FirmID);
            $Result = $Connection->Response;
            }

        return $Result;

        }

    /**
     * Retrieves a firm from the API.
     * @param string|FirmKey|FirmChange|object $FirmID
     * @return Firm
     */
    public function get(string $FirmID): Firm
        {
        $Result = $this->init($FirmID);
        return new Firm($Result);
        }

    /**
     * Retrieves all active Firm Codes from AM.Net
     * @return array
     */
    public function get_firms() : array
        {
        $Connection = new Connection('GET', 'Firm', '?all=true');

        $Results = array();
        if (!is_wp_error($Connection))
            {
            $Results = $Connection->Response;
            }

        $Results = str_replace(' ', '', array_column($Results, 'FirmCode'));

        if (($key = array_search('#####', $Results)) !== false)
            {
            unset($Results[$key]);
            }

        return $Results;
        }

    /**
     * Returns an array of Firm Codes that were changes after a specific date.
     * @param DateTime $DateTime
     * @return array
     */
    public function get_firms_changed_since(DateTime $DateTime) : array
        {
            $Connection = new Connection('GET', 'FirmChanges', '?since=' . $DateTime->format('m-d-Y H:i:s'));
            $Results = array();
            if (!is_wp_error($Connection))
            {
                $Results = array_column($Connection->Response, 'Firm');
                $Results = str_replace(' ', '', array_column($Results, 'FirmCode'));

                if (($key = array_search('#####', $Results)) !== false)
                    {
                    unset($Results[$key]);
                    }

            }

        return $Results;
        }

    /**
     * Returns an array of Firm Codes from AM.Net
     * @param DateTime|null $DateTime null return all populated only returns firms with changes since the date
     * @return array of Firm Codes
     */
    public function get_list(?DateTime $DateTime = null) : array
        {

        $Results = (empty($DateTime)) ? $this->get_firms() : $this->get_firms_changed_since($DateTime);

        return $Results;
        }

    /**
     * Called by cron to sync firm changes
     * @return void
     */
    public function schedule_updates(): void
        {

        // When did we last sync
        $TimeZone = new DateTimeZone(wp_timezone_string());

        $LastSync = (!empty(get_option('cdsSyncedFirms'))) ? new DateTime(get_option('cdsSyncedFirms'), $TimeZone) : null;

        // Get list of firms
        $Keys = $this->get_list($LastSync);

        $Cron = new Cron();

        // If there are lists to process and we are outside business hours run process.
        if (!empty($Keys))
            {
            // Schedule the cron job
            $Cron->create_cron_batch($Keys, 'CDS_Populate_Firm');
            // Update the last synced datetime.
            update_option('cdsSyncedFirms', current_time('Y-m-d\TH:i'));
            }
        }

    /**
     * Imports a firm into storage.
     * @param string $FirmCode
     * @return void
     */
    public function import(string $FirmCode): void
        {

        $Connection = new Connection('GET', 'Firm', '?firm=' . $FirmCode);
        $Firm = new Firm($Connection->Response);

        $LocalFirm = new LocalFirm();

        foreach ($Firm as $key => $val)
            {
            if (property_exists($LocalFirm, $key))
                {
                $LocalFirm->$key = trim($val);
                }
            }

        // Add additional property values
        $LocalFirm->ID = $Firm->FirmCode;
        $LocalFirm->FirmJSON = json_encode($Firm);

        global $wpdb;

        // The SQL query
        $Query = $wpdb->prepare("SELECT ID FROM cds_firms WHERE ID =  %s", array(trim($LocalFirm->ID)));
        $Results = $wpdb->query($Query);

        // Check to see if the SQL returned an error
        if (!$wpdb->last_error)
            {
            // Check for insert or update
            if ($Results > 0)
                {
                $wpdb->update('cds_firms', (array) $LocalFirm, array('ID' => trim($LocalFirm->ID)), '%s','%s');
                }
            else
                {
                $wpdb->insert('cds_firms', (array) $LocalFirm, '%s');
                }
            }

        if ($wpdb->last_error)
            {
            $Data = [];
            $Data = ['Query:' => $Query];
            $Data = ['Error:' => $wpdb->last_error];
            $Result = new WP_Error('SQL', __METHOD__, $Data);
            new Logging($Result);
            }
        }
    }