<?php

namespace cds\api;

use cds\api\storage\LocalList;
use cds\helpers\ApiHelper;
use cds\options\Cron;

defined('ABSPATH') or exit;

class Lists
    {

    public ?string $ListType;
    public ?string $ListUsage;
    /** @var ListItem[]|null */
    public ?array $ListItems;

    /**
     * @param string|object|null $Data
     */
    public function __construct($Data = null)
        {
        if (is_string($Data))
            {
            $Data = $this->init($Data);
            }
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ListType = $Data->ListType ?? null;
            $this->ListUsage = $Data->ListUsage ?? null;
            $Helper = new ApiHelper();
            $this->ListItems = $Helper->array_of_type('ListItem', $Data->ListItems);
            }
        }

    /**
     * Get a list from ListKey Name
     * @param string $ListKey
     * @return object
     */
    private function init(string $ListType): object
        {

        $Result = new LocalList($ListType);

        if (isset($Result->ListJSON))
            {
            $Result = json_decode($Result->ListJSON);
            }
        else
            {
            $Connection = new Connection('GET', 'lists', '/multiple?listKeys=' . $ListType);
            $Result = $Connection->Response[0];
            }

        return $Result;

        }

    /**
     * Returns a list with items from the API using the list name. (EX: 'NAME')
     * @param string $ListKey ListKey as name (EX: 'NATY')
     * @return Lists Returns a List Object
     */
    public function get(?string $ListType): Lists
        {
        return new Lists($this->init($ListType));
        }

    /**
     * Returns an array of all ListKeyDtos from the API, maps to the AM.Net Sycod table.
     * @return array Returns an array of ListKeys.
     */
    public function get_lists()
        {
        $Connection = new Connection('GET', 'Lists');
        return array_unique(array_column($Connection->Response, 'ListKey'));
        }

    /**
     * Retrieves the items for a specific list using the list name.
     * @param string $ListKey (EX: 'NATY')
     * @return array
     */
    public function get_items(string $ListType): array
        {
        $Connection = new Connection('Lists', '?listKey=' . $ListType);
        $Items = array();
        if (!is_wp_error($Connection->Response))
            {
            foreach ($Connection->Response as $ListType)
                {
                array_push($Items, new ListItem($ListType));
                }
            }
        return $Items;
        }

    /**
     * Get the description of a specific list item
     * @param string $ListType
     * @param string $Code
     * @return bool|int|string
     */
    public function get_item_description(string $ListType, string $Code)
        {
            $List = new Lists($ListType);
            return array_search($Code, array_column((array) $List->ListItems, 'Code', 'Description'));
        }

    /**
     * Get an array of list types to be imported.
     * @return void
     */
    public function schedule_updates()
        {
        // Cat the object
        $Lists = new Lists();

        // Returns an array if successful
        $Lists = $Lists->get_lists();

        // Fire-up cron
        $Cron = new Cron();

        // If there are lists to process and we are outside business hours run process.
        if ($Lists)
            {
            // Schedule the cron job
            $Cron->create_cron_batch($Lists, 'CDS_Populate_List');
            // Update the last synced datetime.
            update_option('cdsSyncedLists', current_time('Y-m-d\TH:i'));
            }
        }

    /**
     * Called by cron this imports a list.
     * @param string $ListCode
     * @return void
     */
    public function import(string $ListType): void
        {

        // Force Connection to API for import.
        $Connection = new Connection('GET', 'lists', '/multiple?listKeys=' . $ListType);
        $Result = $Connection->Response[0];

        $LocalList = new LocalList();

        $LocalList->ListType = $Result->ListType;
        $LocalList->ListJSON = json_encode($Result);

        global $wpdb;

        $Query = $wpdb->prepare("SELECT ListType FROM cds_lists WHERE ListType = %s", array('ListType' => $LocalList->ListType));
        $Results = $wpdb->query($Query);

        if (!$wpdb->last_error)
            {
            if ($Results > 0)
                {
                $wpdb->update('cds_lists', (array) $LocalList, array('ListType' => $LocalList->ListType), '%s','%s');
                }
            else
                {
                $wpdb->insert('cds_lists', (array) $LocalList, '%s');
                }
            }
        }


    /**
     * Creates HTML for a select input field using the named list data.
     *
     * @param string            $ListKey The list key name for example NATY.
     * @param string|null       $Filter Allows the list to be filtered on the Misc1 key.
     * @param string|null       $ID Sets the id and name of the selector.
     * @param string|null       $Class Sets the CSS class for the selector.
     * @param string|null       $AddOption Adds a option for example "Select One"
     * @param string|null       $Value Used to set the selected value. Will check on both code and description.
     * @return string           HTML needed to create the select box.
     */
    public static function create_input_select_field(string $ListKey, string $Filter = null, string $ID = null, string $Class = null, string $AddOption = null, string $Value = null, bool $Required = false)
        {

        $Identifier = !empty($ID) ? 'id="' . $ID . '"' : null;
        $Name = !empty($ID) ? 'name="' . $ID . '"' : null;
        $Class = !empty($Class) ? 'class="' . $Class . '"' : null;
        $Required = $Required ? 'required' : null;

        // Start building the control
        $Input = '<select ' . $Identifier . ' ' . $Name . ' ' . $Class . ' ' . $Required . '>';

        // Add a blank option is needed
        if (!empty($AddOption))
            {
            $Input .= '<option value="">' . $AddOption . '</option>';
            }

        $LocalList = new LocalList($ListKey);
        if (isset($LocalList->ListType))
            {
            $List = new Lists(json_decode($LocalList->ListJSON));
            }
        else
            {
            $List = new Lists($ListKey);
            }

        $Items = (array) $List->ListItems;

        if (!empty($Filter))
            {
            $Items = array_filter($Items,
                        function ($Key, $Value, $Filter)
                            {
                            return $Key = 'Misc1' && $Value == $Filter;
                            },
                        ARRAY_FILTER_USE_BOTH
                    );
            }

        // Loop through the items
        foreach ($Items as $item)
            {

            // Check for and set the selected option
            if (strtolower($item->Code) === strtolower($Value) || strtolower($item->Description) === strtolower($Value))
                {
                $Input .= '<option value="' . $item->Code . '" selected>' . $item->Description . '</option>';
                }
            else
                {
                $Input .= '<option value="' . $item->Code . '">' . $item->Description . '</option>';
                }
            }

        // Close the control
        $Input .= '</select>';

        // Return the control code
        return $Input;
        }
    }