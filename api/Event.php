<?php

namespace cds\api;

use cds\api\storage\LocalEvent;
use cds\helpers\ApiHelper;
use cds\options\Cron;
use DateTime;
use DateTimeZone;
use MongoDB\BSON\Timestamp;
use WP_Error;

defined('ABSPATH') or exit;

class Event
	{
	public ?string $TypeCode;
	public ?string $Year;
	public ?string $StatusCode;
	public ?string $Code;
	public ?string $Code2;
	public ?string $EventName;
	public ?string $FieldsOfStudyCodes;
	public ?string $FieldsOfInterestCodes;
	public ?string $BeginDate;
	public ?string $EndDate;
	public ?string $AccountingCloseDate;
	public ?string $OpenTime;
	public ?string $BeginTimeDay1;
	public ?string $BeginTimeDay2;
	public ?string $BeginTimeDay3;
	public ?string $MorningBreakTime;
	public ?string $LunchTime;
	public ?string $AfternoonBreakTime;
	public ?string $EndTimeDay1;
	public ?string $EndTimeDay2;
	public ?string $EndTimeDay3;
	public ?int $MinimumRegistrations;
	public ?int $MaximumRegistrations;
	public ?int $CurrentRegistrations;
	public ?int $BudgetedRegistrations;
	public ?int $WaitListLength;
	public ?string $LastRegistration;
	public ?string $PrePrepCode;
	public ?int $CreditHours;
	public ?string $FacilityLink;
	public ?int $BudgetedExpenses;
	public ?int $ActualExpenses;
	public ?int $ManualPrice;
	public ?int $ManualsOrdered;
	public ?int $ManualsReceived;
	public ?int $ManualsReturned;
	public ?string $ManualsReturnedDate;
	public ?int $ManualsSentOut;
	public ?int $ManualsSold;
	public ?int $ManualsLost;
	public ?int $ManualsDistributed;
	public ?int $ManualsSentToSite;
	public ?int $ManualsReturnedFromSite;
	public ?int $RatingMaterials;
	public ?int $RatingFacility;
	public ?int $RatingRelevance;
	public ?int $RatingObjectivesMet;
	public ?int $RatingPrerequisites;
	public ?int $RatingTimeAllotted;
	public ?int $RatingAudioVisual;
	public ?int $RatingCount;
	public ?string $RatingsComments;
	public ?string $FacilityComments;
	public ?string $EvaluationNewEventSuggestions;
	public ?int $RatingFood;
	public ?int $RatingOverall;
	public ?int $RatingHotel;
	public ?string $Vendor1;
	public ?string $Vendor2;
	public ?string $Vendor3;
	public ?string $PurchaseOptionCode;
	public ?string $Administrator1;
	public ?string $Administrator2;
	public ?string $Administrator3;
	public ?string $StaffContact;
	public ?string $RecordAdded;
	public ?string $RecordAddedBy;
	public ?string $RecordLastUpdated;
	public ?string $RecordLastUpdatedBy;
	public ?string $CompanyCode;
	public ?string $DivisionCode;
	public ?string $GLAccount;
	public ?string $GLCompany;
	public ?bool $ConfidentialConfirmation;
	public ?string $ConfirmationNote;
	public ?bool $SendConfirmation;
	public ?string $LevelCode;
	public ?string $Description;
	public ?string $LateFeeDate;
	public ?string $Title;
	/**
	 * @var Credits[]|null
	 */
	public ?array $Credits;
	public ?bool $UniqueEvent;
	public ?string $CancellationDate;
	public ?string $MeetingRoom;
	public ?string $LunchRoom;
	public ?string $RegistrationDesk;
	public ?string $RoomSetup1;
	public ?string $RoomSetup2;
	public ?string $RoomSetup3;
	public ?string $HeadTable;
	public ?string $AVSetup1;
	public ?string $AVSetup2;
	public ?string $AVSetup3;
	public ?string $AVSetup4;
	public ?string $AVSetup5;
	public ?string $AVSetup6;
	public ?string $FacilityNote;
	public ?string $Schedule;
	/**
	 * @var RelatedEvents[]|null
	 */
	public ?array $RelatedEvents;
	/**
	 * @var RelatedProducts[]|null
	 */
	public ?array $RelatedProducts;
	public ?string $LeaderNote;
	public ?string $RegistrationCateringNote;
	public ?string $MorningBreakCateringNote;
	public ?string $LunchCateringNote;
	public ?string $AfternoonBreakCateringNote;
	public ?string $RegistrationCutoff;
	public ?bool $InHouseManual;
	public ?string $MaterialsSentToAdministrator;
	public ?string $MaterialsSentToAdministratorBy;
	public ?string $PacketSentToAdministrator;
	public ?string $PacketSentToAdministratorBy;
	public ?string $PacketReturned;
	public ?string $PacketReturnedBy;
	public ?string $LastPrePrep;
	public ?int $PassportDiscountMembers;
	public ?int $PassportDiscountNonmembers;
	public ?string $CurriculumCode;
	public ?string $MealGuaranteedBy;
	public ?string $MealGuaranteeContact;
	public ?string $MealGuaranteeDate;
	public ?int $MealGuaranteesRequested;
	public ?bool $ExcludeFromInternalCatalog;
	public ?string $ProductCode;
	public ?string $ProductOrderNumber;
	public ?string $ReconcileDate;
	public ?string $CreditNote;
	public ?string $CourseSaleAuthorNamesId;
	public ?string $CourseSaleShipTo;
	public ?string $FacilityUnlinkedAddress;
	public ?string $CourseSaleNamesId;
	public ?string $CourseSaleFirmCode;
	public ?bool $CourseSalePrintMasterManual;
	public ?string $FacilityLocationFirmCode;
	public ?int $MatrixRequired;
	public ?string $MatrixTopicTo;
	public ?int $MatrixMaximum;
	public ?int $PassportMaximumRegistrants;
	public ?int $PassportRegistrations;
	public ?string $GeneralSessionCode;
	public ?string $GeneralSessionDescription;
	public ?string $GeneralSessionDay;
	public ?string $GeneralSessionTime;
	public ?Credits $GeneralSessionCredits;
	public ?string $CodeYear;
	public ?bool $CalculateSessionCredits;
	public ?string $MarketingDescription;
	public ?string $DesignedFor;
	public ?string $Objectives;
	public ?string $MajorSubjects;
	public ?string $Prerequisites;
	public ?string $AdvancedPreparation;
	public ?string $ChapterCode;
	public ?bool $ExcludeFromWebsite;
	public ?bool $NewEvent;
	public ?string $NationalAcronym;
	public ?string $RoomSetupCount;
	public ?string $Flag;
	public ?string $FlagUpdated;
	public ?string $FlagUpdatedBy;
	public ?string $MarketingKeywords;
	public ?string $MarketingTopicCodes;
	public ?string $MarketingMisc;
	public ?string $MarketingTestimonials;
	public ?string $ConfirmationMemo;
	public ?bool $YellowBook;
	public ?bool $Cfp;
	public ?bool $AttestAndComp;
	/**
	 * @var PersonKey[]|null
	 */
	public ?array $Authors;
	public ?string $WebLink;
	public ?string $WebURL;
	public ?string $SurveyUrl1;
	public ?string $SurveyUrl2;
	public ?string $SurveyUrl3;
	public ?string $FormatCodes;
	public ?bool $CpeBankEligible;
	public ?string $ExternalEventCode1;
	public ?string $ExternalEventCode2;
	public ?bool $Cle;
	public ?bool $EnrolledAgent;
	public ?bool $NASBACertified;
	/**
	 * @var Fees[]|null
	 */
	public ?array $Fees;
	/**
	 * @var Sessions[]|null
	 */
	public ?array $Sessions;
	/**
	 * @var Leaders[]|null
	 */
	public ?array $Leaders;
	/**
	 * @var FirmKey[]|null
	 */
	public ?array $Exhibitors;
	/**
	 * @var UserDefinedFields[]|null
	 */
	public ?array $UserDefinedFields;
	/**
	 * @var UserDefinedLists[]|null
	 */
	public ?array $UserDefinedLists;
	/**
	 * @var BundleItems[]|null
	 */
	public ?array $BundleItems;
	/**
	 * @var TaxRules[]|null
	 */
	public ?array $TaxRules;
	public ?string $TaxRuleCode;
	public ?bool $MemberOnly;

	/**
	 * Initialize an event object
	 * @param mixed $Data
	 */
	public function __construct($Data = null)
		{
		if (is_string($Data))
			{
			$Data = $this->init($Data);
			}
		if (is_object($Data) && !is_wp_error($Data))
			{
			$this->TypeCode = $Data->TypeCode;
			$this->Year = $Data->Year;
			$this->StatusCode = $Data->StatusCode;
			$this->Code = $Data->Code;
			$this->Code2 = $Data->Code2;
			$this->EventName = $Data->EventName;
			$this->FieldsOfStudyCodes = $Data->FieldsOfStudyCodes;
			$this->FieldsOfInterestCodes = $Data->FieldsOfInterestCodes;
			$this->BeginDate = $Data->BeginDate;
			$this->EndDate = $Data->EndDate;
			$this->AccountingCloseDate = $Data->AccountingCloseDate;
			$this->OpenTime = $Data->OpenTime;
			$this->BeginTimeDay1 = $Data->BeginTimeDay1;
			$this->BeginTimeDay2 = $Data->BeginTimeDay2;
			$this->BeginTimeDay3 = $Data->BeginTimeDay3;
			$this->MorningBreakTime = $Data->MorningBreakTime;
			$this->LunchTime = $Data->LunchTime;
			$this->AfternoonBreakTime = $Data->AfternoonBreakTime;
			$this->EndTimeDay1 = $Data->EndTimeDay1;
			$this->EndTimeDay2 = $Data->EndTimeDay2;
			$this->EndTimeDay3 = $Data->EndTimeDay3;
			$this->MinimumRegistrations = $Data->MinimumRegistrations;
			$this->MaximumRegistrations = $Data->MaximumRegistrations;
			$this->CurrentRegistrations = $Data->CurrentRegistrations;
			$this->BudgetedRegistrations = $Data->BudgetedRegistrations;
			$this->WaitListLength = $Data->WaitListLength;
			$this->LastRegistration = $Data->LastRegistration;
			$this->PrePrepCode = $Data->PrePrepCode;
			$this->CreditHours = $Data->CreditHours;
			$this->FacilityLink = $Data->FacilityLink;
			$this->BudgetedExpenses = $Data->BudgetedExpenses;
			$this->ActualExpenses = $Data->ActualExpenses;
			$this->ManualPrice = $Data->ManualPrice;
			$this->ManualsOrdered = $Data->ManualsOrdered;
			$this->ManualsReceived = $Data->ManualsReceived;
			$this->ManualsReturned = $Data->ManualsReturned;
			$this->ManualsReturnedDate = $Data->ManualsReturnedDate;
			$this->ManualsSentOut = $Data->ManualsSentOut;
			$this->ManualsSold = $Data->ManualsSold;
			$this->ManualsLost = $Data->ManualsLost;
			$this->ManualsDistributed = $Data->ManualsDistributed;
			$this->ManualsSentToSite = $Data->ManualsSentToSite;
			$this->ManualsReturnedFromSite = $Data->ManualsReturnedFromSite;
			$this->RatingMaterials = $Data->RatingMaterials;
			$this->RatingFacility = $Data->RatingFacility;
			$this->RatingRelevance = $Data->RatingRelevance;
			$this->RatingObjectivesMet = $Data->RatingObjectivesMet ?? $Data->RatingObectivesMet; // PHIL Fixing spelling
			$this->RatingPrerequisites = $Data->RatingPrerequisites;
			$this->RatingTimeAllotted = $Data->RatingTimeAllotted;
			$this->RatingAudioVisual = $Data->RatingAudioVisual;
			$this->RatingCount = $Data->RatingCount;
			$this->RatingsComments = $Data->RatingsComments;
			$this->FacilityComments = $Data->FacilityComments;
			$this->EvaluationNewEventSuggestions = $Data->EvaluationNewEventSuggestions;
			$this->RatingFood = $Data->RatingFood;
			$this->RatingOverall = $Data->RatingOverall;
			$this->RatingHotel = $Data->RatingHotel;
			$this->Vendor1 = $Data->Vendor1;
			$this->Vendor2 = $Data->Vendor2;
			$this->Vendor3 = $Data->Vendor3;
			$this->PurchaseOptionCode = $Data->PurchaseOptionCode;
			$this->Administrator1 = $Data->Administrator1;
			$this->Administrator2 = $Data->Administrator2;
			$this->Administrator3 = $Data->Administrator3;
			$this->StaffContact = $Data->StaffContact;
			$this->RecordAdded = $Data->RecordAdded;
			$this->RecordAddedBy = $Data->RecordAddedBy;
			$this->RecordLastUpdated = $Data->RecordLastUpdated;
			$this->RecordLastUpdatedBy = $Data->RecordLastUpdatedBy;
			$this->CompanyCode = $Data->CompanyCode;
			$this->DivisionCode = $Data->DivisionCode;
			$this->GLAccount = $Data->GLAccount;
			$this->GLCompany = $Data->GLCompany;
			$this->ConfidentialConfirmation = $Data->ConfidentialConfirmation;
			$this->ConfirmationNote = $Data->ConfirmationNote;
			$this->SendConfirmation = $Data->SendConfirmation;
			$this->LevelCode = $Data->LevelCode;
			$this->Description = $Data->Description;
			$this->LateFeeDate = $Data->LateFeeDate;
			$this->Title = $Data->Title;
			$this->Credits = ApiHelper::array_of_type('Credits', $Data->Credits);
			$this->UniqueEvent = $Data->UniqueEvent;
			$this->CancellationDate = $Data->CancellationDate;
			$this->MeetingRoom = $Data->MeetingRoom;
			$this->LunchRoom = $Data->LunchRoom;
			$this->RegistrationDesk = $Data->RegistrationDesk;
			$this->RoomSetup1 = $Data->RoomSetup1;
			$this->RoomSetup2 = $Data->RoomSetup2;
			$this->RoomSetup3 = $Data->RoomSetup3;
			$this->HeadTable = $Data->HeadTable;
			$this->AVSetup1 = $Data->AVSetup1;
			$this->AVSetup2 = $Data->AVSetup2;
			$this->AVSetup3 = $Data->AVSetup3;
			$this->AVSetup4 = $Data->AVSetup4;
			$this->AVSetup5 = $Data->AVSetup5;
			$this->AVSetup6 = $Data->AVSetup6;
			$this->FacilityNote = $Data->FacilityNote;
			$this->Schedule = $Data->Schedule;
			$this->RelatedEvents = ApiHelper::array_of_type('RelatedEvents', $Data->RelatedEvents);
			$this->RelatedProducts = ApiHelper::array_of_type('RelatedProducts', $Data->RelatedProducts);
			$this->LeaderNote = $Data->LeaderNote;
			$this->RegistrationCateringNote = $Data->RegistrationCateringNote;
			$this->MorningBreakCateringNote = $Data->MorningBreakCateringNote;
			$this->LunchCateringNote = $Data->LunchCateringNote;
			$this->AfternoonBreakCateringNote = $Data->AfternoonBreakCateringNote;
			$this->RegistrationCutoff = $Data->RegistrationCutoff;
			$this->InHouseManual = $Data->InHouseManual;
			$this->MaterialsSentToAdministrator = $Data->MaterialsSentToAdministrator;
			$this->MaterialsSentToAdministratorBy = $Data->MaterialsSentToAdministratorBy;
			$this->PacketSentToAdministrator = $Data->PacketSentToAdministrator;
			$this->PacketSentToAdministratorBy = $Data->PacketSentToAdministratorBy;
			$this->PacketReturned = $Data->PacketReturned;
			$this->PacketReturnedBy = $Data->PacketReturnedBy;
			$this->LastPrePrep = $Data->LastPrePrep;
			$this->PassportDiscountMembers = $Data->PassportDiscountMembers;
			$this->PassportDiscountNonmembers = $Data->PassportDiscountNonmembers;
			$this->CurriculumCode = $Data->CurriculumCode;
			$this->MealGuaranteedBy = $Data->MealGuaranteedBy;
			$this->MealGuaranteeContact = $Data->MealGuaranteeContact;
			$this->MealGuaranteeDate = $Data->MealGuaranteeDate;
			$this->MealGuaranteesRequested = $Data->MealGuaranteesRequested;
			$this->ExcludeFromInternalCatalog = $Data->ExcludeFromInternalCatalog;
			$this->ProductCode = $Data->ProductCode;
			$this->ProductOrderNumber = $Data->ProductOrderNumber;
			$this->ReconcileDate = $Data->ReconcileDate;
			$this->CreditNote = $Data->CreditNote;
			$this->CourseSaleAuthorNamesId = $Data->CourseSaleAuthorNamesId;
			$this->CourseSaleShipTo = $Data->CourseSaleShipTo;
			$this->FacilityUnlinkedAddress = $Data->FacilityUnlinkedAddress;
			$this->CourseSaleNamesId = $Data->CourseSaleNamesId;
			$this->CourseSaleFirmCode = $Data->CourseSaleFirmCode;
			$this->CourseSalePrintMasterManual = $Data->CourseSalePrintMasterManual;
			$this->FacilityLocationFirmCode = $Data->FacilityLocationFirmCode;
			$this->MatrixRequired = $Data->MatrixRequired;
			$this->MatrixTopicTo = $Data->MatrixTopicTo;
			$this->MatrixMaximum = $Data->MatrixMaximum;
			$this->PassportMaximumRegistrants = $Data->PassportMaximumRegistrants ?? $Data->PassportMaxiumumRegistrants; // PHIL Fixing spelling
			$this->PassportRegistrations = $Data->PassportRegistrations;
			$this->GeneralSessionCode = $Data->GeneralSessionCode;
			$this->GeneralSessionDescription = $Data->GeneralSessionDescription;
			$this->GeneralSessionDay = $Data->GeneralSessionDay;
			$this->GeneralSessionTime = $Data->GeneralSessionTime;
			$this->GeneralSessionCredits = $Data->GeneralSessionCredits;
			$this->CodeYear = $Data->CodeYear;
			$this->CalculateSessionCredits = $Data->CalculateSessionCredits;
			$this->MarketingDescription = $Data->MarketingDescription;
			$this->DesignedFor = $Data->DesignedFor;
			$this->Objectives = $Data->Objectives;
			$this->MajorSubjects = $Data->MajorSubjects;
			$this->Prerequisites = $Data->Prerequisites ?? $Data->Prerequisties; // PHIL Fixing spelling
			$this->AdvancedPreparation = $Data->AdvancedPreparation;
			$this->ChapterCode = $Data->ChapterCode;
			$this->ExcludeFromWebsite = $Data->ExcludeFromWebsite;
			$this->NewEvent = $Data->NewEvent;
			$this->NationalAcronym = $Data->NationalAcronym;
			$this->RoomSetupCount = $Data->RoomSetupCount;
			$this->Flag = $Data->Flag;
			$this->FlagUpdated = $Data->FlagUpdated;
			$this->FlagUpdatedBy = $Data->FlagUpdatedBy;
			$this->MarketingKeywords = $Data->MarketingKeywords;
			$this->MarketingTopicCodes = $Data->MarketingTopicCodes;
			$this->MarketingMisc = $Data->MarketingMisc;
			$this->MarketingTestimonials = $Data->MarketingTestimonials;
			$this->ConfirmationMemo = $Data->ConfirmationMemo;
			$this->YellowBook = $Data->YellowBook;
			$this->Cfp = $Data->Cfp;
			$this->AttestAndComp = $Data->AttestAndComp;
			$this->Authors = ApiHelper::array_of_type('PersonKey', $Data->Authors);
			$this->WebLink = $Data->WebLink;
			$this->WebURL = $Data->WebURL;
			$this->SurveyUrl1 = $Data->SurveyUrl1;
			$this->SurveyUrl2 = $Data->SurveyUrl2;
			$this->SurveyUrl3 = $Data->SurveyUrl3;
			$this->FormatCodes = $Data->FormatCodes;
			$this->CpeBankEligible = $Data->CpeBankEligible;
			$this->ExternalEventCode1 = $Data->ExternalEventCode1;
			$this->ExternalEventCode2 = $Data->ExternalEventCode2;
			$this->Cle = $Data->Cle;
			$this->EnrolledAgent = $Data->EnrolledAgent;
			$this->NASBACertified = $Data->NASBACertified;
			$this->Fees = ApiHelper::array_of_type('Fees', $Data->Fees);
			$this->Sessions = ApiHelper::array_of_type('Sessions', $Data->Sessions);
			$this->Leaders = ApiHelper::array_of_type('Leaders', $Data->Leaders);
			$this->Exhibitors = ApiHelper::array_of_type('FirmKey', $Data->Exhibitors);
			$this->UserDefinedFields = ApiHelper::array_of_type('UserDefinedFields', $Data->UserDefinedFields);
			$this->UserDefinedLists = ApiHelper::array_of_type('UserDefinedLists', $Data->UserDefinedLists);
			$this->BundleItems = ApiHelper::array_of_type('BundleItems', $Data->BundleItems);
			$this->TaxRules = ApiHelper::array_of_type('TaxRules', $Data->TaxRules);
			$this->TaxRuleCode = $Data->TaxRuleCode;
			$this->MemberOnly = $Data->MemberOnly;
			}
		}

	private function init(string $CodeYr)
		{
		// Check storage first
		$Result = new LocalEvent(str_replace(' ', '', $CodeYr));

		// If we have a clean local copy we need to use it
		if (isset($Result->EventJSON) && $Result->IsDirty === 0)
			{
			$Result = json_decode($Result->EventJSON);
			}
		else
			{
			$Connection = new Connection('GET', 'Event', '?CodeYr=' . $CodeYr);
			$Result = $Connection->Response;
			}

		return $Result;
		}

	/**
	 * Retrieves tan event by code year
	 * @param string 		$CodeYr
	 * @return Event
	 */
	public function get(string $CodeYr): Event
		{
		return new Event($CodeYr);
		}


	/**
	 * Returns an array of code year values for events that were modified after the given datetime.
	 * @param DateTime 		$DateTime
	 * @param bool 			$IncludeFees default to true
	 * @param bool 			$IncludeSessions defaults to true
	 * @return array
	 */
	public function get_changed_since(DateTime $DateTime, bool $IncludeFees = true, bool $IncludeSessions = true): array
		{
		$Connection = new Connection('GET', 'Event', '?since=' . $DateTime->format('m-d-Y H:i:s') . '&includeFees=' . var_export($IncludeFees, true) . '&includeSessions=' . var_export($IncludeSessions, true));

		$Results = array();
		if (!is_wp_error($Connection))
			{
			$Results = array_column($Connection->Response, 'EventCodeYear');
			}

		return str_replace(' ', '', $Results);

		}

	/**
	 * Returns an array of code year values for events occurring after the datetime supplied.
	 * @param DateTime $DateTime
	 * @return array
	 */
	public function get_from(DateTime $DateTime): array
		{
		$Connection = new Connection('GET', 'Event', '?fromdt=' . $DateTime->format('m-d-Y H:i:s'));

		$Results = array();
		if (!is_wp_error($Connection))
			{
			$Results = array_column($Connection->Response, 'EventCodeYear');
			}

		return str_replace(' ', '', $Results);

		}

	/**
	 * Retrieves a list of FirmKeys that have changed since the supplied DateTime. If DateTime is not supplied return all.
	 * @param DateTime|null $DateTime
	 * @return array|mixed
	 */
	public function get_list(?DateTime $DateTime = null)
		{

		if (empty($DateTime))
			{
			// Getting all future events
			$EventList = $this->get_from(new DateTime('now', wp_timezone()));
			}
		else
			{
			// Getting events in the future with changes
			$EventList = $this->get_changed_since($DateTime);
			}

		return $EventList;

		}

	/**
	 * Imports a event into storage.
	 * @param string $CodeYear
	 * @return void
	 */
	public function import(string $CodeYear,Event $Event = null): void
		{
		if (empty($Event->CodeYear))
			{
				$Event = $this->get($CodeYear);
			}

		$x = $Event->MarketingDescription;

		$LocalEvent = new LocalEvent();

		// If we expand the properties of storage this should catch the data
		foreach ($Event as $key => $val)
			{
			if (property_exists($LocalEvent, $key))
				{
				$LocalEvent->$key = trim($val);
				}
			}

		// Known properties in storage that must be populated
		$LocalEvent->ID = str_replace(' ', '', $Event->CodeYear);
		$LocalEvent->EventJSON = json_encode($Event);
		$LocalEvent->IsDirty = true;

		global $wpdb;

		// The SQL query
		$Query = $wpdb->prepare("SELECT ID FROM cds_events WHERE ID =  %s", array($LocalEvent->ID));
		$Results = $wpdb->query($Query);

		// Check to see if the SQL returned an error
		if (!$wpdb->last_error)
			{
			// Check for insert or update
			if ($Results > 0)
				{
				$wpdb->update('cds_events', (array) $LocalEvent, array('ID' => $LocalEvent->ID), '%s', '%s');
				}
			else
				{
				$wpdb->insert('cds_events', (array) $LocalEvent, '%s');
				}
			}

		if ($wpdb->last_error)
			{
			$Data = [];
			$Data = ['Query:' => $Query];
			$Data = ['Error:' => $LocalEvent->ID . '-' . $wpdb->last_error];
			$Result = new WP_Error('SQL', __METHOD__, $Data);

			new Logging($Result);

			}
		}

	/**
	 * Called by cron to sync event changes
	 * @return void
	 */
	public function schedule_updates(): void
		{

		// When did we last sync
		$TimeZone = new DateTimeZone(wp_timezone_string());

		if (!empty(get_option('cdsSyncedEvents')))
			{
			$LastSync = new DateTime(get_option('cdsSyncedEvents'), $TimeZone);
			}
		else
			{
			$LastSync = null;
			}

		// Get list of firms
		$Keys = $this->get_list($LastSync);

		$Cron = new Cron();

		// If there are lists to process and we are outside business hours run process.
		if (!empty($Keys))
			{
			// Schedule the cron job
			$Cron->create_cron_batch($Keys, 'CDS_Populate_Events');
			// Update the last synced datetime.
			update_option('cdsSyncedEvents', current_time('Y-m-d\TH:i'));
			}
		}

	}