<?php

namespace cds\api;
use cds\helpers\ApiHelper;

defined('ABSPATH') or exit;

class Sessions
    {
    public ?string $EventCode;
    public ?string $EventYear;
    public ?string $SessionCode;
    public ?string $Day;
    public ?string $SessionTime;
    public ?string $Description;
    /**
     * @var string[]|null
     */
    public ?array $ConcurrentSessions;
    public ?int $MaxRegistrations;
    public ?int $RegistrationCount;
    public ?string $RecordAdded;
    public ?string $RecordAddedBy;
    public ?string $RecordLastUpdated;
    public ?string $RecordLastUpdatedBy;
    public ?string $DoubleTimeSlot;
    /**
     * @var Credits[]|null
     */
    public ?array $Credits;
    public ?string $StatusCode;
    public ?string $SessionDate;
    public ?string $EndTime;
    public ?string $SortSequence;
    /**
     * @var string[]|null
     */
    public ?array $AVSetup;
    /**
     * @var PersonKey[]|null
     */
    public ?array $SessionAdmins;
    public ?string $MeetingRoom;
    public ?string $Notes;
    public ?string $MarketingCopy;
    public ?string $SessionTypeCode;
    public ?string $MarketingKeywords;
    /**
     * @var string[]|null
     */
    public ?array $TopicCodes;
    public ?string $TimeSlot;
    public ?string $Workshop;
    public ?string $WorkshopDay;
    /**
     * @var string[]|null
     */
    public ?array $FieldsOfStudyCodes;
    public ?int $MemberFee;
    public ?int $NonmemberFee;
    public ?bool $GuestsOnly;
    public ?int $GuestFee;
    public ?int $AdditionalGuestFee;
    public ?string $SessionTrackCode;
    public ?bool $ExcludeFromWebsite;
    public ?bool $RegistrantsOnly;
    public ?string $ExternalVendorCode1;
    public ?string $ExternalVendorCode2;
    public ?string $ControlId;
    /**
     * @var Leaders[]|null
     */
    public ?array $Leaders;
    public ?string $SessionType;
    public ?string $WebLink;

    /**
     * @param string[]|null $ConcurrentSessions
     * @param Credits[]|null $Credits
     * @param string[]|null $AVSetup
     * @param PersonKey[]|null $SessionAdmins
     * @param string[]|null $TopicCodes
     * @param string[]|null $FieldsOfStudyCodes
     * @param Leaders[]|null $Leaders
     */

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->EventCode = $Data->EventCode;
            $this->EventYear = $Data->EventYear;
            $this->SessionCode = $Data->SessionCode;
            $this->Day = $Data->Day;
            $this->SessionTime = $Data->SessionTime;
            $this->Description = $Data->Description;
            $this->ConcurrentSessions = ApiHelper::array_of_type(null,$Data->ConcurrentSesssions) ?? ApiHelper::array_of_type(null,$Data->ConcurrentSessions);
            $this->MaxRegistrations = $Data->MaxRegistrations;
            $this->RegistrationCount = $Data->RegistrationCount;
            $this->RecordAdded = $Data->RecordAdded;
            $this->RecordAddedBy = $Data->RecordAddedBy;
            $this->RecordLastUpdated = $Data->RecordLastUpdated;
            $this->RecordLastUpdatedBy = $Data->RecordLastUpdatedBy;
            $this->DoubleTimeSlot = $Data->DoubleTimeSlot ?? $Data->DoubleTimeslot;
            $this->Credits = ApiHelper::array_of_type('Credits', $Data->Credits);
            $this->StatusCode = $Data->StatusCode;
            $this->SessionDate = $Data->SessionDate;
            $this->EndTime = $Data->EndTime;
            $this->SortSequence = $Data->SortSequence;
            $this->AVSetup = ApiHelper::array_of_type(null,$Data->AVSetup);
            $this->SessionAdmins = ApiHelper::array_of_type('PersonKey',$Data->SessionAdmins);
            $this->MeetingRoom = $Data->MeetingRoom;
            $this->Notes = $Data->Notes;
            $this->MarketingCopy = $Data->MarketingCopy;
            $this->SessionTypeCode = $Data->SessionTypeCode;
            $this->MarketingKeywords = $Data->MarketingKeywords;
            $this->TopicCodes = ApiHelper::array_of_type(null,$Data->TopicCodes);
            $this->TimeSlot = $Data->TimeSlot ?? $Data->Timeslot;
            $this->Workshop = $Data->Workshop;
            $this->WorkshopDay = $Data->WorkshopDay;
            $this->FieldsOfStudyCodes = ApiHelper::array_of_type(null,$Data->FieldsOfStudyCodes);
            $this->MemberFee = $Data->MemberFee;
            $this->NonmemberFee = $Data->NonmemberFee;
            $this->GuestsOnly = $Data->GuestsOnly;
            $this->GuestFee = $Data->GuestFee;
            $this->AdditionalGuestFee = $Data->AdditionalGuestFee;
            $this->SessionTrackCode = $Data->SessionTrackCode;
            $this->ExcludeFromWebsite = $Data->ExcludeFromWebsite;
            $this->RegistrantsOnly = $Data->RegistrantsOnly;
            $this->ExternalVendorCode1 = $Data->ExternalVendorCode1;
            $this->ExternalVendorCode2 = $Data->ExternalVendorCode2;
            $this->ControlId = $Data->ControlId;
            $this->Leaders = ApiHelper::array_of_type('Leaders',$Data->Leaders);
            $this->SessionType = $Data->SessionType;
            $this->WebLink = $Data->WebLink;
            }
        }
    }