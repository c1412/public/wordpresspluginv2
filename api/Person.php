<?php

namespace cds\api;

use cds\api\storage\LocalPerson;
use cds\helpers\ApiHelper;
use cds\options\Cron;
use DateTime;
use DateTimeZone;
use WP_Error;
use WP_User;

defined('ABSPATH') or exit;

class Person
    {
    public ?string $ID;
    public ?string $FirstName;
    public ?string $LastName;
    public ?string $MiddleInitial;
    public ?string $Suffix;
    public ?string $SalutationFirst;
    public ?string $PriorName;
    public ?string $GenderCode;
    public ?string $PronounCode;
    public ?string $MemberTypeCode;
    public ?string $MemberTypeDescription;
    public ?string $MemberStatusDescription;
    public ?string $ProspectiveMemberSourceCodes;
    public ?string $FirmName1;
    public ?string $FirmName2;
    public ?string $HomeAddressLine1;
    public ?string $HomeAddressLine2;
    public ?string $HomeAddressCity;
    public ?string $HomeAddressStateCode;
    public ?string $HomeAddressStreetZip;
    public ?string $HomeAddressPobZip;
    public ?string $HomeAddressForeignCountry;
    public ?string $FirmAddressLine1;
    public ?string $FirmAddressLine2;
    public ?string $FirmAddressCity;
    public ?string $FirmAddressStateCode;
    public ?string $FirmAddressStreetZip;
    public ?string $FirmAddressPobZip;
    public ?string $FirmAddressForeignCountry;
    public ?string $Email;
    public ?string $InStateCertificateNumber;
    public ?string $InStateCertificationDate;
    public ?string $OutOfStateCertificationDate;
    public ?string $OutOfStateCertificationStateCode;
    public ?string $OutOfStateCertificateNumber;
    public ?string $LinkedFirmCode;
    public ?string $SecondaryLinkedFirmCode;
    public ?bool $IsFirmAdmin;
    public ?string $GeneralMailPreferenceCode;
    public ?string $CPEMailPreferenceCode;
    public ?string $PositionDescription;
    public ?string $PositionCode;
    public ?string $HomePhone;
    public ?string $DirectPhone;
    public ?string $DirectFax;
    public ?string $NickName;
    public ?string $Credentials;
    public ?string $CertifiedCode;
    public ?string $Title;
    public ?string $BirthDate;
    public ?string $PreferredChapterCode;
    public ?string $ActualChapterCode;
    public ?string $Spouse;
    public ?string $MobilePhone;
    public ?string $ConfirmRegistrationMethodCode;
    public ?string $MemberStatusCode;
    public ?string $InHouseMailExclusionCode;
    public ?string $ThirdPartyMailExclusionCode;
    public ?string $FaxExclusionCode;
    public ?string $EmailExclusionCode;
    public ?string $TextMessageExclusionCode;
    public ?string $MinorityGroupCode;
    public ?string $MemberSolicitationCode;
    public ?string $CityCode;
    public ?string $CountyCode;
    public ?string $InternetExclusionCode;
    public ?string $HomeAddressFax;
    public ?string $OfficePhoneExtension;
    public ?string $JoinDate;
    public ?string $JoinDate2;
    public ?string $BillingClassCode;
    public ?string $ListCodes;
    public ?string $DuesPaidThrough;
    public ?string $AicpaNumber;
    public ?bool $AicpaMember;
    public ?string $LicensedCode;
    public ?string $GroupMembershipCodes;
    public ?string $FieldsOfInterestCodes;
    public ?string $SpecialNeedsCodes;
    public ?string $AreasOfExpertiseCodes;
    public ?string $SectionsCodes;
    public ?string $MailStop;
    public ?string $Facebook;
    public ?string $LinkedIn;
    public ?string $Twitter;
    public ?string $Instagram;
    public ?string $EmailOptInCodes;
    public ?string $EmailOptOutCodes;
    public ?string $NasbaID;
    public ?bool $NasbaOptOut;
    public ?string $EmailAddressTypeCode;
    public ?string $EmailAddress2;
    public ?string $EmailAddress2TypeCode;
    public ?string $LeaderBio;
    public ?string $UpdatedBy;
    public ?string $LastUpdate;
    /** @var UserDefinedFields[] */
    public ?array $UserDefinedFields;
    /** @var UserDefinedLists[] */
    public ?array $UserDefinedLists;

    /**
     * Generates a new Person Object
     * @param mixed string|stdClass|null                 Accepts a PersonID or a stdClass
     * @param bool $IncludeLegislative                   Include legislative data default false
     * @param bool $includeMetaData                      Include meta data defaults to off
     */
    public function __construct(mixed $Data = null)
        {

        // Allowed to use PersonID
        if (is_string($Data))
            {
            $Data = $this->init($Data);
            }

        // Don't try to process an error or null
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ID = trim($Data->ID);
            $this->FirstName = $Data->FirstName;
            $this->LastName = $Data->LastName;
            $this->MiddleInitial = $Data->MiddleInitial;
            $this->Suffix = $Data->Suffix;
            $this->SalutationFirst = $Data->SalutationFirst;
            $this->PriorName = $Data->PriorName;
            $this->GenderCode = $Data->GenderCode;
            $this->PronounCode = $Data->PronounCode;
            $this->MemberTypeCode = $Data->MemberTypeCode;
            $this->MemberTypeDescription = $Data->MemberTypeDescription;
            $this->MemberStatusDescription = $Data->MemberStatusDescription;
            $this->ProspectiveMemberSourceCodes = $Data->ProspectiveMemberSourceCodes;
            $this->FirmName1 = $Data->FirmName1;
            $this->FirmName2 = $Data->FirmName2;
            $this->HomeAddressLine1 = $Data->HomeAddressLine1;
            $this->HomeAddressLine2 = $Data->HomeAddressLine2;
            $this->HomeAddressCity = $Data->HomeAddressCity;
            $this->HomeAddressStateCode = $Data->HomeAddressStateCode;
            $this->HomeAddressStreetZip = $Data->HomeAddressStreetZip;
            $this->HomeAddressPobZip = $Data->HomeAddressPobZip;
            $this->HomeAddressForeignCountry = $Data->HomeAddressForeignCountry;
            $this->FirmAddressLine1 = $Data->FirmAddressLine1;
            $this->FirmAddressLine2 = $Data->FirmAddressLine2;
            $this->FirmAddressCity = $Data->FirmAddressCity;
            $this->FirmAddressStateCode = $Data->FirmAddressStateCode;
            $this->FirmAddressStreetZip = $Data->FirmAddressStreetZip;
            $this->FirmAddressPobZip = $Data->FirmAddressPobZip;
            $this->FirmAddressForeignCountry = $Data->FirmAddressForeignCountry;
            $this->Email = $Data->Email;
            $this->InStateCertificateNumber = $Data->InStateCertificateNumber;
            $this->InStateCertificationDate = $Data->InStateCertificationDate;
            $this->OutOfStateCertificationDate = $Data->OutOfStateCertificationDate;
            $this->OutOfStateCertificationStateCode = $Data->OutOfStateCertificationStateCode;
            $this->OutOfStateCertificateNumber = $Data->OutOfStateCertificateNumber;
            $this->LinkedFirmCode = $Data->LinkedFirmCode;
            $this->SecondaryLinkedFirmCode = $Data->SecondaryLinkedFirmCode;
            $this->IsFirmAdmin = $Data->IsFirmAdmin;
            $this->GeneralMailPreferenceCode = $Data->GeneralMailPreferenceCode;
            $this->CPEMailPreferenceCode = $Data->CPEMailPreferenceCode;
            $this->PositionDescription = $Data->PositionDescription;
            $this->PositionCode = $Data->PositionCode;
            $this->HomePhone = $Data->HomePhone;
            $this->DirectPhone = $Data->DirectPhone;
            $this->DirectFax = $Data->DirectFax;
            $this->NickName = $Data->NickName;
            $this->Credentials = $Data->Credentials;
            $this->CertifiedCode = $Data->CertifiedCode;
            $this->Title = $Data->Title;
            $this->BirthDate = $Data->BirthDate;
            $this->PreferredChapterCode = $Data->PreferredChapterCode;
            $this->ActualChapterCode = $Data->ActualChapterCode;
            $this->Spouse = $Data->Spouse;
            $this->MobilePhone = $Data->MobilePhone;
            $this->ConfirmRegistrationMethodCode = $Data->ConfirmRegistrationMethodCode;
            $this->MemberStatusCode = $Data->MemberStatusCode;
            $this->InHouseMailExclusionCode = $Data->InHouseMailExclusionCode;
            $this->ThirdPartyMailExclusionCode = $Data->ThirdPartyMailExclusionCode;
            $this->FaxExclusionCode = $Data->FaxExclusionCode;
            $this->EmailExclusionCode = $Data->EmailExclusionCode;
            $this->TextMessageExclusionCode = $Data->TextMessageExclusionCode;
            $this->MinorityGroupCode = $Data->MinorityGroupCode;
            $this->MemberSolicitationCode = $Data->MemberSolicitationCode;
            $this->CityCode = $Data->CityCode;
            $this->CountyCode = $Data->CountyCode;
            $this->InternetExclusionCode = $Data->InternetExclusionCode;
            $this->HomeAddressFax = $Data->HomeAddressFax;
            $this->OfficePhoneExtension = $Data->OfficePhoneExtension;
            $this->JoinDate = $Data->JoinDate;
            $this->JoinDate2 = $Data->JoinDate2;
            $this->BillingClassCode = $Data->BillingClassCode;
            $this->ListCodes = $Data->ListCodes;
            $this->DuesPaidThrough = $Data->DuesPaidThrough;
            $this->AicpaNumber = $Data->AicpaNumber;
            $this->AicpaMember = $Data->AicpaMember;
            $this->LicensedCode = $Data->LicensedCode;
            $this->GroupMembershipCodes = $Data->GroupMembershipCodes;
            $this->FieldsOfInterestCodes = $Data->FieldsOfInterestCodes;
            $this->SpecialNeedsCodes = $Data->SpecialNeedsCodes;
            $this->AreasOfExpertiseCodes = $Data->AreasOfExpertiseCodes;
            $this->SectionsCodes = $Data->SectionsCodes;
            $this->MailStop = $Data->Mailstop; // PHIL Renamed to meet standard
            $this->Facebook = $Data->Facebook;
            $this->LinkedIn = $Data->LinkedIn;
            $this->Twitter = $Data->Twitter;
            $this->Instagram = $Data->Instagram;
            $this->EmailOptInCodes = $Data->EmailOptInCodes;
            $this->EmailOptOutCodes = $Data->EmailOptOutCodes;
            $this->NasbaID = $Data->NasbaID;
            $this->NasbaOptOut = $Data->NasbaOptOut;
            $this->EmailAddressTypeCode = $Data->EmailAddressTypeCode;
            $this->EmailAddress2 = $Data->EmailAddress2;
            $this->EmailAddress2TypeCode = $Data->EmailAddress2TypeCode;
            $this->LeaderBio = $Data->LeaderBio;
            $this->UpdatedBy = $Data->UpdatedBy;
            $this->LastUpdate = $Data->LastUpdate;
            $this->UserDefinedFields = ApiHelper::array_of_type('UserDefinedFields', $Data->UserDefinedFields);
            $this->UserDefinedLists = ApiHelper::array_of_type('UserDefinedLists', $Data->UserDefinedLists);
            }
        }

    /**
     * Gets a list if PersonIDs that need processed.
     * @return void
     */
    public function schedule_updates(): void
        {

        $TimeZone = new DateTimeZone(wp_timezone_string());

        if (!empty(get_option('cdsSyncedPersons')))
            {
            $LastSync = new DateTime(get_option('cdsSyncedPersons'), $TimeZone);
            }
        else
            {
            $LastSync = null;
            }

        $Keys = $this->get_list($LastSync);

        $Cron = new Cron();

        if (!empty($Keys))
            {
            // Schedule the cron job
            $Cron->create_cron_batch($Keys, 'CDS_Populate_Person');
            // Update the last synced datetime.
            update_option('cdsSyncedPersons', current_time('Y-m-d\TH:i'));
            }
        }

    /**
     * Import a person object to local storage
     * @param string $PersonID
     * @return WP_Error|string
     */
    public function import(string $PersonID): WP_Error|string
        {

        // Going to a direct api connection to ensure we always get the source data
        $Connection = new Connection('GET', 'Person', '/' . trim($PersonID) . '?includeLegislative=true&includeMetaData=true');
        $Person = new Person($Connection->Response);

        // Ensure we don't import a user without an email.
        if (empty($Person->Email))
            {
            $Error = new WP_Error('Person Import Failed:', 'Attempted import for user without email.', (array) $Person);
            new Logging($Error);
            return $Error;
            }

        // Create a new storage object
        $LocalPerson = new LocalPerson();

        // Loop though the person object and assign properties.
        foreach ($Person as $key => $value)
            {
            // Checking for matching properties
            if (property_exists($LocalPerson, $key))
                {
                // Making Assignments
                $LocalPerson->$key = trim($value);
                }
            }

        // Adding the full person object to the storage object.
        $LocalPerson->PersonJSON = json_encode($Person);

        // Start looking for the WordPress user
        switch (true)
            {
            case (!empty($WordPressUserID = get_users(array('meta_key'    => 'cdsId', 'meta_value'  => trim($Person->ID), 'fields'      => 'ids', 'number'      => 1, 'count_total' => false))[0])):
                break;
            case (!empty($WordPressUserID = get_user_by('login', trim($Person->Email))->ID)):
                break;
            case (!empty($WordPressUserID = get_user_by('login', trim($Person->EmailAddress2))->ID)):
                break;
            default:
                $WordPressUserID = wp_create_user(trim($Person->Email), $Person->ID, $Person->Email);
            }

        // Checking to see if creating a user failed.
        if (is_wp_error($WordPressUserID))
            {
            $Error = new WP_Error($WordPressUserID->get_error_code(), $WordPressUserID->get_error_message(), (array) $Person);
            new Logging($Error);
            return $Error;
            }
        else
            {
            // We have a user so let's update.
            $WordPressUserID = $this->update_wordpress_user($WordPressUserID, $Person);
            }

        // Was updating the user successful
        if (is_wp_error($WordPressUserID))
            {
            // No log the error
            $Error = new WP_Error($WordPressUserID->get_error_code(), $WordPressUserID->get_error_message(), (array) $Person);
            new Logging($Error);
            return $Error;
            }
        else
            {

            $LocalPerson->WordPressUserID = $WordPressUserID;

            global $wpdb;

            $Query = $wpdb->prepare("SELECT ID FROM cds_persons WHERE ID = %s;", array($LocalPerson->ID));
            $Results = $wpdb->query($Query);

            // Select threw an error log and bailout
            if (!$wpdb->last_error)
                {
                if ($Results > 0)
                    {
                    $wpdb->update('cds_persons', (array) $LocalPerson, array('ID' => trim($LocalPerson->ID)), '%s','%s');
                    }
                else
                    {
                    $wpdb->insert('cds_persons', (array) $LocalPerson, '%s');
                    }
                }

            // If an error occurred log it.
            if ($wpdb->last_error)
                {
                $Data = [];
                $Data = ['Query:' => $Query];
                $Data = ['Error:' => $wpdb->last_error];
                $Data = ['PersonStorage>' => json_encode($LocalPerson)];
                $Result = new WP_Error('SQL', $wpdb->last_error, $Data);
                new Logging($Result);

                }

            return $WordPressUserID;

            }
        }

    /**
     * Updates a Wordpress from a Person
     * @param string $WordPressUserID
     * @param Person $Person
     * @return WP_Error|string
     */
    public function update_wordpress_user(string $WordPressUserID, Person $Person): WP_Error|string
        {
        // Setting up the roles this will make all users ties to the hosting organization administrators
        if (isset($Person->LinkedFirmCode) && $Person->LinkedFirmCode === get_option('cdsFirm'))
            {
            $Role = 'administrator';
            // This must remain a string.
            $ToolBarOn = 'true';
            }
        else
            {
            $ToolBarOn = 'false';
            if ($Person->MemberStatusCode == 'M')
                {
                $Role = 'member';
                }
            else
                {
                $Role = 'non-member';
                }
            }

        // You can not modify a WordPress username without a data action.
        // This function is used in more than one location so it makes sense to
        // build this in here.
        global $wpdb;

        $wpdb->update(
            $wpdb->users,
            ['user_login' => trim($Person->Email)],
            ['ID' => $WordPressUserID]
        );

        // Setting up the user
        $WordPressUser = (
        array(
             'ID'                   => trim($WordPressUserID),
             'user_nicename'        => sanitize_email(strtolower(trim($Person->Email))),
             'user_email'           => trim($Person->Email), //sanitize_email(strtolower(trim($Person->Email))),
             'display_name'         => sanitize_text_field($Person->SalutationFirst),
             'nickname'             => sanitize_text_field($Person->SalutationFirst),
             'first_name'           => sanitize_text_field($Person->FirstName),
             'last_name'            => sanitize_text_field($Person->LastName),
             'description'          => sanitize_text_field($Person->LeaderBio),
             'user_registered'      => current_time('mysql'),
             'show_admin_bar_front' => $ToolBarOn,
             'role'                 => $Role,

            )
          );

        // Update core user values
        if (!is_wp_error($Result = wp_update_user($WordPressUser)))
            {
            update_user_meta($WordPressUserID, 'cdsId', sanitize_text_field(trim($Person->ID)));
            update_user_meta($WordPressUserID, 'show_admin_bar_front', $ToolBarOn);
            return $WordPressUserID;
            }
        else
            {
            $Error = new WP_Error($Result->get_error_code(), $Result->get_error_message(), (array) $Person);
            new Logging($Error);
            return $Error;
            }
        }

    /**
     * Used to initialize and populate a person object
     * @param string $PersonID
     * @param bool $IncludeLegislative
     * @param bool $includeMetaData
     * @return object
     */
    private function init(string $PersonID): object
        {

        // Check storage first
        $Result = new LocalPerson($PersonID);

        if (isset($Result->PersonJSON))
            {
            $Result = json_decode($Result->PersonJSON);
            }
        else
            {
            $Connection = new Connection('GET', 'Person', '/' . trim($PersonID) . '?includeLegislative=false&includeMetaData=false');
            $Result = $Connection->Response;
            }

        return $Result;

        }

    /**
     * Get a person object by person ID
     * @param string $PersonID
     * @return Person
     */
    public function get(string $PersonID) : Person
        {
        $Result = $this->init($PersonID);
        return new Person($Result);
        }

    /**
     * Returns a Person object based on a search. See PersonSearch to return a list.
     * @param string|null $Email
     * @param string|null $LastName
     * @param string|null $FirstName
     * @param string|null $MobileNumber
     * @param string|null $FirmCode
     * @return Person
     */
    public function search(?string $Email = null, ?string $LastName = null, ?string $FirstName = null, ?string $MobileNumber = null, string $FirmCode = null) : Person
        {

        $PersonSearch = new PersonSearch($Email, $LastName, $FirstName, $MobileNumber, $FirmCode);

        if (isset($PersonSearch->SearchHits) && $PersonSearch->SearchHits > 1)
            {
            $Person = $this->get($PersonSearch->PersonID);
            }

        return $Person;

        }

    /**
     * Return a person object using an email address.
     * @param string $Email
     * @return Person
     */
    public function get_by_email(string $EmailAddress): Person
        {

        global $wpdb;

        $Query = $wpdb->prepare("SELECT * FROM cds_persons WHERE Email = %s OR EmailAddress2 = %s LIMIT 1", array(trim($EmailAddress), trim($EmailAddress)));
        $Result = $wpdb->get_Results($Query)[0];

        if (isset($Result->PersonJson))
            {
            $Result = new $this(json_decode($Result->PersonJson));
            }
        else
            {
            $Result = $this->search($EmailAddress);
            $Result = new $this($Result);
            }

        return $Result;

        }


    /**
     * Retrieves a list of PersonKeys that have changed since the supplied DateTime. If DateTime is not supplied return all.
     * @param DateTime|null $DateTime
     * @param bool|null $includeLegislative
     * @return array
     */
    public function get_list(? DateTime $DateTime = null, $IncludeLegislative = false) : array
        {

        if (empty($DateTime))
            {
            // PeronKey Objects
            $Connection = new Connection('GET', 'Person', '?all=true&excludeBlankEmails=true');
            }
        else
            {
            // PersonChange Objects
            $Connection = new Connection('GET', 'PersonChanges', '?since=' . $DateTime->format('m-d-Y H:i:s') . '&includeLegislative=' . var_export($IncludeLegislative, true));
            }

        // Checking for error
        if (!is_wp_error($Connection))
            {
            // if we have FirmChange Objects get the FirmObjects
            if (!array_key_exists('NamesID', (array) $Connection->Response[0]))
                {
                $Results = array_column($Connection->Response, 'Person');
                }
            else
                {
                $Results = $Connection->Response;
                }

            $Results = str_replace(' ', '', array_column($Results, 'NamesID'));
            }
        return $Results;
        }

    /**
     * Update a person from the API using the Person ID.
     * @param string $PersonID
     * @param Person $UpdatePerson
     * @return Person|\WP_Error
     */
    public function update(string $PersonID, Person $UpdatePerson): Person|WP_Error
        {
        $Connection = new Connection('PUT', 'Person', '/' . trim($PersonID), json_encode($UpdatePerson));

        if (is_wp_error($Connection->Response))
            {
            $Result = $Connection->Response;
            }
        else
            {
            $Result = new self($Connection->Response);
            }

        return $Result;
        }

    /**
     * Creates a new person in AM.Net using the API.
     * @param Person $NewPerson
     * @return Person|\WP_Error
     */
    public function create(Person $NewPerson): Person|WP_Error
        {

        $Connection = new Connection('POST', 'Person', null, json_encode($NewPerson));

        if (is_wp_error($Connection->Response))
            {
            $Result = $Connection->Response;
            }
        else
            {
            $Result = new self($Connection->Response);
            }

        return $Result;
        }

    /**
     * Return a wordpress user via an email search.
     * @param mixed $EmailAddress
     * @return \WP_User|bool
     */
    public function get_wordpress_user_by_email(string $EmailAddress) : WP_User
        {

        // Do the simple stuff first
        $WPUser = get_user_by('login', $EmailAddress);

        // Did we find a user?
        if (!empty($WPUser))
            {
            // Return the user
            return $WPUser;
            }
        else
            {
            // Get a person object
            $Person = $this->get_by_email($EmailAddress);
            }

        // Check primary email
        if (!empty($Person) && isset($Person->Email))
            {
            $WPUser = get_user_by('login', $Person->Email);
            }
        // Check secondary email
        if (empty($WPUser) && isset($Person->EmailAddress2))
            {
            $WPUser = get_user_by('login', $Person->Email);
            }

        return $WPUser;
        }

    /**
     * Returns a Wordpress user from a person ID
     * @param string $PersonID
     * @return WP_User|bool
     */
    public function get_wordpress_user_by_person_id(string $PersonID): WP_User|bool
        {
        $Users = get_users(
            array(
                'meta_key'   => 'cdsId',
                'meta_value' => $PersonID,
                'number'     => 1,
                )
            );

        return $Users[0];
        }

    /**
     * Gets a person object using the Wordpress user id.
     * @param string $WordPressID
     * @return Person|null
     */
    public function get_person_by_wordpress_id(string $WordPressID) : Person
        {
        $PersonID = get_user_meta($WordPressID, 'cdsId', true);

        if (!empty($PersonID))
            {
            $Result = $this->get($PersonID);
            }
        else
            {
            $Result = null;
            }

        return $Result;

        }
    }