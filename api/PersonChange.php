<?php

namespace cds\api;

defined('ABSPATH') or exit;

class PersonChange
    {

    public ?string $ChangeDate;
    public ? PersonKey $Person;

    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ChangeDate = $Data->ChangeDate;
            $this->Person = new PersonKey($Data->Person);
            }
        }
    }