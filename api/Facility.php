<?php

namespace cds\api;
use cds\api\models\Helper;
use cds\helpers\ApiHelper;

defined('ABSPATH') or exit;

class Facility
    {
    public ?string $AirportEta;
    public ?string $CateringContactEmail;
    public ?string $CateringContactName;
    public ?string $CateringContactPhone;
    public ?string $CateringContactPhoneFax;
    public ?string $Directions;
    /** @var FirmKey[] */
    public array $LinkedHotels;
    public ?string $Note1;
    public ?string $Note2;
    public ?string $Note3;
    public ?string $ReservationPhone;
    public ?string $ReservationPhoneExt;
    public ?string $RoomNote;
    public ?string $RoomRate1Begin;
    public ?string $RoomRate1Description; // PHIL Adjusted the name to correct spelling.
    public ?string $RoomRate1Double;
    public ?string $RoomRate1End;
    public ?string $RoomRate1Single;
    public ?string $RoomRate2Begin;
    public ?string $RoomRate2Description; // PHIL Adjusted the name to correct spelling.
    public ?string $RoomRate2Double;
    public ?string $RoomRate2End;
    public ?string $RoomRate2Single;
    public ?string $RoomRate3Begin;
    public ?string $RoomRate3Description; // PHIL Adjusted the name to correct spelling.
    public ?string $RoomRate3Double;
    public ?string $RoomRate3End;
    public ?string $RoomRate3Single;
    public ?string $RoomRate4Begin;
    public ?string $RoomRate4Description; // PHIL Adjusted the name to correct spelling.
    public ?string $RoomRate4Double;
    public ?string $RoomRate4End;
    public ?string $RoomRate4Single;
    public ?string $SalesContactEmail;
    public ?string $SalesContactName;
    public ?string $SalesContactPhone;
    public ?string $salesContactPhoneExt;
    public ?string $ShuttleInfo;
    public ?string $WebDirections;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->AirportEta = $Data->AirportEta;
            $this->CateringContactEmail = $Data->CateringContactEmail;
            $this->CateringContactName = $Data->CateringContactName;
            $this->CateringContactPhone = $Data->CateringContactPhone;
            $this->CateringContactPhoneFax = $Data->CateringContactPhoneFax;
            $this->Directions = $Data->Directions;
            $this->LinkedHotels = ApiHelper::array_of_type('FirmKey', $Data->LinkedHotels);
            $this->Note1 = $Data->Note1;
            $this->Note2 = $Data->Note2;
            $this->Note3 = $Data->Note3;
            $this->ReservationPhone = $Data->ReservationPhone;
            $this->ReservationPhoneExt = $Data->ReservationPhoneExt;
            $this->RoomNote = $Data->RoomNote;
            $this->RoomRate1Begin = $Data->RoomRate1Begin;
            $this->RoomRate1Description = $Data->RoomeRate1Description ?? $Data->RoomRate1Description; // PHIL Adjusted the name to correct spelling.
            $this->RoomRate1Double = $Data->RoomRate1Double;
            $this->RoomRate1End = $Data->RoomRate1End;
            $this->RoomRate1Single = $Data->RoomRate1Single;
            $this->RoomRate2Begin = $Data->RoomRate2Begin;
            $this->RoomRate2Description = $Data->RoomeRate2Description ?? $Data->RoomRate2Description; // PHIL Adjusted the name to correct spelling.
            $this->RoomRate2Double = $Data->RoomRate2Double;
            $this->RoomRate2End = $Data->RoomRate2End;
            $this->RoomRate2Single = $Data->RoomRate2Single;
            $this->RoomRate3Begin = $Data->RoomRate3Begin;
            $this->RoomRate3Description = $Data->RoomeRate3Description ?? $Data->RoomRate3Description; // PHIL Adjusted the name to correct spelling.
            $this->RoomRate3Double = $Data->RoomRate3Double;
            $this->RoomRate3End = $Data->RoomRate3End;
            $this->RoomRate3Single = $Data->RoomRate3Single;
            $this->RoomRate4Begin = $Data->RoomRate4Begin;
            $this->RoomRate4Description = $Data->RoomeRate4Description ?? $Data->RoomRate4Description; // PHIL Adjusted the name to correct spelling.
            $this->RoomRate4Double = $Data->RoomRate4Double;
            $this->RoomRate4End = $Data->RoomRate4End;
            $this->RoomRate4Single = $Data->RoomRate4Single;
            $this->SalesContactEmail = $Data->SalesContactEmail;
            $this->SalesContactName = $Data->SalesContactName;
            $this->SalesContactPhone = $Data->SalesContactPhone;
            $this->salesContactPhoneExt = $Data->salesContactPhoneExt;
            $this->ShuttleInfo = $Data->ShuttleInfo;
            $this->WebDirections = $Data->WebDirections;
            }
        }
    }