<?php

namespace cds\api;
use cds\helpers\ApiHelper;

defined('ABSPATH') or exit;

class MetaData
    {

    public ?string $ApiObjectName;
    /** @var Field[]|null */
    public ?array $Fields;

    /**
     * Summary of __construct
     * @param object|null $Data
     * @suppress 0416
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->Fields = ApiHelper::array_of_type('Field', $Data->FirmDTO);
            }
        }
    }