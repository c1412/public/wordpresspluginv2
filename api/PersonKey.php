<?php

namespace cds\api;

defined('ABSPATH') or exit;

class PersonKey
    {

    public ?string $ID;
    public ?string $NamesID;
    public ?string $LastModified;

    /**
     * Summary of __construct
     * @param object|null $Data
     * @suppress 0416
     */
    public function __construct(?object $Data = null)
        {
        if (!is_wp_error($Data) && is_object($Data))
            {
            $this->ID = trim($Data->NamesID);
            $this->NamesID = trim($Data->NamesID);
            $this->LastModified = $Data->LastModified;
            }
        }
    }