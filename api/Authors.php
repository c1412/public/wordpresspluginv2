<?php

namespace cds\api;

defined('ABSPATH') or exit;

class Authors
    {
    public ?string $NamesID;
    public ?string $LastModified;

    public function __construct(?object $Data = null)
        {
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->NamesID = $Data->NamesID;
            $this->LastModified = $Data->LastModified;
            }

        }
    }