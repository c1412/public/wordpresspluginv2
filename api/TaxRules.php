<?php

namespace cds\api;

defined('ABSPATH') or exit;

class TaxRules
{
	public ?string $StateCode;
	public ?int $TaxRate;

	public function __construct(?object $Data = null)
	{
        if (is_object($Data) && !is_wp_error($Data))
            {
            $this->StateCode = $Data->StateCode;
            $this->TaxRate = $Data->TaxRate;
            }
	}
}